///
//  Generated code. Do not modify.
//  source: whiteboard/board.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use boardDescriptor instead')
const Board$json = const {
  '1': 'Board',
  '2': const [
    const {'1': 'operator', '3': 1, '4': 1, '5': 11, '6': '.whiteboard.core.User', '10': 'operator'},
    const {'1': 'timestamp', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'timestamp'},
    const {'1': 'self', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.Action', '9': 0, '10': 'self'},
    const {'1': 'window', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.window.Action', '9': 0, '10': 'window'},
    const {'1': 'floating', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.board.floating.Action', '9': 0, '10': 'floating'},
    const {'1': 'channel', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Action', '9': 0, '10': 'channel'},
    const {'1': 'qa', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.qa.Action', '9': 0, '10': 'qa'},
    const {'1': 'answer', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.answer.Action', '9': 0, '10': 'answer'},
    const {'1': 'ppt', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Action', '9': 0, '10': 'ppt'},
    const {'1': 'pdf', '3': 14, '4': 1, '5': 11, '6': '.whiteboard.board.pdf.Action', '9': 0, '10': 'pdf'},
    const {'1': 'palette', '3': 15, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Action', '9': 0, '10': 'palette'},
    const {'1': 'media', '3': 16, '4': 1, '5': 11, '6': '.whiteboard.board.media.Action', '9': 0, '10': 'media'},
    const {'1': 'responder', '3': 17, '4': 1, '5': 11, '6': '.whiteboard.board.responder.Action', '9': 0, '10': 'responder'},
    const {'1': 'timer', '3': 18, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Action', '9': 0, '10': 'timer'},
    const {'1': 'stage', '3': 19, '4': 1, '5': 11, '6': '.whiteboard.board.stage.Action', '9': 0, '10': 'stage'},
    const {'1': 'lucky', '3': 20, '4': 1, '5': 11, '6': '.whiteboard.board.lucky.Action', '9': 0, '10': 'lucky'},
    const {'1': 'dice', '3': 21, '4': 1, '5': 11, '6': '.whiteboard.board.dice.Action', '9': 0, '10': 'dice'},
    const {'1': 'collaborator', '3': 22, '4': 1, '5': 11, '6': '.whiteboard.board.collaborator.Action', '9': 0, '10': 'collaborator'},
    const {'1': 'biteacher', '3': 23, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.Action', '9': 0, '10': 'biteacher'},
    const {'1': 'share', '3': 24, '4': 1, '5': 11, '6': '.whiteboard.board.share.Action', '9': 0, '10': 'share'},
    const {'1': 'debug', '3': 70, '4': 1, '5': 11, '6': '.whiteboard.board.debug.Action', '9': 0, '10': 'debug'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Board`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List boardDescriptor = $convert.base64Decode('CgVCb2FyZBIxCghvcGVyYXRvchgBIAEoCzIVLndoaXRlYm9hcmQuY29yZS5Vc2VyUghvcGVyYXRvchI4Cgl0aW1lc3RhbXAYAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgl0aW1lc3RhbXASLgoEc2VsZhgFIAEoCzIYLndoaXRlYm9hcmQuYm9hcmQuQWN0aW9uSABSBHNlbGYSOQoGd2luZG93GAYgASgLMh8ud2hpdGVib2FyZC5ib2FyZC53aW5kb3cuQWN0aW9uSABSBndpbmRvdxI/CghmbG9hdGluZxgJIAEoCzIhLndoaXRlYm9hcmQuYm9hcmQuZmxvYXRpbmcuQWN0aW9uSABSCGZsb2F0aW5nEjwKB2NoYW5uZWwYCiABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLmNoYW5uZWwuQWN0aW9uSABSB2NoYW5uZWwSLQoCcWEYCyABKAsyGy53aGl0ZWJvYXJkLmJvYXJkLnFhLkFjdGlvbkgAUgJxYRI5CgZhbnN3ZXIYDCABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLmFuc3dlci5BY3Rpb25IAFIGYW5zd2VyEjAKA3BwdBgNIAEoCzIcLndoaXRlYm9hcmQuYm9hcmQucHB0LkFjdGlvbkgAUgNwcHQSMAoDcGRmGA4gASgLMhwud2hpdGVib2FyZC5ib2FyZC5wZGYuQWN0aW9uSABSA3BkZhI8CgdwYWxldHRlGA8gASgLMiAud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLkFjdGlvbkgAUgdwYWxldHRlEjYKBW1lZGlhGBAgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5tZWRpYS5BY3Rpb25IAFIFbWVkaWESQgoJcmVzcG9uZGVyGBEgASgLMiIud2hpdGVib2FyZC5ib2FyZC5yZXNwb25kZXIuQWN0aW9uSABSCXJlc3BvbmRlchI2CgV0aW1lchgSIAEoCzIeLndoaXRlYm9hcmQuYm9hcmQudGltZXIuQWN0aW9uSABSBXRpbWVyEjYKBXN0YWdlGBMgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5zdGFnZS5BY3Rpb25IAFIFc3RhZ2USNgoFbHVja3kYFCABKAsyHi53aGl0ZWJvYXJkLmJvYXJkLmx1Y2t5LkFjdGlvbkgAUgVsdWNreRIzCgRkaWNlGBUgASgLMh0ud2hpdGVib2FyZC5ib2FyZC5kaWNlLkFjdGlvbkgAUgRkaWNlEksKDGNvbGxhYm9yYXRvchgWIAEoCzIlLndoaXRlYm9hcmQuYm9hcmQuY29sbGFib3JhdG9yLkFjdGlvbkgAUgxjb2xsYWJvcmF0b3ISQgoJYml0ZWFjaGVyGBcgASgLMiIud2hpdGVib2FyZC5ib2FyZC5iaXRlYWNoZXIuQWN0aW9uSABSCWJpdGVhY2hlchI2CgVzaGFyZRgYIAEoCzIeLndoaXRlYm9hcmQuYm9hcmQuc2hhcmUuQWN0aW9uSABSBXNoYXJlEjYKBWRlYnVnGEYgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5kZWJ1Zy5BY3Rpb25IAFIFZGVidWdCCAoGYWN0aW9u');
