///
//  Generated code. Do not modify.
//  source: whiteboard/board.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'core/user.pb.dart' as $0;
import '../google/protobuf/timestamp.pb.dart' as $1;
import 'board/action.pb.dart' as $2;
import 'board/window/action.pb.dart' as $3;
import 'board/floating/action.pb.dart' as $4;
import 'board/channel/action.pb.dart' as $5;
import 'board/qa/action.pb.dart' as $6;
import 'board/answer/action.pb.dart' as $7;
import 'board/ppt/action.pb.dart' as $8;
import 'board/pdf/action.pb.dart' as $9;
import 'board/palette/action.pb.dart' as $10;
import 'board/media/action.pb.dart' as $11;
import 'board/responder/action.pb.dart' as $12;
import 'board/timer/action.pb.dart' as $13;
import 'board/stage/action.pb.dart' as $14;
import 'board/lucky/action.pb.dart' as $15;
import 'board/dice/action.pb.dart' as $16;
import 'board/collaborator/action.pb.dart' as $17;
import 'board/biteacher/action.pb.dart' as $18;
import 'board/share/action.pb.dart' as $19;
import 'board/debug/action.pb.dart' as $20;

enum Board_Action {
  self, 
  window, 
  floating, 
  channel, 
  qa, 
  answer, 
  ppt, 
  pdf, 
  palette, 
  media, 
  responder, 
  timer, 
  stage, 
  lucky, 
  dice, 
  collaborator, 
  biteacher, 
  share, 
  debug, 
  notSet
}

class Board extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Board_Action> _Board_ActionByTag = {
    5 : Board_Action.self,
    6 : Board_Action.window,
    9 : Board_Action.floating,
    10 : Board_Action.channel,
    11 : Board_Action.qa,
    12 : Board_Action.answer,
    13 : Board_Action.ppt,
    14 : Board_Action.pdf,
    15 : Board_Action.palette,
    16 : Board_Action.media,
    17 : Board_Action.responder,
    18 : Board_Action.timer,
    19 : Board_Action.stage,
    20 : Board_Action.lucky,
    21 : Board_Action.dice,
    22 : Board_Action.collaborator,
    23 : Board_Action.biteacher,
    24 : Board_Action.share,
    70 : Board_Action.debug,
    0 : Board_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Board', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard'), createEmptyInstance: create)
    ..oo(0, [5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 70])
    ..aOM<$0.User>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'operator', subBuilder: $0.User.create)
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timestamp', subBuilder: $1.Timestamp.create)
    ..aOM<$2.Action>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'self', subBuilder: $2.Action.create)
    ..aOM<$3.Action>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $3.Action.create)
    ..aOM<$4.Action>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'floating', subBuilder: $4.Action.create)
    ..aOM<$5.Action>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'channel', subBuilder: $5.Action.create)
    ..aOM<$6.Action>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'qa', subBuilder: $6.Action.create)
    ..aOM<$7.Action>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'answer', subBuilder: $7.Action.create)
    ..aOM<$8.Action>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ppt', subBuilder: $8.Action.create)
    ..aOM<$9.Action>(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pdf', subBuilder: $9.Action.create)
    ..aOM<$10.Action>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'palette', subBuilder: $10.Action.create)
    ..aOM<$11.Action>(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'media', subBuilder: $11.Action.create)
    ..aOM<$12.Action>(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'responder', subBuilder: $12.Action.create)
    ..aOM<$13.Action>(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timer', subBuilder: $13.Action.create)
    ..aOM<$14.Action>(19, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stage', subBuilder: $14.Action.create)
    ..aOM<$15.Action>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lucky', subBuilder: $15.Action.create)
    ..aOM<$16.Action>(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dice', subBuilder: $16.Action.create)
    ..aOM<$17.Action>(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'collaborator', subBuilder: $17.Action.create)
    ..aOM<$18.Action>(23, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'biteacher', subBuilder: $18.Action.create)
    ..aOM<$19.Action>(24, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'share', subBuilder: $19.Action.create)
    ..aOM<$20.Action>(70, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'debug', subBuilder: $20.Action.create)
    ..hasRequiredFields = false
  ;

  Board._() : super();
  factory Board({
    $0.User? operator,
    $1.Timestamp? timestamp,
    $2.Action? self,
    $3.Action? window,
    $4.Action? floating,
    $5.Action? channel,
    $6.Action? qa,
    $7.Action? answer,
    $8.Action? ppt,
    $9.Action? pdf,
    $10.Action? palette,
    $11.Action? media,
    $12.Action? responder,
    $13.Action? timer,
    $14.Action? stage,
    $15.Action? lucky,
    $16.Action? dice,
    $17.Action? collaborator,
    $18.Action? biteacher,
    $19.Action? share,
    $20.Action? debug,
  }) {
    final _result = create();
    if (operator != null) {
      _result.operator = operator;
    }
    if (timestamp != null) {
      _result.timestamp = timestamp;
    }
    if (self != null) {
      _result.self = self;
    }
    if (window != null) {
      _result.window = window;
    }
    if (floating != null) {
      _result.floating = floating;
    }
    if (channel != null) {
      _result.channel = channel;
    }
    if (qa != null) {
      _result.qa = qa;
    }
    if (answer != null) {
      _result.answer = answer;
    }
    if (ppt != null) {
      _result.ppt = ppt;
    }
    if (pdf != null) {
      _result.pdf = pdf;
    }
    if (palette != null) {
      _result.palette = palette;
    }
    if (media != null) {
      _result.media = media;
    }
    if (responder != null) {
      _result.responder = responder;
    }
    if (timer != null) {
      _result.timer = timer;
    }
    if (stage != null) {
      _result.stage = stage;
    }
    if (lucky != null) {
      _result.lucky = lucky;
    }
    if (dice != null) {
      _result.dice = dice;
    }
    if (collaborator != null) {
      _result.collaborator = collaborator;
    }
    if (biteacher != null) {
      _result.biteacher = biteacher;
    }
    if (share != null) {
      _result.share = share;
    }
    if (debug != null) {
      _result.debug = debug;
    }
    return _result;
  }
  factory Board.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Board.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Board clone() => Board()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Board copyWith(void Function(Board) updates) => super.copyWith((message) => updates(message as Board)) as Board; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Board create() => Board._();
  Board createEmptyInstance() => create();
  static $pb.PbList<Board> createRepeated() => $pb.PbList<Board>();
  @$core.pragma('dart2js:noInline')
  static Board getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Board>(create);
  static Board? _defaultInstance;

  Board_Action whichAction() => _Board_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.User get operator => $_getN(0);
  @$pb.TagNumber(1)
  set operator($0.User v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasOperator() => $_has(0);
  @$pb.TagNumber(1)
  void clearOperator() => clearField(1);
  @$pb.TagNumber(1)
  $0.User ensureOperator() => $_ensure(0);

  @$pb.TagNumber(2)
  $1.Timestamp get timestamp => $_getN(1);
  @$pb.TagNumber(2)
  set timestamp($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTimestamp() => $_has(1);
  @$pb.TagNumber(2)
  void clearTimestamp() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureTimestamp() => $_ensure(1);

  @$pb.TagNumber(5)
  $2.Action get self => $_getN(2);
  @$pb.TagNumber(5)
  set self($2.Action v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSelf() => $_has(2);
  @$pb.TagNumber(5)
  void clearSelf() => clearField(5);
  @$pb.TagNumber(5)
  $2.Action ensureSelf() => $_ensure(2);

  @$pb.TagNumber(6)
  $3.Action get window => $_getN(3);
  @$pb.TagNumber(6)
  set window($3.Action v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasWindow() => $_has(3);
  @$pb.TagNumber(6)
  void clearWindow() => clearField(6);
  @$pb.TagNumber(6)
  $3.Action ensureWindow() => $_ensure(3);

  @$pb.TagNumber(9)
  $4.Action get floating => $_getN(4);
  @$pb.TagNumber(9)
  set floating($4.Action v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasFloating() => $_has(4);
  @$pb.TagNumber(9)
  void clearFloating() => clearField(9);
  @$pb.TagNumber(9)
  $4.Action ensureFloating() => $_ensure(4);

  @$pb.TagNumber(10)
  $5.Action get channel => $_getN(5);
  @$pb.TagNumber(10)
  set channel($5.Action v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasChannel() => $_has(5);
  @$pb.TagNumber(10)
  void clearChannel() => clearField(10);
  @$pb.TagNumber(10)
  $5.Action ensureChannel() => $_ensure(5);

  @$pb.TagNumber(11)
  $6.Action get qa => $_getN(6);
  @$pb.TagNumber(11)
  set qa($6.Action v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasQa() => $_has(6);
  @$pb.TagNumber(11)
  void clearQa() => clearField(11);
  @$pb.TagNumber(11)
  $6.Action ensureQa() => $_ensure(6);

  @$pb.TagNumber(12)
  $7.Action get answer => $_getN(7);
  @$pb.TagNumber(12)
  set answer($7.Action v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasAnswer() => $_has(7);
  @$pb.TagNumber(12)
  void clearAnswer() => clearField(12);
  @$pb.TagNumber(12)
  $7.Action ensureAnswer() => $_ensure(7);

  @$pb.TagNumber(13)
  $8.Action get ppt => $_getN(8);
  @$pb.TagNumber(13)
  set ppt($8.Action v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasPpt() => $_has(8);
  @$pb.TagNumber(13)
  void clearPpt() => clearField(13);
  @$pb.TagNumber(13)
  $8.Action ensurePpt() => $_ensure(8);

  @$pb.TagNumber(14)
  $9.Action get pdf => $_getN(9);
  @$pb.TagNumber(14)
  set pdf($9.Action v) { setField(14, v); }
  @$pb.TagNumber(14)
  $core.bool hasPdf() => $_has(9);
  @$pb.TagNumber(14)
  void clearPdf() => clearField(14);
  @$pb.TagNumber(14)
  $9.Action ensurePdf() => $_ensure(9);

  @$pb.TagNumber(15)
  $10.Action get palette => $_getN(10);
  @$pb.TagNumber(15)
  set palette($10.Action v) { setField(15, v); }
  @$pb.TagNumber(15)
  $core.bool hasPalette() => $_has(10);
  @$pb.TagNumber(15)
  void clearPalette() => clearField(15);
  @$pb.TagNumber(15)
  $10.Action ensurePalette() => $_ensure(10);

  @$pb.TagNumber(16)
  $11.Action get media => $_getN(11);
  @$pb.TagNumber(16)
  set media($11.Action v) { setField(16, v); }
  @$pb.TagNumber(16)
  $core.bool hasMedia() => $_has(11);
  @$pb.TagNumber(16)
  void clearMedia() => clearField(16);
  @$pb.TagNumber(16)
  $11.Action ensureMedia() => $_ensure(11);

  @$pb.TagNumber(17)
  $12.Action get responder => $_getN(12);
  @$pb.TagNumber(17)
  set responder($12.Action v) { setField(17, v); }
  @$pb.TagNumber(17)
  $core.bool hasResponder() => $_has(12);
  @$pb.TagNumber(17)
  void clearResponder() => clearField(17);
  @$pb.TagNumber(17)
  $12.Action ensureResponder() => $_ensure(12);

  @$pb.TagNumber(18)
  $13.Action get timer => $_getN(13);
  @$pb.TagNumber(18)
  set timer($13.Action v) { setField(18, v); }
  @$pb.TagNumber(18)
  $core.bool hasTimer() => $_has(13);
  @$pb.TagNumber(18)
  void clearTimer() => clearField(18);
  @$pb.TagNumber(18)
  $13.Action ensureTimer() => $_ensure(13);

  @$pb.TagNumber(19)
  $14.Action get stage => $_getN(14);
  @$pb.TagNumber(19)
  set stage($14.Action v) { setField(19, v); }
  @$pb.TagNumber(19)
  $core.bool hasStage() => $_has(14);
  @$pb.TagNumber(19)
  void clearStage() => clearField(19);
  @$pb.TagNumber(19)
  $14.Action ensureStage() => $_ensure(14);

  @$pb.TagNumber(20)
  $15.Action get lucky => $_getN(15);
  @$pb.TagNumber(20)
  set lucky($15.Action v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasLucky() => $_has(15);
  @$pb.TagNumber(20)
  void clearLucky() => clearField(20);
  @$pb.TagNumber(20)
  $15.Action ensureLucky() => $_ensure(15);

  @$pb.TagNumber(21)
  $16.Action get dice => $_getN(16);
  @$pb.TagNumber(21)
  set dice($16.Action v) { setField(21, v); }
  @$pb.TagNumber(21)
  $core.bool hasDice() => $_has(16);
  @$pb.TagNumber(21)
  void clearDice() => clearField(21);
  @$pb.TagNumber(21)
  $16.Action ensureDice() => $_ensure(16);

  @$pb.TagNumber(22)
  $17.Action get collaborator => $_getN(17);
  @$pb.TagNumber(22)
  set collaborator($17.Action v) { setField(22, v); }
  @$pb.TagNumber(22)
  $core.bool hasCollaborator() => $_has(17);
  @$pb.TagNumber(22)
  void clearCollaborator() => clearField(22);
  @$pb.TagNumber(22)
  $17.Action ensureCollaborator() => $_ensure(17);

  @$pb.TagNumber(23)
  $18.Action get biteacher => $_getN(18);
  @$pb.TagNumber(23)
  set biteacher($18.Action v) { setField(23, v); }
  @$pb.TagNumber(23)
  $core.bool hasBiteacher() => $_has(18);
  @$pb.TagNumber(23)
  void clearBiteacher() => clearField(23);
  @$pb.TagNumber(23)
  $18.Action ensureBiteacher() => $_ensure(18);

  @$pb.TagNumber(24)
  $19.Action get share => $_getN(19);
  @$pb.TagNumber(24)
  set share($19.Action v) { setField(24, v); }
  @$pb.TagNumber(24)
  $core.bool hasShare() => $_has(19);
  @$pb.TagNumber(24)
  void clearShare() => clearField(24);
  @$pb.TagNumber(24)
  $19.Action ensureShare() => $_ensure(19);

  @$pb.TagNumber(70)
  $20.Action get debug => $_getN(20);
  @$pb.TagNumber(70)
  set debug($20.Action v) { setField(70, v); }
  @$pb.TagNumber(70)
  $core.bool hasDebug() => $_has(20);
  @$pb.TagNumber(70)
  void clearDebug() => clearField(70);
  @$pb.TagNumber(70)
  $20.Action ensureDebug() => $_ensure(20);
}

