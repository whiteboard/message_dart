///
//  Generated code. Do not modify.
//  source: whiteboard/core/stream_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use streamTypeDescriptor instead')
const StreamType$json = const {
  '1': 'StreamType',
  '2': const [
    const {'1': 'STREAM_TYPE_UNSPECIFIED', '2': 0},
    const {'1': 'BUILTIN', '2': 10},
    const {'1': 'EXTERNAL', '2': 11},
  ],
};

/// Descriptor for `StreamType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List streamTypeDescriptor = $convert.base64Decode('CgpTdHJlYW1UeXBlEhsKF1NUUkVBTV9UWVBFX1VOU1BFQ0lGSUVEEAASCwoHQlVJTFRJThAKEgwKCEVYVEVSTkFMEAs=');
