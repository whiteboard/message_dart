///
//  Generated code. Do not modify.
//  source: whiteboard/core/gauge.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Gauge extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Gauge', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'top', $pb.PbFieldType.OF)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'right', $pb.PbFieldType.OF)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bottom', $pb.PbFieldType.OF)
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'left', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Gauge._() : super();
  factory Gauge({
    $core.double? top,
    $core.double? right,
    $core.double? bottom,
    $core.double? left,
  }) {
    final _result = create();
    if (top != null) {
      _result.top = top;
    }
    if (right != null) {
      _result.right = right;
    }
    if (bottom != null) {
      _result.bottom = bottom;
    }
    if (left != null) {
      _result.left = left;
    }
    return _result;
  }
  factory Gauge.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Gauge.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Gauge clone() => Gauge()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Gauge copyWith(void Function(Gauge) updates) => super.copyWith((message) => updates(message as Gauge)) as Gauge; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Gauge create() => Gauge._();
  Gauge createEmptyInstance() => create();
  static $pb.PbList<Gauge> createRepeated() => $pb.PbList<Gauge>();
  @$core.pragma('dart2js:noInline')
  static Gauge getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Gauge>(create);
  static Gauge? _defaultInstance;

  @$pb.TagNumber(3)
  $core.double get top => $_getN(0);
  @$pb.TagNumber(3)
  set top($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasTop() => $_has(0);
  @$pb.TagNumber(3)
  void clearTop() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get right => $_getN(1);
  @$pb.TagNumber(4)
  set right($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasRight() => $_has(1);
  @$pb.TagNumber(4)
  void clearRight() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get bottom => $_getN(2);
  @$pb.TagNumber(5)
  set bottom($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(5)
  $core.bool hasBottom() => $_has(2);
  @$pb.TagNumber(5)
  void clearBottom() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get left => $_getN(3);
  @$pb.TagNumber(6)
  set left($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(6)
  $core.bool hasLeft() => $_has(3);
  @$pb.TagNumber(6)
  void clearLeft() => clearField(6);
}

