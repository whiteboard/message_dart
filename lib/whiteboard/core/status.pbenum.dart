///
//  Generated code. Do not modify.
//  source: whiteboard/core/status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Status extends $pb.ProtobufEnum {
  static const Status STATUS_UNSPECIFIED = Status._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STATUS_UNSPECIFIED');
  static const Status CREATED = Status._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CREATED');
  static const Status PREPARING = Status._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PREPARING');
  static const Status STARTED = Status._(20, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STARTED');
  static const Status LEFT = Status._(21, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LEFT');
  static const Status DELAYED = Status._(22, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DELAYED');
  static const Status TERMINATED = Status._(30, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TERMINATED');
  static const Status SUSPENDED = Status._(31, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SUSPENDED');

  static const $core.List<Status> values = <Status> [
    STATUS_UNSPECIFIED,
    CREATED,
    PREPARING,
    STARTED,
    LEFT,
    DELAYED,
    TERMINATED,
    SUSPENDED,
  ];

  static final $core.Map<$core.int, Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Status? valueOf($core.int value) => _byValue[value];

  const Status._($core.int v, $core.String n) : super(v, n);
}

