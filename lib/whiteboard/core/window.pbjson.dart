///
//  Generated code. Do not modify.
//  source: whiteboard/core/window.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use windowDescriptor instead')
const Window$json = const {
  '1': 'Window',
  '2': const [
    const {'1': 'info', '3': 2, '4': 1, '5': 11, '6': '.whiteboard.core.WindowInfo', '10': 'info'},
    const {'1': 'point', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'point'},
    const {'1': 'min', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.core.Size', '10': 'min'},
    const {'1': 'size', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.core.Size', '10': 'size'},
    const {'1': 'max', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.core.Size', '10': 'max'},
    const {'1': 'margin', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.core.Gauge', '10': 'margin'},
    const {'1': 'padding', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.core.Gauge', '10': 'padding'},
    const {'1': 'scrollbar', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.core.Scrollbar', '10': 'scrollbar'},
    const {'1': 'status', '3': 14, '4': 1, '5': 14, '6': '.whiteboard.core.WindowStatus', '10': 'status'},
    const {'1': 'z_index', '3': 15, '4': 1, '5': 5, '10': 'zIndex'},
  ],
};

/// Descriptor for `Window`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List windowDescriptor = $convert.base64Decode('CgZXaW5kb3cSLwoEaW5mbxgCIAEoCzIbLndoaXRlYm9hcmQuY29yZS5XaW5kb3dJbmZvUgRpbmZvEiwKBXBvaW50GAMgASgLMhYud2hpdGVib2FyZC5jb3JlLlBvaW50UgVwb2ludBInCgNtaW4YBSABKAsyFS53aGl0ZWJvYXJkLmNvcmUuU2l6ZVIDbWluEikKBHNpemUYBiABKAsyFS53aGl0ZWJvYXJkLmNvcmUuU2l6ZVIEc2l6ZRInCgNtYXgYByABKAsyFS53aGl0ZWJvYXJkLmNvcmUuU2l6ZVIDbWF4Ei4KBm1hcmdpbhgKIAEoCzIWLndoaXRlYm9hcmQuY29yZS5HYXVnZVIGbWFyZ2luEjAKB3BhZGRpbmcYCyABKAsyFi53aGl0ZWJvYXJkLmNvcmUuR2F1Z2VSB3BhZGRpbmcSOAoJc2Nyb2xsYmFyGA0gASgLMhoud2hpdGVib2FyZC5jb3JlLlNjcm9sbGJhclIJc2Nyb2xsYmFyEjUKBnN0YXR1cxgOIAEoDjIdLndoaXRlYm9hcmQuY29yZS5XaW5kb3dTdGF0dXNSBnN0YXR1cxIXCgd6X2luZGV4GA8gASgFUgZ6SW5kZXg=');
