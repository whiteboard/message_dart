///
//  Generated code. Do not modify.
//  source: whiteboard/core/device_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use deviceStatusDescriptor instead')
const DeviceStatus$json = const {
  '1': 'DeviceStatus',
  '2': const [
    const {'1': 'DEVICE_STATUS_UNSPECIFIED', '2': 0},
    const {'1': 'NO_SUCH', '2': 1},
    const {'1': 'OPENED', '2': 2},
    const {'1': 'CLOSED', '2': 3},
  ],
};

/// Descriptor for `DeviceStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List deviceStatusDescriptor = $convert.base64Decode('CgxEZXZpY2VTdGF0dXMSHQoZREVWSUNFX1NUQVRVU19VTlNQRUNJRklFRBAAEgsKB05PX1NVQ0gQARIKCgZPUEVORUQQAhIKCgZDTE9TRUQQAw==');
