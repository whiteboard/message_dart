///
//  Generated code. Do not modify.
//  source: whiteboard/core/direction.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use directionDescriptor instead')
const Direction$json = const {
  '1': 'Direction',
  '2': const [
    const {'1': 'DIRECTION_UNSPECIFIED', '2': 0},
    const {'1': 'RIGHTWARD', '2': 1},
    const {'1': 'UPWARD', '2': 2},
    const {'1': 'LEFTWARD', '2': 3},
    const {'1': 'DOWNWARD', '2': 4},
  ],
};

/// Descriptor for `Direction`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List directionDescriptor = $convert.base64Decode('CglEaXJlY3Rpb24SGQoVRElSRUNUSU9OX1VOU1BFQ0lGSUVEEAASDQoJUklHSFRXQVJEEAESCgoGVVBXQVJEEAISDAoITEVGVFdBUkQQAxIMCghET1dOV0FSRBAE');
