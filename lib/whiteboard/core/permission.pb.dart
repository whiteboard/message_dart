///
//  Generated code. Do not modify.
//  source: whiteboard/core/permission.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Permission extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Permission', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'authorized')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hosted')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'prohibited')
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'staged')
    ..hasRequiredFields = false
  ;

  Permission._() : super();
  factory Permission({
    $core.bool? authorized,
    $core.bool? hosted,
    $core.bool? prohibited,
    $core.bool? staged,
  }) {
    final _result = create();
    if (authorized != null) {
      _result.authorized = authorized;
    }
    if (hosted != null) {
      _result.hosted = hosted;
    }
    if (prohibited != null) {
      _result.prohibited = prohibited;
    }
    if (staged != null) {
      _result.staged = staged;
    }
    return _result;
  }
  factory Permission.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Permission.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Permission clone() => Permission()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Permission copyWith(void Function(Permission) updates) => super.copyWith((message) => updates(message as Permission)) as Permission; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Permission create() => Permission._();
  Permission createEmptyInstance() => create();
  static $pb.PbList<Permission> createRepeated() => $pb.PbList<Permission>();
  @$core.pragma('dart2js:noInline')
  static Permission getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Permission>(create);
  static Permission? _defaultInstance;

  @$pb.TagNumber(3)
  $core.bool get authorized => $_getBF(0);
  @$pb.TagNumber(3)
  set authorized($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasAuthorized() => $_has(0);
  @$pb.TagNumber(3)
  void clearAuthorized() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get hosted => $_getBF(1);
  @$pb.TagNumber(4)
  set hosted($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasHosted() => $_has(1);
  @$pb.TagNumber(4)
  void clearHosted() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get prohibited => $_getBF(2);
  @$pb.TagNumber(5)
  set prohibited($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(5)
  $core.bool hasProhibited() => $_has(2);
  @$pb.TagNumber(5)
  void clearProhibited() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get staged => $_getBF(3);
  @$pb.TagNumber(6)
  set staged($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(6)
  $core.bool hasStaged() => $_has(3);
  @$pb.TagNumber(6)
  void clearStaged() => clearField(6);
}

