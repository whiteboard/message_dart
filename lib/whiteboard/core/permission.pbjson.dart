///
//  Generated code. Do not modify.
//  source: whiteboard/core/permission.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use permissionDescriptor instead')
const Permission$json = const {
  '1': 'Permission',
  '2': const [
    const {'1': 'authorized', '3': 3, '4': 1, '5': 8, '10': 'authorized'},
    const {'1': 'hosted', '3': 4, '4': 1, '5': 8, '10': 'hosted'},
    const {'1': 'prohibited', '3': 5, '4': 1, '5': 8, '10': 'prohibited'},
    const {'1': 'staged', '3': 6, '4': 1, '5': 8, '10': 'staged'},
  ],
};

/// Descriptor for `Permission`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List permissionDescriptor = $convert.base64Decode('CgpQZXJtaXNzaW9uEh4KCmF1dGhvcml6ZWQYAyABKAhSCmF1dGhvcml6ZWQSFgoGaG9zdGVkGAQgASgIUgZob3N0ZWQSHgoKcHJvaGliaXRlZBgFIAEoCFIKcHJvaGliaXRlZBIWCgZzdGFnZWQYBiABKAhSBnN0YWdlZA==');
