///
//  Generated code. Do not modify.
//  source: whiteboard/core/user_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use userTypeDescriptor instead')
const UserType$json = const {
  '1': 'UserType',
  '2': const [
    const {'1': 'USER_TYPE_UNSPECIFIED', '2': 0},
    const {'1': 'SYSTEM', '2': 0},
    const {'1': 'LECTURER', '2': 1},
    const {'1': 'ASSISTANT', '2': 2},
    const {'1': 'STUDENT', '2': 3},
    const {'1': 'TOURIST', '2': 4},
    const {'1': 'INSPECTOR', '2': 5},
  ],
  '3': const {'2': true},
};

/// Descriptor for `UserType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List userTypeDescriptor = $convert.base64Decode('CghVc2VyVHlwZRIZChVVU0VSX1RZUEVfVU5TUEVDSUZJRUQQABIKCgZTWVNURU0QABIMCghMRUNUVVJFUhABEg0KCUFTU0lTVEFOVBACEgsKB1NUVURFTlQQAxILCgdUT1VSSVNUEAQSDQoJSU5TUEVDVE9SEAUaAhAB');
