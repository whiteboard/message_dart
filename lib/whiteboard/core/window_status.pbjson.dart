///
//  Generated code. Do not modify.
//  source: whiteboard/core/window_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use windowStatusDescriptor instead')
const WindowStatus$json = const {
  '1': 'WindowStatus',
  '2': const [
    const {'1': 'WINDOW_STATUS_UNSPECIFIED', '2': 0},
    const {'1': 'MINIMIZED', '2': 1},
    const {'1': 'NORMAL', '2': 2},
    const {'1': 'MAXIMIZED', '2': 3},
  ],
};

/// Descriptor for `WindowStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List windowStatusDescriptor = $convert.base64Decode('CgxXaW5kb3dTdGF0dXMSHQoZV0lORE9XX1NUQVRVU19VTlNQRUNJRklFRBAAEg0KCU1JTklNSVpFRBABEgoKBk5PUk1BTBACEg0KCU1BWElNSVpFRBAD');
