///
//  Generated code. Do not modify.
//  source: whiteboard/core/serialize.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Serialize extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Serialize', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'error')
    ..hasRequiredFields = false
  ;

  Serialize._() : super();
  factory Serialize({
    $core.String? error,
  }) {
    final _result = create();
    if (error != null) {
      _result.error = error;
    }
    return _result;
  }
  factory Serialize.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Serialize.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Serialize clone() => Serialize()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Serialize copyWith(void Function(Serialize) updates) => super.copyWith((message) => updates(message as Serialize)) as Serialize; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Serialize create() => Serialize._();
  Serialize createEmptyInstance() => create();
  static $pb.PbList<Serialize> createRepeated() => $pb.PbList<Serialize>();
  @$core.pragma('dart2js:noInline')
  static Serialize getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Serialize>(create);
  static Serialize? _defaultInstance;

  @$pb.TagNumber(5)
  $core.String get error => $_getSZ(0);
  @$pb.TagNumber(5)
  set error($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasError() => $_has(0);
  @$pb.TagNumber(5)
  void clearError() => clearField(5);
}

