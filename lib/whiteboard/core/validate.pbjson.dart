///
//  Generated code. Do not modify.
//  source: whiteboard/core/validate.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use validateDescriptor instead')
const Validate$json = const {
  '1': 'Validate',
  '2': const [
    const {'1': 'errors', '3': 5, '4': 3, '5': 11, '6': '.whiteboard.core.Validate.ErrorsEntry', '10': 'errors'},
  ],
  '3': const [Validate_ErrorsEntry$json],
};

@$core.Deprecated('Use validateDescriptor instead')
const Validate_ErrorsEntry$json = const {
  '1': 'ErrorsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Validate`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List validateDescriptor = $convert.base64Decode('CghWYWxpZGF0ZRI9CgZlcnJvcnMYBSADKAsyJS53aGl0ZWJvYXJkLmNvcmUuVmFsaWRhdGUuRXJyb3JzRW50cnlSBmVycm9ycxo5CgtFcnJvcnNFbnRyeRIQCgNrZXkYASABKAlSA2tleRIUCgV2YWx1ZRgCIAEoCVIFdmFsdWU6AjgB');
