///
//  Generated code. Do not modify.
//  source: whiteboard/core/stream_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class StreamType extends $pb.ProtobufEnum {
  static const StreamType STREAM_TYPE_UNSPECIFIED = StreamType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STREAM_TYPE_UNSPECIFIED');
  static const StreamType BUILTIN = StreamType._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'BUILTIN');
  static const StreamType EXTERNAL = StreamType._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'EXTERNAL');

  static const $core.List<StreamType> values = <StreamType> [
    STREAM_TYPE_UNSPECIFIED,
    BUILTIN,
    EXTERNAL,
  ];

  static final $core.Map<$core.int, StreamType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static StreamType? valueOf($core.int value) => _byValue[value];

  const StreamType._($core.int v, $core.String n) : super(v, n);
}

