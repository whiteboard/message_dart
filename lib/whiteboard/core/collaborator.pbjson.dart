///
//  Generated code. Do not modify.
//  source: whiteboard/core/collaborator.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use collaboratorDescriptor instead')
const Collaborator$json = const {
  '1': 'Collaborator',
  '2': const [
    const {'1': 'id', '3': 3, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'nickname', '3': 4, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'type', '3': 5, '4': 1, '5': 14, '6': '.whiteboard.core.UserType', '10': 'type'},
    const {'1': 'phone', '3': 9, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'device', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.core.Device', '10': 'device'},
    const {'1': 'permission', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.core.Permission', '10': 'permission'},
  ],
};

/// Descriptor for `Collaborator`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List collaboratorDescriptor = $convert.base64Decode('CgxDb2xsYWJvcmF0b3ISDgoCaWQYAyABKAlSAmlkEhoKCG5pY2tuYW1lGAQgASgJUghuaWNrbmFtZRItCgR0eXBlGAUgASgOMhkud2hpdGVib2FyZC5jb3JlLlVzZXJUeXBlUgR0eXBlEhQKBXBob25lGAkgASgJUgVwaG9uZRIvCgZkZXZpY2UYCiABKAsyFy53aGl0ZWJvYXJkLmNvcmUuRGV2aWNlUgZkZXZpY2USOwoKcGVybWlzc2lvbhgLIAEoCzIbLndoaXRlYm9hcmQuY29yZS5QZXJtaXNzaW9uUgpwZXJtaXNzaW9u');
