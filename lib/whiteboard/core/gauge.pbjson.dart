///
//  Generated code. Do not modify.
//  source: whiteboard/core/gauge.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use gaugeDescriptor instead')
const Gauge$json = const {
  '1': 'Gauge',
  '2': const [
    const {'1': 'top', '3': 3, '4': 1, '5': 2, '10': 'top'},
    const {'1': 'right', '3': 4, '4': 1, '5': 2, '10': 'right'},
    const {'1': 'bottom', '3': 5, '4': 1, '5': 2, '10': 'bottom'},
    const {'1': 'left', '3': 6, '4': 1, '5': 2, '10': 'left'},
  ],
};

/// Descriptor for `Gauge`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gaugeDescriptor = $convert.base64Decode('CgVHYXVnZRIQCgN0b3AYAyABKAJSA3RvcBIUCgVyaWdodBgEIAEoAlIFcmlnaHQSFgoGYm90dG9tGAUgASgCUgZib3R0b20SEgoEbGVmdBgGIAEoAlIEbGVmdA==');
