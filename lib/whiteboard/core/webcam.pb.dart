///
//  Generated code. Do not modify.
//  source: whiteboard/core/webcam.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'device_status.pbenum.dart' as $0;

class Webcam extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Webcam', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..e<$0.DeviceStatus>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $0.DeviceStatus.DEVICE_STATUS_UNSPECIFIED, valueOf: $0.DeviceStatus.valueOf, enumValues: $0.DeviceStatus.values)
    ..hasRequiredFields = false
  ;

  Webcam._() : super();
  factory Webcam({
    $0.DeviceStatus? status,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory Webcam.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Webcam.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Webcam clone() => Webcam()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Webcam copyWith(void Function(Webcam) updates) => super.copyWith((message) => updates(message as Webcam)) as Webcam; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Webcam create() => Webcam._();
  Webcam createEmptyInstance() => create();
  static $pb.PbList<Webcam> createRepeated() => $pb.PbList<Webcam>();
  @$core.pragma('dart2js:noInline')
  static Webcam getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Webcam>(create);
  static Webcam? _defaultInstance;

  @$pb.TagNumber(3)
  $0.DeviceStatus get status => $_getN(0);
  @$pb.TagNumber(3)
  set status($0.DeviceStatus v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

