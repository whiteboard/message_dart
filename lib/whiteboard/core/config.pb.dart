///
//  Generated code. Do not modify.
//  source: whiteboard/core/config.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'user.pb.dart' as $8;
import 'device.pb.dart' as $3;
import 'stream.pb.dart' as $9;
import 'stage.pb.dart' as $10;

class Config extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Config', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'collapsed')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'prohibited')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'autostart')
    ..aOM<$8.User>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'host', subBuilder: $8.User.create)
    ..aOM<$3.Device>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'device', subBuilder: $3.Device.create)
    ..aOM<$9.Stream>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stream', subBuilder: $9.Stream.create)
    ..aOM<$10.Stage>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stage', subBuilder: $10.Stage.create)
    ..hasRequiredFields = false
  ;

  Config._() : super();
  factory Config({
    $core.bool? collapsed,
    $core.bool? prohibited,
    $core.bool? autostart,
    $8.User? host,
    $3.Device? device,
    $9.Stream? stream,
    $10.Stage? stage,
  }) {
    final _result = create();
    if (collapsed != null) {
      _result.collapsed = collapsed;
    }
    if (prohibited != null) {
      _result.prohibited = prohibited;
    }
    if (autostart != null) {
      _result.autostart = autostart;
    }
    if (host != null) {
      _result.host = host;
    }
    if (device != null) {
      _result.device = device;
    }
    if (stream != null) {
      _result.stream = stream;
    }
    if (stage != null) {
      _result.stage = stage;
    }
    return _result;
  }
  factory Config.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Config.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Config clone() => Config()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Config copyWith(void Function(Config) updates) => super.copyWith((message) => updates(message as Config)) as Config; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Config create() => Config._();
  Config createEmptyInstance() => create();
  static $pb.PbList<Config> createRepeated() => $pb.PbList<Config>();
  @$core.pragma('dart2js:noInline')
  static Config getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Config>(create);
  static Config? _defaultInstance;

  @$pb.TagNumber(3)
  $core.bool get collapsed => $_getBF(0);
  @$pb.TagNumber(3)
  set collapsed($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasCollapsed() => $_has(0);
  @$pb.TagNumber(3)
  void clearCollapsed() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get prohibited => $_getBF(1);
  @$pb.TagNumber(4)
  set prohibited($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasProhibited() => $_has(1);
  @$pb.TagNumber(4)
  void clearProhibited() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get autostart => $_getBF(2);
  @$pb.TagNumber(5)
  set autostart($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(5)
  $core.bool hasAutostart() => $_has(2);
  @$pb.TagNumber(5)
  void clearAutostart() => clearField(5);

  @$pb.TagNumber(9)
  $8.User get host => $_getN(3);
  @$pb.TagNumber(9)
  set host($8.User v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasHost() => $_has(3);
  @$pb.TagNumber(9)
  void clearHost() => clearField(9);
  @$pb.TagNumber(9)
  $8.User ensureHost() => $_ensure(3);

  @$pb.TagNumber(10)
  $3.Device get device => $_getN(4);
  @$pb.TagNumber(10)
  set device($3.Device v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasDevice() => $_has(4);
  @$pb.TagNumber(10)
  void clearDevice() => clearField(10);
  @$pb.TagNumber(10)
  $3.Device ensureDevice() => $_ensure(4);

  @$pb.TagNumber(11)
  $9.Stream get stream => $_getN(5);
  @$pb.TagNumber(11)
  set stream($9.Stream v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasStream() => $_has(5);
  @$pb.TagNumber(11)
  void clearStream() => clearField(11);
  @$pb.TagNumber(11)
  $9.Stream ensureStream() => $_ensure(5);

  @$pb.TagNumber(12)
  $10.Stage get stage => $_getN(6);
  @$pb.TagNumber(12)
  set stage($10.Stage v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasStage() => $_has(6);
  @$pb.TagNumber(12)
  void clearStage() => clearField(12);
  @$pb.TagNumber(12)
  $10.Stage ensureStage() => $_ensure(6);
}

