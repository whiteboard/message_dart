///
//  Generated code. Do not modify.
//  source: whiteboard/core/collaborator.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'device.pb.dart' as $3;
import 'permission.pb.dart' as $4;

import 'user_type.pbenum.dart' as $5;

class Collaborator extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Collaborator', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..e<$5.UserType>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $5.UserType.USER_TYPE_UNSPECIFIED, valueOf: $5.UserType.valueOf, enumValues: $5.UserType.values)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOM<$3.Device>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'device', subBuilder: $3.Device.create)
    ..aOM<$4.Permission>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permission', subBuilder: $4.Permission.create)
    ..hasRequiredFields = false
  ;

  Collaborator._() : super();
  factory Collaborator({
    $core.String? id,
    $core.String? nickname,
    $5.UserType? type,
    $core.String? phone,
    $3.Device? device,
    $4.Permission? permission,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (type != null) {
      _result.type = type;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (device != null) {
      _result.device = device;
    }
    if (permission != null) {
      _result.permission = permission;
    }
    return _result;
  }
  factory Collaborator.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Collaborator.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Collaborator clone() => Collaborator()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Collaborator copyWith(void Function(Collaborator) updates) => super.copyWith((message) => updates(message as Collaborator)) as Collaborator; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Collaborator create() => Collaborator._();
  Collaborator createEmptyInstance() => create();
  static $pb.PbList<Collaborator> createRepeated() => $pb.PbList<Collaborator>();
  @$core.pragma('dart2js:noInline')
  static Collaborator getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Collaborator>(create);
  static Collaborator? _defaultInstance;

  @$pb.TagNumber(3)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(3)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(3)
  void clearId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get nickname => $_getSZ(1);
  @$pb.TagNumber(4)
  set nickname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasNickname() => $_has(1);
  @$pb.TagNumber(4)
  void clearNickname() => clearField(4);

  @$pb.TagNumber(5)
  $5.UserType get type => $_getN(2);
  @$pb.TagNumber(5)
  set type($5.UserType v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasType() => $_has(2);
  @$pb.TagNumber(5)
  void clearType() => clearField(5);

  @$pb.TagNumber(9)
  $core.String get phone => $_getSZ(3);
  @$pb.TagNumber(9)
  set phone($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(9)
  $core.bool hasPhone() => $_has(3);
  @$pb.TagNumber(9)
  void clearPhone() => clearField(9);

  @$pb.TagNumber(10)
  $3.Device get device => $_getN(4);
  @$pb.TagNumber(10)
  set device($3.Device v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasDevice() => $_has(4);
  @$pb.TagNumber(10)
  void clearDevice() => clearField(10);
  @$pb.TagNumber(10)
  $3.Device ensureDevice() => $_ensure(4);

  @$pb.TagNumber(11)
  $4.Permission get permission => $_getN(5);
  @$pb.TagNumber(11)
  set permission($4.Permission v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasPermission() => $_has(5);
  @$pb.TagNumber(11)
  void clearPermission() => clearField(11);
  @$pb.TagNumber(11)
  $4.Permission ensurePermission() => $_ensure(5);
}

