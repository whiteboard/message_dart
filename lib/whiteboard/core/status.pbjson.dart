///
//  Generated code. Do not modify.
//  source: whiteboard/core/status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use statusDescriptor instead')
const Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'STATUS_UNSPECIFIED', '2': 0},
    const {'1': 'CREATED', '2': 10},
    const {'1': 'PREPARING', '2': 11},
    const {'1': 'STARTED', '2': 20},
    const {'1': 'LEFT', '2': 21},
    const {'1': 'DELAYED', '2': 22},
    const {'1': 'TERMINATED', '2': 30},
    const {'1': 'SUSPENDED', '2': 31},
  ],
};

/// Descriptor for `Status`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List statusDescriptor = $convert.base64Decode('CgZTdGF0dXMSFgoSU1RBVFVTX1VOU1BFQ0lGSUVEEAASCwoHQ1JFQVRFRBAKEg0KCVBSRVBBUklORxALEgsKB1NUQVJURUQQFBIICgRMRUZUEBUSCwoHREVMQVlFRBAWEg4KClRFUk1JTkFURUQQHhINCglTVVNQRU5ERUQQHw==');
