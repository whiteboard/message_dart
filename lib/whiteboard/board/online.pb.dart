///
//  Generated code. Do not modify.
//  source: whiteboard/board/online.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../core/collaborator.pb.dart' as $0;

class Online extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Online', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total', $pb.PbFieldType.O3)
    ..aOM<$0.Collaborator>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'collaborator', subBuilder: $0.Collaborator.create)
    ..hasRequiredFields = false
  ;

  Online._() : super();
  factory Online({
    $core.int? total,
    $0.Collaborator? collaborator,
  }) {
    final _result = create();
    if (total != null) {
      _result.total = total;
    }
    if (collaborator != null) {
      _result.collaborator = collaborator;
    }
    return _result;
  }
  factory Online.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Online.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Online clone() => Online()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Online copyWith(void Function(Online) updates) => super.copyWith((message) => updates(message as Online)) as Online; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Online create() => Online._();
  Online createEmptyInstance() => create();
  static $pb.PbList<Online> createRepeated() => $pb.PbList<Online>();
  @$core.pragma('dart2js:noInline')
  static Online getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Online>(create);
  static Online? _defaultInstance;

  @$pb.TagNumber(3)
  $core.int get total => $_getIZ(0);
  @$pb.TagNumber(3)
  set total($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasTotal() => $_has(0);
  @$pb.TagNumber(3)
  void clearTotal() => clearField(3);

  @$pb.TagNumber(4)
  $0.Collaborator get collaborator => $_getN(1);
  @$pb.TagNumber(4)
  set collaborator($0.Collaborator v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasCollaborator() => $_has(1);
  @$pb.TagNumber(4)
  void clearCollaborator() => clearField(4);
  @$pb.TagNumber(4)
  $0.Collaborator ensureCollaborator() => $_ensure(1);
}

