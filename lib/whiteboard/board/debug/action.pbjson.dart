///
//  Generated code. Do not modify.
//  source: whiteboard/board/debug/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'serialize', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.core.Serialize', '9': 0, '10': 'serialize'},
    const {'1': 'validate', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.core.Validate', '9': 0, '10': 'validate'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SOgoJc2VyaWFsaXplGAUgASgLMhoud2hpdGVib2FyZC5jb3JlLlNlcmlhbGl6ZUgAUglzZXJpYWxpemUSNwoIdmFsaWRhdGUYBiABKAsyGS53aGl0ZWJvYXJkLmNvcmUuVmFsaWRhdGVIAFIIdmFsaWRhdGVCCAoGYWN0aW9u');
