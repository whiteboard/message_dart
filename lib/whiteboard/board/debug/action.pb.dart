///
//  Generated code. Do not modify.
//  source: whiteboard/board/debug/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/serialize.pb.dart' as $25;
import '../../core/validate.pb.dart' as $26;

enum Action_Action {
  serialize, 
  validate, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.serialize,
    6 : Action_Action.validate,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.debug'), createEmptyInstance: create)
    ..oo(0, [5, 6])
    ..aOM<$25.Serialize>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serialize', subBuilder: $25.Serialize.create)
    ..aOM<$26.Validate>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validate', subBuilder: $26.Validate.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $25.Serialize? serialize,
    $26.Validate? validate,
  }) {
    final _result = create();
    if (serialize != null) {
      _result.serialize = serialize;
    }
    if (validate != null) {
      _result.validate = validate;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(5)
  $25.Serialize get serialize => $_getN(0);
  @$pb.TagNumber(5)
  set serialize($25.Serialize v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSerialize() => $_has(0);
  @$pb.TagNumber(5)
  void clearSerialize() => clearField(5);
  @$pb.TagNumber(5)
  $25.Serialize ensureSerialize() => $_ensure(0);

  @$pb.TagNumber(6)
  $26.Validate get validate => $_getN(1);
  @$pb.TagNumber(6)
  set validate($26.Validate v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasValidate() => $_has(1);
  @$pb.TagNumber(6)
  void clearValidate() => clearField(6);
  @$pb.TagNumber(6)
  $26.Validate ensureValidate() => $_ensure(1);
}

