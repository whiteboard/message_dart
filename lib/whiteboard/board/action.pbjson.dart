///
//  Generated code. Do not modify.
//  source: whiteboard/board/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'online', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.board.Online', '9': 0, '10': 'online'},
    const {'1': 'offline', '3': 4, '4': 1, '5': 11, '6': '.whiteboard.board.Offline', '9': 0, '10': 'offline'},
    const {'1': 'collapse', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.Collapse', '9': 0, '10': 'collapse'},
    const {'1': 'expand', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.Expand', '9': 0, '10': 'expand'},
    const {'1': 'status_changed', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.board.StatusChanged', '9': 0, '10': 'statusChanged'},
    const {'1': 'webcam', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.Webcam', '9': 0, '10': 'webcam'},
    const {'1': 'microphone', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.Microphone', '9': 0, '10': 'microphone'},
    const {'1': 'host', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.Host', '9': 0, '10': 'host'},
    const {'1': 'stream', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.Stream', '9': 0, '10': 'stream'},
    const {'1': 'raise_hand', '3': 20, '4': 1, '5': 11, '6': '.whiteboard.board.RaiseHand', '9': 0, '10': 'raiseHand'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SMgoGb25saW5lGAMgASgLMhgud2hpdGVib2FyZC5ib2FyZC5PbmxpbmVIAFIGb25saW5lEjUKB29mZmxpbmUYBCABKAsyGS53aGl0ZWJvYXJkLmJvYXJkLk9mZmxpbmVIAFIHb2ZmbGluZRI4Cghjb2xsYXBzZRgHIAEoCzIaLndoaXRlYm9hcmQuYm9hcmQuQ29sbGFwc2VIAFIIY29sbGFwc2USMgoGZXhwYW5kGAggASgLMhgud2hpdGVib2FyZC5ib2FyZC5FeHBhbmRIAFIGZXhwYW5kEkgKDnN0YXR1c19jaGFuZ2VkGAkgASgLMh8ud2hpdGVib2FyZC5ib2FyZC5TdGF0dXNDaGFuZ2VkSABSDXN0YXR1c0NoYW5nZWQSMgoGd2ViY2FtGAogASgLMhgud2hpdGVib2FyZC5ib2FyZC5XZWJjYW1IAFIGd2ViY2FtEj4KCm1pY3JvcGhvbmUYCyABKAsyHC53aGl0ZWJvYXJkLmJvYXJkLk1pY3JvcGhvbmVIAFIKbWljcm9waG9uZRIsCgRob3N0GAwgASgLMhYud2hpdGVib2FyZC5ib2FyZC5Ib3N0SABSBGhvc3QSMgoGc3RyZWFtGA0gASgLMhgud2hpdGVib2FyZC5ib2FyZC5TdHJlYW1IAFIGc3RyZWFtEjwKCnJhaXNlX2hhbmQYFCABKAsyGy53aGl0ZWJvYXJkLmJvYXJkLlJhaXNlSGFuZEgAUglyYWlzZUhhbmRCCAoGYWN0aW9u');
