///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/triggering.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'slide.pb.dart' as $67;
import 'trigger.pb.dart' as $69;

class Triggering extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Triggering', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.ppt'), createEmptyInstance: create)
    ..aOM<$67.Slide>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'slide', subBuilder: $67.Slide.create)
    ..aOM<$69.Trigger>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'trigger', subBuilder: $69.Trigger.create)
    ..hasRequiredFields = false
  ;

  Triggering._() : super();
  factory Triggering({
    $67.Slide? slide,
    $69.Trigger? trigger,
  }) {
    final _result = create();
    if (slide != null) {
      _result.slide = slide;
    }
    if (trigger != null) {
      _result.trigger = trigger;
    }
    return _result;
  }
  factory Triggering.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Triggering.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Triggering clone() => Triggering()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Triggering copyWith(void Function(Triggering) updates) => super.copyWith((message) => updates(message as Triggering)) as Triggering; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Triggering create() => Triggering._();
  Triggering createEmptyInstance() => create();
  static $pb.PbList<Triggering> createRepeated() => $pb.PbList<Triggering>();
  @$core.pragma('dart2js:noInline')
  static Triggering getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Triggering>(create);
  static Triggering? _defaultInstance;

  @$pb.TagNumber(5)
  $67.Slide get slide => $_getN(0);
  @$pb.TagNumber(5)
  set slide($67.Slide v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSlide() => $_has(0);
  @$pb.TagNumber(5)
  void clearSlide() => clearField(5);
  @$pb.TagNumber(5)
  $67.Slide ensureSlide() => $_ensure(0);

  @$pb.TagNumber(6)
  $69.Trigger get trigger => $_getN(1);
  @$pb.TagNumber(6)
  set trigger($69.Trigger v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasTrigger() => $_has(1);
  @$pb.TagNumber(6)
  void clearTrigger() => clearField(6);
  @$pb.TagNumber(6)
  $69.Trigger ensureTrigger() => $_ensure(1);
}

