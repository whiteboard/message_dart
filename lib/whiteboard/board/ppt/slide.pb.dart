///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/slide.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Slide extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Slide', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.ppt'), createEmptyInstance: create)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'index', $pb.PbFieldType.O3)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'step', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Slide._() : super();
  factory Slide({
    $core.int? index,
    $core.int? step,
  }) {
    final _result = create();
    if (index != null) {
      _result.index = index;
    }
    if (step != null) {
      _result.step = step;
    }
    return _result;
  }
  factory Slide.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Slide.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Slide clone() => Slide()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Slide copyWith(void Function(Slide) updates) => super.copyWith((message) => updates(message as Slide)) as Slide; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Slide create() => Slide._();
  Slide createEmptyInstance() => create();
  static $pb.PbList<Slide> createRepeated() => $pb.PbList<Slide>();
  @$core.pragma('dart2js:noInline')
  static Slide getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Slide>(create);
  static Slide? _defaultInstance;

  @$pb.TagNumber(5)
  $core.int get index => $_getIZ(0);
  @$pb.TagNumber(5)
  set index($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasIndex() => $_has(0);
  @$pb.TagNumber(5)
  void clearIndex() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get step => $_getIZ(1);
  @$pb.TagNumber(6)
  set step($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(6)
  $core.bool hasStep() => $_has(1);
  @$pb.TagNumber(6)
  void clearStep() => clearField(6);
}

