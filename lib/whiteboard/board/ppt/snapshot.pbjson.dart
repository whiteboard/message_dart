///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'file', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.File', '10': 'file'},
    const {'1': 'current', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Slide', '10': 'current'},
    const {'1': 'triggers', '3': 7, '4': 3, '5': 11, '6': '.whiteboard.board.ppt.Trigger', '10': 'triggers'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBIpCgRmaWxlGAMgASgLMhUud2hpdGVib2FyZC5jb3JlLkZpbGVSBGZpbGUSNQoHY3VycmVudBgGIAEoCzIbLndoaXRlYm9hcmQuYm9hcmQucHB0LlNsaWRlUgdjdXJyZW50EjkKCHRyaWdnZXJzGAcgAygLMh0ud2hpdGVib2FyZC5ib2FyZC5wcHQuVHJpZ2dlclIIdHJpZ2dlcnM=');
