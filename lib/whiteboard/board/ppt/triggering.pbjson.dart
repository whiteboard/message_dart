///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/triggering.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use triggeringDescriptor instead')
const Triggering$json = const {
  '1': 'Triggering',
  '2': const [
    const {'1': 'slide', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Slide', '10': 'slide'},
    const {'1': 'trigger', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Trigger', '10': 'trigger'},
  ],
};

/// Descriptor for `Triggering`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List triggeringDescriptor = $convert.base64Decode('CgpUcmlnZ2VyaW5nEjEKBXNsaWRlGAUgASgLMhsud2hpdGVib2FyZC5ib2FyZC5wcHQuU2xpZGVSBXNsaWRlEjcKB3RyaWdnZXIYBiABKAsyHS53aGl0ZWJvYXJkLmJvYXJkLnBwdC5UcmlnZ2VyUgd0cmlnZ2Vy');
