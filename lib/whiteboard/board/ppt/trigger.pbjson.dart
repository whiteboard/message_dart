///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/trigger.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use triggerDescriptor instead')
const Trigger$json = const {
  '1': 'Trigger',
  '2': const [
    const {'1': 'id', '3': 5, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'category', '3': 6, '4': 1, '5': 9, '10': 'category'},
    const {'1': 'step', '3': 7, '4': 1, '5': 5, '10': 'step'},
  ],
};

/// Descriptor for `Trigger`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List triggerDescriptor = $convert.base64Decode('CgdUcmlnZ2VyEg4KAmlkGAUgASgFUgJpZBIaCghjYXRlZ29yeRgGIAEoCVIIY2F0ZWdvcnkSEgoEc3RlcBgHIAEoBVIEc3RlcA==');
