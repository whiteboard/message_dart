///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/jump.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use jumpDescriptor instead')
const Jump$json = const {
  '1': 'Jump',
  '2': const [
    const {'1': 'to', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Slide', '10': 'to'},
    const {'1': 'direction', '3': 6, '4': 1, '5': 14, '6': '.whiteboard.core.Direction', '10': 'direction'},
  ],
};

/// Descriptor for `Jump`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List jumpDescriptor = $convert.base64Decode('CgRKdW1wEisKAnRvGAUgASgLMhsud2hpdGVib2FyZC5ib2FyZC5wcHQuU2xpZGVSAnRvEjgKCWRpcmVjdGlvbhgGIAEoDjIaLndoaXRlYm9hcmQuY29yZS5EaXJlY3Rpb25SCWRpcmVjdGlvbg==');
