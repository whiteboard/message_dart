///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.Window', '10': 'window'},
    const {'1': 'open', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Open', '9': 0, '10': 'open'},
    const {'1': 'jump', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Jump', '9': 0, '10': 'jump'},
    const {'1': 'triggering', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Triggering', '9': 0, '10': 'triggering'},
    const {'1': 'snapshot', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.ppt.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SLwoGd2luZG93GAMgASgLMhcud2hpdGVib2FyZC5jb3JlLldpbmRvd1IGd2luZG93EjAKBG9wZW4YBSABKAsyGi53aGl0ZWJvYXJkLmJvYXJkLnBwdC5PcGVuSABSBG9wZW4SMAoEanVtcBgGIAEoCzIaLndoaXRlYm9hcmQuYm9hcmQucHB0Lkp1bXBIAFIEanVtcBJCCgp0cmlnZ2VyaW5nGAcgASgLMiAud2hpdGVib2FyZC5ib2FyZC5wcHQuVHJpZ2dlcmluZ0gAUgp0cmlnZ2VyaW5nEjwKCHNuYXBzaG90GAogASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wcHQuU25hcHNob3RIAFIIc25hcHNob3RCCAoGYWN0aW9u');
