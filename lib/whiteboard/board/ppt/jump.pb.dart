///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/jump.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'slide.pb.dart' as $67;

import '../../core/direction.pbenum.dart' as $68;

class Jump extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Jump', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.ppt'), createEmptyInstance: create)
    ..aOM<$67.Slide>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'to', subBuilder: $67.Slide.create)
    ..e<$68.Direction>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'direction', $pb.PbFieldType.OE, defaultOrMaker: $68.Direction.DIRECTION_UNSPECIFIED, valueOf: $68.Direction.valueOf, enumValues: $68.Direction.values)
    ..hasRequiredFields = false
  ;

  Jump._() : super();
  factory Jump({
    $67.Slide? to,
    $68.Direction? direction,
  }) {
    final _result = create();
    if (to != null) {
      _result.to = to;
    }
    if (direction != null) {
      _result.direction = direction;
    }
    return _result;
  }
  factory Jump.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Jump.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Jump clone() => Jump()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Jump copyWith(void Function(Jump) updates) => super.copyWith((message) => updates(message as Jump)) as Jump; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Jump create() => Jump._();
  Jump createEmptyInstance() => create();
  static $pb.PbList<Jump> createRepeated() => $pb.PbList<Jump>();
  @$core.pragma('dart2js:noInline')
  static Jump getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Jump>(create);
  static Jump? _defaultInstance;

  @$pb.TagNumber(5)
  $67.Slide get to => $_getN(0);
  @$pb.TagNumber(5)
  set to($67.Slide v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasTo() => $_has(0);
  @$pb.TagNumber(5)
  void clearTo() => clearField(5);
  @$pb.TagNumber(5)
  $67.Slide ensureTo() => $_ensure(0);

  @$pb.TagNumber(6)
  $68.Direction get direction => $_getN(1);
  @$pb.TagNumber(6)
  set direction($68.Direction v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasDirection() => $_has(1);
  @$pb.TagNumber(6)
  void clearDirection() => clearField(6);
}

