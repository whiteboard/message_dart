///
//  Generated code. Do not modify.
//  source: whiteboard/board/ppt/slide.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use slideDescriptor instead')
const Slide$json = const {
  '1': 'Slide',
  '2': const [
    const {'1': 'index', '3': 5, '4': 1, '5': 5, '10': 'index'},
    const {'1': 'step', '3': 6, '4': 1, '5': 5, '10': 'step'},
  ],
};

/// Descriptor for `Slide`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List slideDescriptor = $convert.base64Decode('CgVTbGlkZRIUCgVpbmRleBgFIAEoBVIFaW5kZXgSEgoEc3RlcBgGIAEoBVIEc3RlcA==');
