///
//  Generated code. Do not modify.
//  source: whiteboard/board/offline.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../core/user.pb.dart' as $1;

class Offline extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Offline', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total', $pb.PbFieldType.O3)
    ..aOM<$1.User>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $1.User.create)
    ..hasRequiredFields = false
  ;

  Offline._() : super();
  factory Offline({
    $core.int? total,
    $1.User? user,
  }) {
    final _result = create();
    if (total != null) {
      _result.total = total;
    }
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory Offline.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Offline.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Offline clone() => Offline()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Offline copyWith(void Function(Offline) updates) => super.copyWith((message) => updates(message as Offline)) as Offline; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Offline create() => Offline._();
  Offline createEmptyInstance() => create();
  static $pb.PbList<Offline> createRepeated() => $pb.PbList<Offline>();
  @$core.pragma('dart2js:noInline')
  static Offline getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Offline>(create);
  static Offline? _defaultInstance;

  @$pb.TagNumber(3)
  $core.int get total => $_getIZ(0);
  @$pb.TagNumber(3)
  set total($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasTotal() => $_has(0);
  @$pb.TagNumber(3)
  void clearTotal() => clearField(3);

  @$pb.TagNumber(4)
  $1.User get user => $_getN(1);
  @$pb.TagNumber(4)
  set user($1.User v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(4)
  void clearUser() => clearField(4);
  @$pb.TagNumber(4)
  $1.User ensureUser() => $_ensure(1);
}

