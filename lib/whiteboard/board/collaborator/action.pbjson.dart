///
//  Generated code. Do not modify.
//  source: whiteboard/board/collaborator/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.whiteboard.core.User', '10': 'user'},
    const {'1': 'microphone', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.collaborator.Microphone', '9': 0, '10': 'microphone'},
    const {'1': 'stage', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.collaborator.Stage', '9': 0, '10': 'stage'},
    const {'1': 'webcam', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.collaborator.Webcam', '9': 0, '10': 'webcam'},
    const {'1': 'authorization', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.collaborator.Authorization', '9': 0, '10': 'authorization'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SKQoEdXNlchgBIAEoCzIVLndoaXRlYm9hcmQuY29yZS5Vc2VyUgR1c2VyEksKCm1pY3JvcGhvbmUYCiABKAsyKS53aGl0ZWJvYXJkLmJvYXJkLmNvbGxhYm9yYXRvci5NaWNyb3Bob25lSABSCm1pY3JvcGhvbmUSPAoFc3RhZ2UYCyABKAsyJC53aGl0ZWJvYXJkLmJvYXJkLmNvbGxhYm9yYXRvci5TdGFnZUgAUgVzdGFnZRI/CgZ3ZWJjYW0YDCABKAsyJS53aGl0ZWJvYXJkLmJvYXJkLmNvbGxhYm9yYXRvci5XZWJjYW1IAFIGd2ViY2FtElQKDWF1dGhvcml6YXRpb24YDSABKAsyLC53aGl0ZWJvYXJkLmJvYXJkLmNvbGxhYm9yYXRvci5BdXRob3JpemF0aW9uSABSDWF1dGhvcml6YXRpb25CCAoGYWN0aW9u');
