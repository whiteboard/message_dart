///
//  Generated code. Do not modify.
//  source: whiteboard/board/collaborator/stage.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use stageDescriptor instead')
const Stage$json = const {
  '1': 'Stage',
  '2': const [
    const {'1': 'switch', '3': 10, '4': 1, '5': 8, '10': 'switch'},
  ],
};

/// Descriptor for `Stage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stageDescriptor = $convert.base64Decode('CgVTdGFnZRIWCgZzd2l0Y2gYCiABKAhSBnN3aXRjaA==');
