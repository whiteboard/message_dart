///
//  Generated code. Do not modify.
//  source: whiteboard/board/collaborator/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user.pb.dart' as $12;
import 'microphone.pb.dart' as $21;
import 'stage.pb.dart' as $22;
import 'webcam.pb.dart' as $23;
import 'authorization.pb.dart' as $24;

enum Action_Action {
  microphone, 
  stage, 
  webcam, 
  authorization, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    10 : Action_Action.microphone,
    11 : Action_Action.stage,
    12 : Action_Action.webcam,
    13 : Action_Action.authorization,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.collaborator'), createEmptyInstance: create)
    ..oo(0, [10, 11, 12, 13])
    ..aOM<$12.User>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $12.User.create)
    ..aOM<$21.Microphone>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'microphone', subBuilder: $21.Microphone.create)
    ..aOM<$22.Stage>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stage', subBuilder: $22.Stage.create)
    ..aOM<$23.Webcam>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'webcam', subBuilder: $23.Webcam.create)
    ..aOM<$24.Authorization>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'authorization', subBuilder: $24.Authorization.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $12.User? user,
    $21.Microphone? microphone,
    $22.Stage? stage,
    $23.Webcam? webcam,
    $24.Authorization? authorization,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    if (microphone != null) {
      _result.microphone = microphone;
    }
    if (stage != null) {
      _result.stage = stage;
    }
    if (webcam != null) {
      _result.webcam = webcam;
    }
    if (authorization != null) {
      _result.authorization = authorization;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $12.User get user => $_getN(0);
  @$pb.TagNumber(1)
  set user($12.User v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  $12.User ensureUser() => $_ensure(0);

  @$pb.TagNumber(10)
  $21.Microphone get microphone => $_getN(1);
  @$pb.TagNumber(10)
  set microphone($21.Microphone v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasMicrophone() => $_has(1);
  @$pb.TagNumber(10)
  void clearMicrophone() => clearField(10);
  @$pb.TagNumber(10)
  $21.Microphone ensureMicrophone() => $_ensure(1);

  @$pb.TagNumber(11)
  $22.Stage get stage => $_getN(2);
  @$pb.TagNumber(11)
  set stage($22.Stage v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasStage() => $_has(2);
  @$pb.TagNumber(11)
  void clearStage() => clearField(11);
  @$pb.TagNumber(11)
  $22.Stage ensureStage() => $_ensure(2);

  @$pb.TagNumber(12)
  $23.Webcam get webcam => $_getN(3);
  @$pb.TagNumber(12)
  set webcam($23.Webcam v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasWebcam() => $_has(3);
  @$pb.TagNumber(12)
  void clearWebcam() => clearField(12);
  @$pb.TagNumber(12)
  $23.Webcam ensureWebcam() => $_ensure(3);

  @$pb.TagNumber(13)
  $24.Authorization get authorization => $_getN(4);
  @$pb.TagNumber(13)
  set authorization($24.Authorization v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasAuthorization() => $_has(4);
  @$pb.TagNumber(13)
  void clearAuthorization() => clearField(13);
  @$pb.TagNumber(13)
  $24.Authorization ensureAuthorization() => $_ensure(4);
}

