///
//  Generated code. Do not modify.
//  source: whiteboard/board/collaborator/microphone.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/device_status.pbenum.dart' as $20;

class Microphone extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Microphone', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.collaborator'), createEmptyInstance: create)
    ..e<$20.DeviceStatus>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $20.DeviceStatus.DEVICE_STATUS_UNSPECIFIED, valueOf: $20.DeviceStatus.valueOf, enumValues: $20.DeviceStatus.values)
    ..hasRequiredFields = false
  ;

  Microphone._() : super();
  factory Microphone({
    $20.DeviceStatus? status,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory Microphone.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Microphone.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Microphone clone() => Microphone()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Microphone copyWith(void Function(Microphone) updates) => super.copyWith((message) => updates(message as Microphone)) as Microphone; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Microphone create() => Microphone._();
  Microphone createEmptyInstance() => create();
  static $pb.PbList<Microphone> createRepeated() => $pb.PbList<Microphone>();
  @$core.pragma('dart2js:noInline')
  static Microphone getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Microphone>(create);
  static Microphone? _defaultInstance;

  @$pb.TagNumber(10)
  $20.DeviceStatus get status => $_getN(0);
  @$pb.TagNumber(10)
  set status($20.DeviceStatus v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(10)
  void clearStatus() => clearField(10);
}

