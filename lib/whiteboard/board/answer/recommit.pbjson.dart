///
//  Generated code. Do not modify.
//  source: whiteboard/board/answer/recommit.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use recommitDescriptor instead')
const Recommit$json = const {
  '1': 'Recommit',
  '2': const [
    const {'1': 'id', '3': 5, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'options', '3': 6, '4': 3, '5': 9, '10': 'options'},
  ],
};

/// Descriptor for `Recommit`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List recommitDescriptor = $convert.base64Decode('CghSZWNvbW1pdBIOCgJpZBgFIAEoA1ICaWQSGAoHb3B0aW9ucxgGIAMoCVIHb3B0aW9ucw==');
