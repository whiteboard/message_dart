///
//  Generated code. Do not modify.
//  source: whiteboard/board/answer/recommit.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class Recommit extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Recommit', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.answer'), createEmptyInstance: create)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..pPS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'options')
    ..hasRequiredFields = false
  ;

  Recommit._() : super();
  factory Recommit({
    $fixnum.Int64? id,
    $core.Iterable<$core.String>? options,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (options != null) {
      _result.options.addAll(options);
    }
    return _result;
  }
  factory Recommit.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Recommit.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Recommit clone() => Recommit()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Recommit copyWith(void Function(Recommit) updates) => super.copyWith((message) => updates(message as Recommit)) as Recommit; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Recommit create() => Recommit._();
  Recommit createEmptyInstance() => create();
  static $pb.PbList<Recommit> createRepeated() => $pb.PbList<Recommit>();
  @$core.pragma('dart2js:noInline')
  static Recommit getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Recommit>(create);
  static Recommit? _defaultInstance;

  @$pb.TagNumber(5)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(5)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(5)
  void clearId() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<$core.String> get options => $_getList(1);
}

