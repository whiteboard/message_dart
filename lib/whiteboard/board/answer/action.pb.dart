///
//  Generated code. Do not modify.
//  source: whiteboard/board/answer/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'commit.pb.dart' as $0;
import 'recommit.pb.dart' as $1;

enum Action_Action {
  commit, 
  recommit, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    10 : Action_Action.commit,
    20 : Action_Action.recommit,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.answer'), createEmptyInstance: create)
    ..oo(0, [10, 20])
    ..aOM<$0.Commit>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commit', subBuilder: $0.Commit.create)
    ..aOM<$1.Recommit>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'recommit', subBuilder: $1.Recommit.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $0.Commit? commit,
    $1.Recommit? recommit,
  }) {
    final _result = create();
    if (commit != null) {
      _result.commit = commit;
    }
    if (recommit != null) {
      _result.recommit = recommit;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(10)
  $0.Commit get commit => $_getN(0);
  @$pb.TagNumber(10)
  set commit($0.Commit v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasCommit() => $_has(0);
  @$pb.TagNumber(10)
  void clearCommit() => clearField(10);
  @$pb.TagNumber(10)
  $0.Commit ensureCommit() => $_ensure(0);

  @$pb.TagNumber(20)
  $1.Recommit get recommit => $_getN(1);
  @$pb.TagNumber(20)
  set recommit($1.Recommit v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasRecommit() => $_has(1);
  @$pb.TagNumber(20)
  void clearRecommit() => clearField(20);
  @$pb.TagNumber(20)
  $1.Recommit ensureRecommit() => $_ensure(1);
}

