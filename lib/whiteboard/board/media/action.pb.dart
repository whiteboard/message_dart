///
//  Generated code. Do not modify.
//  source: whiteboard/board/media/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'open.pb.dart' as $40;
import 'control.pb.dart' as $41;
import 'snapshot.pb.dart' as $42;

import 'type.pbenum.dart' as $43;

enum Action_Action {
  open, 
  play, 
  pause, 
  replay, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    10 : Action_Action.open,
    11 : Action_Action.play,
    12 : Action_Action.pause,
    13 : Action_Action.replay,
    15 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.media'), createEmptyInstance: create)
    ..oo(0, [10, 11, 12, 13, 15])
    ..e<$43.Type>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $43.Type.TYPE_UNSPECIFIED, valueOf: $43.Type.valueOf, enumValues: $43.Type.values)
    ..aOM<$2.Window>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$40.Open>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'open', subBuilder: $40.Open.create)
    ..aOM<$41.Control>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'play', subBuilder: $41.Control.create)
    ..aOM<$41.Control>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pause', subBuilder: $41.Control.create)
    ..aOM<$41.Control>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'replay', subBuilder: $41.Control.create)
    ..aOM<$42.Snapshot>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $42.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $43.Type? type,
    $2.Window? window,
    $40.Open? open,
    $41.Control? play,
    $41.Control? pause,
    $41.Control? replay,
    $42.Snapshot? snapshot,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (window != null) {
      _result.window = window;
    }
    if (open != null) {
      _result.open = open;
    }
    if (play != null) {
      _result.play = play;
    }
    if (pause != null) {
      _result.pause = pause;
    }
    if (replay != null) {
      _result.replay = replay;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $43.Type get type => $_getN(0);
  @$pb.TagNumber(3)
  set type($43.Type v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(3)
  void clearType() => clearField(3);

  @$pb.TagNumber(4)
  $2.Window get window => $_getN(1);
  @$pb.TagNumber(4)
  set window($2.Window v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasWindow() => $_has(1);
  @$pb.TagNumber(4)
  void clearWindow() => clearField(4);
  @$pb.TagNumber(4)
  $2.Window ensureWindow() => $_ensure(1);

  @$pb.TagNumber(10)
  $40.Open get open => $_getN(2);
  @$pb.TagNumber(10)
  set open($40.Open v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasOpen() => $_has(2);
  @$pb.TagNumber(10)
  void clearOpen() => clearField(10);
  @$pb.TagNumber(10)
  $40.Open ensureOpen() => $_ensure(2);

  @$pb.TagNumber(11)
  $41.Control get play => $_getN(3);
  @$pb.TagNumber(11)
  set play($41.Control v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasPlay() => $_has(3);
  @$pb.TagNumber(11)
  void clearPlay() => clearField(11);
  @$pb.TagNumber(11)
  $41.Control ensurePlay() => $_ensure(3);

  @$pb.TagNumber(12)
  $41.Control get pause => $_getN(4);
  @$pb.TagNumber(12)
  set pause($41.Control v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasPause() => $_has(4);
  @$pb.TagNumber(12)
  void clearPause() => clearField(12);
  @$pb.TagNumber(12)
  $41.Control ensurePause() => $_ensure(4);

  @$pb.TagNumber(13)
  $41.Control get replay => $_getN(5);
  @$pb.TagNumber(13)
  set replay($41.Control v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasReplay() => $_has(5);
  @$pb.TagNumber(13)
  void clearReplay() => clearField(13);
  @$pb.TagNumber(13)
  $41.Control ensureReplay() => $_ensure(5);

  @$pb.TagNumber(15)
  $42.Snapshot get snapshot => $_getN(6);
  @$pb.TagNumber(15)
  set snapshot($42.Snapshot v) { setField(15, v); }
  @$pb.TagNumber(15)
  $core.bool hasSnapshot() => $_has(6);
  @$pb.TagNumber(15)
  void clearSnapshot() => clearField(15);
  @$pb.TagNumber(15)
  $42.Snapshot ensureSnapshot() => $_ensure(6);
}

