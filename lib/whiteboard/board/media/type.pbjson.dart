///
//  Generated code. Do not modify.
//  source: whiteboard/board/media/type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use typeDescriptor instead')
const Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'TYPE_UNSPECIFIED', '2': 0},
    const {'1': 'VIDEO', '2': 1},
    const {'1': 'AUDIO', '2': 2},
    const {'1': 'IMAGE', '2': 3},
  ],
};

/// Descriptor for `Type`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List typeDescriptor = $convert.base64Decode('CgRUeXBlEhQKEFRZUEVfVU5TUEVDSUZJRUQQABIJCgVWSURFTxABEgkKBUFVRElPEAISCQoFSU1BR0UQAw==');
