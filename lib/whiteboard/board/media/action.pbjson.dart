///
//  Generated code. Do not modify.
//  source: whiteboard/board/media/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'type', '3': 3, '4': 1, '5': 14, '6': '.whiteboard.board.media.Type', '10': 'type'},
    const {'1': 'window', '3': 4, '4': 1, '5': 11, '6': '.whiteboard.core.Window', '10': 'window'},
    const {'1': 'open', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.media.Open', '9': 0, '10': 'open'},
    const {'1': 'play', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.media.Control', '9': 0, '10': 'play'},
    const {'1': 'pause', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.media.Control', '9': 0, '10': 'pause'},
    const {'1': 'replay', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.media.Control', '9': 0, '10': 'replay'},
    const {'1': 'snapshot', '3': 15, '4': 1, '5': 11, '6': '.whiteboard.board.media.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SMAoEdHlwZRgDIAEoDjIcLndoaXRlYm9hcmQuYm9hcmQubWVkaWEuVHlwZVIEdHlwZRIvCgZ3aW5kb3cYBCABKAsyFy53aGl0ZWJvYXJkLmNvcmUuV2luZG93UgZ3aW5kb3cSMgoEb3BlbhgKIAEoCzIcLndoaXRlYm9hcmQuYm9hcmQubWVkaWEuT3BlbkgAUgRvcGVuEjUKBHBsYXkYCyABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLm1lZGlhLkNvbnRyb2xIAFIEcGxheRI3CgVwYXVzZRgMIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQubWVkaWEuQ29udHJvbEgAUgVwYXVzZRI5CgZyZXBsYXkYDSABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLm1lZGlhLkNvbnRyb2xIAFIGcmVwbGF5Ej4KCHNuYXBzaG90GA8gASgLMiAud2hpdGVib2FyZC5ib2FyZC5tZWRpYS5TbmFwc2hvdEgAUghzbmFwc2hvdEIICgZhY3Rpb24=');
