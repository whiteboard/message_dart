///
//  Generated code. Do not modify.
//  source: whiteboard/board/media/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/file.pb.dart' as $37;
import '../../../google/protobuf/duration.pb.dart' as $38;

import 'status.pbenum.dart' as $39;

class Snapshot extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Snapshot', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.media'), createEmptyInstance: create)
    ..aOM<$37.File>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'file', subBuilder: $37.File.create)
    ..aOM<$38.Duration>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', subBuilder: $38.Duration.create)
    ..e<$39.Status>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $39.Status.STATUS_UNSPECIFIED, valueOf: $39.Status.valueOf, enumValues: $39.Status.values)
    ..hasRequiredFields = false
  ;

  Snapshot._() : super();
  factory Snapshot({
    $37.File? file,
    $38.Duration? duration,
    $39.Status? status,
  }) {
    final _result = create();
    if (file != null) {
      _result.file = file;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory Snapshot.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Snapshot.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Snapshot clone() => Snapshot()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Snapshot copyWith(void Function(Snapshot) updates) => super.copyWith((message) => updates(message as Snapshot)) as Snapshot; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Snapshot create() => Snapshot._();
  Snapshot createEmptyInstance() => create();
  static $pb.PbList<Snapshot> createRepeated() => $pb.PbList<Snapshot>();
  @$core.pragma('dart2js:noInline')
  static Snapshot getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Snapshot>(create);
  static Snapshot? _defaultInstance;

  @$pb.TagNumber(3)
  $37.File get file => $_getN(0);
  @$pb.TagNumber(3)
  set file($37.File v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFile() => $_has(0);
  @$pb.TagNumber(3)
  void clearFile() => clearField(3);
  @$pb.TagNumber(3)
  $37.File ensureFile() => $_ensure(0);

  @$pb.TagNumber(4)
  $38.Duration get duration => $_getN(1);
  @$pb.TagNumber(4)
  set duration($38.Duration v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDuration() => $_has(1);
  @$pb.TagNumber(4)
  void clearDuration() => clearField(4);
  @$pb.TagNumber(4)
  $38.Duration ensureDuration() => $_ensure(1);

  @$pb.TagNumber(5)
  $39.Status get status => $_getN(2);
  @$pb.TagNumber(5)
  set status($39.Status v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(5)
  void clearStatus() => clearField(5);
}

