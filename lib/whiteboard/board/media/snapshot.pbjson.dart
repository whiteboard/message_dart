///
//  Generated code. Do not modify.
//  source: whiteboard/board/media/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'file', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.File', '10': 'file'},
    const {'1': 'duration', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Duration', '10': 'duration'},
    const {'1': 'status', '3': 5, '4': 1, '5': 14, '6': '.whiteboard.board.media.Status', '10': 'status'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBIpCgRmaWxlGAMgASgLMhUud2hpdGVib2FyZC5jb3JlLkZpbGVSBGZpbGUSNQoIZHVyYXRpb24YBCABKAsyGS5nb29nbGUucHJvdG9idWYuRHVyYXRpb25SCGR1cmF0aW9uEjYKBnN0YXR1cxgFIAEoDjIeLndoaXRlYm9hcmQuYm9hcmQubWVkaWEuU3RhdHVzUgZzdGF0dXM=');
