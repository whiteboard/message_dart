///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 1, '4': 1, '5': 11, '6': '.whiteboard.core.WindowInfo', '10': 'window'},
    const {'1': 'resize', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.board.window.Resize', '9': 0, '10': 'resize'},
    const {'1': 'scroll', '3': 4, '4': 1, '5': 11, '6': '.whiteboard.board.window.Scroll', '9': 0, '10': 'scroll'},
    const {'1': 'move', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.window.Move', '9': 0, '10': 'move'},
    const {'1': 'close', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.window.Close', '9': 0, '10': 'close'},
    const {'1': 'maximize', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.window.Maximize', '9': 0, '10': 'maximize'},
    const {'1': 'minimize', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.window.Minimize', '9': 0, '10': 'minimize'},
    const {'1': 'restore', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.window.Restore', '9': 0, '10': 'restore'},
    const {'1': 'focus', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.window.Focus', '9': 0, '10': 'focus'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SMwoGd2luZG93GAEgASgLMhsud2hpdGVib2FyZC5jb3JlLldpbmRvd0luZm9SBndpbmRvdxI5CgZyZXNpemUYAyABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLndpbmRvdy5SZXNpemVIAFIGcmVzaXplEjkKBnNjcm9sbBgEIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQud2luZG93LlNjcm9sbEgAUgZzY3JvbGwSMwoEbW92ZRgFIAEoCzIdLndoaXRlYm9hcmQuYm9hcmQud2luZG93Lk1vdmVIAFIEbW92ZRI2CgVjbG9zZRgGIAEoCzIeLndoaXRlYm9hcmQuYm9hcmQud2luZG93LkNsb3NlSABSBWNsb3NlEj8KCG1heGltaXplGAogASgLMiEud2hpdGVib2FyZC5ib2FyZC53aW5kb3cuTWF4aW1pemVIAFIIbWF4aW1pemUSPwoIbWluaW1pemUYCyABKAsyIS53aGl0ZWJvYXJkLmJvYXJkLndpbmRvdy5NaW5pbWl6ZUgAUghtaW5pbWl6ZRI8CgdyZXN0b3JlGAwgASgLMiAud2hpdGVib2FyZC5ib2FyZC53aW5kb3cuUmVzdG9yZUgAUgdyZXN0b3JlEjYKBWZvY3VzGA0gASgLMh4ud2hpdGVib2FyZC5ib2FyZC53aW5kb3cuRm9jdXNIAFIFZm9jdXNCCAoGYWN0aW9u');
