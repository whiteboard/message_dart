///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/resize.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/size.pb.dart' as $100;

class Resize extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Resize', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.window'), createEmptyInstance: create)
    ..aOM<$100.Size>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', subBuilder: $100.Size.create)
    ..hasRequiredFields = false
  ;

  Resize._() : super();
  factory Resize({
    $100.Size? size,
  }) {
    final _result = create();
    if (size != null) {
      _result.size = size;
    }
    return _result;
  }
  factory Resize.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Resize.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Resize clone() => Resize()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Resize copyWith(void Function(Resize) updates) => super.copyWith((message) => updates(message as Resize)) as Resize; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Resize create() => Resize._();
  Resize createEmptyInstance() => create();
  static $pb.PbList<Resize> createRepeated() => $pb.PbList<Resize>();
  @$core.pragma('dart2js:noInline')
  static Resize getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Resize>(create);
  static Resize? _defaultInstance;

  @$pb.TagNumber(5)
  $100.Size get size => $_getN(0);
  @$pb.TagNumber(5)
  set size($100.Size v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSize() => $_has(0);
  @$pb.TagNumber(5)
  void clearSize() => clearField(5);
  @$pb.TagNumber(5)
  $100.Size ensureSize() => $_ensure(0);
}

