///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/scroll.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Scroll extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Scroll', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.window'), createEmptyInstance: create)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ratio', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Scroll._() : super();
  factory Scroll({
    $core.double? ratio,
  }) {
    final _result = create();
    if (ratio != null) {
      _result.ratio = ratio;
    }
    return _result;
  }
  factory Scroll.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Scroll.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Scroll clone() => Scroll()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Scroll copyWith(void Function(Scroll) updates) => super.copyWith((message) => updates(message as Scroll)) as Scroll; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Scroll create() => Scroll._();
  Scroll createEmptyInstance() => create();
  static $pb.PbList<Scroll> createRepeated() => $pb.PbList<Scroll>();
  @$core.pragma('dart2js:noInline')
  static Scroll getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Scroll>(create);
  static Scroll? _defaultInstance;

  @$pb.TagNumber(5)
  $core.double get ratio => $_getN(0);
  @$pb.TagNumber(5)
  set ratio($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasRatio() => $_has(0);
  @$pb.TagNumber(5)
  void clearRatio() => clearField(5);
}

