///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/focus.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Focus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Focus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.window'), createEmptyInstance: create)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zIndex', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Focus._() : super();
  factory Focus({
    $core.int? zIndex,
  }) {
    final _result = create();
    if (zIndex != null) {
      _result.zIndex = zIndex;
    }
    return _result;
  }
  factory Focus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Focus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Focus clone() => Focus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Focus copyWith(void Function(Focus) updates) => super.copyWith((message) => updates(message as Focus)) as Focus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Focus create() => Focus._();
  Focus createEmptyInstance() => create();
  static $pb.PbList<Focus> createRepeated() => $pb.PbList<Focus>();
  @$core.pragma('dart2js:noInline')
  static Focus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Focus>(create);
  static Focus? _defaultInstance;

  @$pb.TagNumber(5)
  $core.int get zIndex => $_getIZ(0);
  @$pb.TagNumber(5)
  set zIndex($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasZIndex() => $_has(0);
  @$pb.TagNumber(5)
  void clearZIndex() => clearField(5);
}

