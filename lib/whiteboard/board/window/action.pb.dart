///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window_info.pb.dart' as $50;
import 'resize.pb.dart' as $101;
import 'scroll.pb.dart' as $102;
import 'move.pb.dart' as $103;
import 'close.pb.dart' as $104;
import 'maximize.pb.dart' as $105;
import 'minimize.pb.dart' as $106;
import 'restore.pb.dart' as $107;
import 'focus.pb.dart' as $108;

enum Action_Action {
  resize, 
  scroll, 
  move, 
  close, 
  maximize, 
  minimize, 
  restore, 
  focus, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    3 : Action_Action.resize,
    4 : Action_Action.scroll,
    5 : Action_Action.move,
    6 : Action_Action.close,
    10 : Action_Action.maximize,
    11 : Action_Action.minimize,
    12 : Action_Action.restore,
    13 : Action_Action.focus,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.window'), createEmptyInstance: create)
    ..oo(0, [3, 4, 5, 6, 10, 11, 12, 13])
    ..aOM<$50.WindowInfo>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $50.WindowInfo.create)
    ..aOM<$101.Resize>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'resize', subBuilder: $101.Resize.create)
    ..aOM<$102.Scroll>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'scroll', subBuilder: $102.Scroll.create)
    ..aOM<$103.Move>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'move', subBuilder: $103.Move.create)
    ..aOM<$104.Close>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'close', subBuilder: $104.Close.create)
    ..aOM<$105.Maximize>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maximize', subBuilder: $105.Maximize.create)
    ..aOM<$106.Minimize>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minimize', subBuilder: $106.Minimize.create)
    ..aOM<$107.Restore>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'restore', subBuilder: $107.Restore.create)
    ..aOM<$108.Focus>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'focus', subBuilder: $108.Focus.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $50.WindowInfo? window,
    $101.Resize? resize,
    $102.Scroll? scroll,
    $103.Move? move,
    $104.Close? close,
    $105.Maximize? maximize,
    $106.Minimize? minimize,
    $107.Restore? restore,
    $108.Focus? focus,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (resize != null) {
      _result.resize = resize;
    }
    if (scroll != null) {
      _result.scroll = scroll;
    }
    if (move != null) {
      _result.move = move;
    }
    if (close != null) {
      _result.close = close;
    }
    if (maximize != null) {
      _result.maximize = maximize;
    }
    if (minimize != null) {
      _result.minimize = minimize;
    }
    if (restore != null) {
      _result.restore = restore;
    }
    if (focus != null) {
      _result.focus = focus;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $50.WindowInfo get window => $_getN(0);
  @$pb.TagNumber(1)
  set window($50.WindowInfo v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(1)
  void clearWindow() => clearField(1);
  @$pb.TagNumber(1)
  $50.WindowInfo ensureWindow() => $_ensure(0);

  @$pb.TagNumber(3)
  $101.Resize get resize => $_getN(1);
  @$pb.TagNumber(3)
  set resize($101.Resize v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasResize() => $_has(1);
  @$pb.TagNumber(3)
  void clearResize() => clearField(3);
  @$pb.TagNumber(3)
  $101.Resize ensureResize() => $_ensure(1);

  @$pb.TagNumber(4)
  $102.Scroll get scroll => $_getN(2);
  @$pb.TagNumber(4)
  set scroll($102.Scroll v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasScroll() => $_has(2);
  @$pb.TagNumber(4)
  void clearScroll() => clearField(4);
  @$pb.TagNumber(4)
  $102.Scroll ensureScroll() => $_ensure(2);

  @$pb.TagNumber(5)
  $103.Move get move => $_getN(3);
  @$pb.TagNumber(5)
  set move($103.Move v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMove() => $_has(3);
  @$pb.TagNumber(5)
  void clearMove() => clearField(5);
  @$pb.TagNumber(5)
  $103.Move ensureMove() => $_ensure(3);

  @$pb.TagNumber(6)
  $104.Close get close => $_getN(4);
  @$pb.TagNumber(6)
  set close($104.Close v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasClose() => $_has(4);
  @$pb.TagNumber(6)
  void clearClose() => clearField(6);
  @$pb.TagNumber(6)
  $104.Close ensureClose() => $_ensure(4);

  @$pb.TagNumber(10)
  $105.Maximize get maximize => $_getN(5);
  @$pb.TagNumber(10)
  set maximize($105.Maximize v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasMaximize() => $_has(5);
  @$pb.TagNumber(10)
  void clearMaximize() => clearField(10);
  @$pb.TagNumber(10)
  $105.Maximize ensureMaximize() => $_ensure(5);

  @$pb.TagNumber(11)
  $106.Minimize get minimize => $_getN(6);
  @$pb.TagNumber(11)
  set minimize($106.Minimize v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasMinimize() => $_has(6);
  @$pb.TagNumber(11)
  void clearMinimize() => clearField(11);
  @$pb.TagNumber(11)
  $106.Minimize ensureMinimize() => $_ensure(6);

  @$pb.TagNumber(12)
  $107.Restore get restore => $_getN(7);
  @$pb.TagNumber(12)
  set restore($107.Restore v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasRestore() => $_has(7);
  @$pb.TagNumber(12)
  void clearRestore() => clearField(12);
  @$pb.TagNumber(12)
  $107.Restore ensureRestore() => $_ensure(7);

  @$pb.TagNumber(13)
  $108.Focus get focus => $_getN(8);
  @$pb.TagNumber(13)
  set focus($108.Focus v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasFocus() => $_has(8);
  @$pb.TagNumber(13)
  void clearFocus() => clearField(13);
  @$pb.TagNumber(13)
  $108.Focus ensureFocus() => $_ensure(8);
}

