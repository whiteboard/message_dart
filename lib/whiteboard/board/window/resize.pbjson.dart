///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/resize.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use resizeDescriptor instead')
const Resize$json = const {
  '1': 'Resize',
  '2': const [
    const {'1': 'size', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.core.Size', '10': 'size'},
  ],
};

/// Descriptor for `Resize`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resizeDescriptor = $convert.base64Decode('CgZSZXNpemUSKQoEc2l6ZRgFIAEoCzIVLndoaXRlYm9hcmQuY29yZS5TaXplUgRzaXpl');
