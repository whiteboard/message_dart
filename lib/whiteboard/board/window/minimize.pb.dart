///
//  Generated code. Do not modify.
//  source: whiteboard/board/window/minimize.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Minimize extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Minimize', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.window'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Minimize._() : super();
  factory Minimize() => create();
  factory Minimize.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Minimize.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Minimize clone() => Minimize()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Minimize copyWith(void Function(Minimize) updates) => super.copyWith((message) => updates(message as Minimize)) as Minimize; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Minimize create() => Minimize._();
  Minimize createEmptyInstance() => create();
  static $pb.PbList<Minimize> createRepeated() => $pb.PbList<Minimize>();
  @$core.pragma('dart2js:noInline')
  static Minimize getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Minimize>(create);
  static Minimize? _defaultInstance;
}

