///
//  Generated code. Do not modify.
//  source: whiteboard/board/floating/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'palette.pb.dart' as $31;
import 'snapshot.pb.dart' as $32;

enum Action_Action {
  palette, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.palette,
    12 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.floating'), createEmptyInstance: create)
    ..oo(0, [5, 12])
    ..aOM<$2.Window>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$31.Palette>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'palette', subBuilder: $31.Palette.create)
    ..aOM<$32.Snapshot>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $32.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $2.Window? window,
    $31.Palette? palette,
    $32.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (palette != null) {
      _result.palette = palette;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $2.Window get window => $_getN(0);
  @$pb.TagNumber(3)
  set window($2.Window v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(3)
  void clearWindow() => clearField(3);
  @$pb.TagNumber(3)
  $2.Window ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $31.Palette get palette => $_getN(1);
  @$pb.TagNumber(5)
  set palette($31.Palette v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasPalette() => $_has(1);
  @$pb.TagNumber(5)
  void clearPalette() => clearField(5);
  @$pb.TagNumber(5)
  $31.Palette ensurePalette() => $_ensure(1);

  @$pb.TagNumber(12)
  $32.Snapshot get snapshot => $_getN(2);
  @$pb.TagNumber(12)
  set snapshot($32.Snapshot v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasSnapshot() => $_has(2);
  @$pb.TagNumber(12)
  void clearSnapshot() => clearField(12);
  @$pb.TagNumber(12)
  $32.Snapshot ensureSnapshot() => $_ensure(2);
}

