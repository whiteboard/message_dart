///
//  Generated code. Do not modify.
//  source: whiteboard/board/floating/palette.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use paletteDescriptor instead')
const Palette$json = const {
  '1': 'Palette',
  '2': const [
    const {'1': 'user', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.core.User', '10': 'user'},
  ],
};

/// Descriptor for `Palette`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List paletteDescriptor = $convert.base64Decode('CgdQYWxldHRlEikKBHVzZXIYBSABKAsyFS53aGl0ZWJvYXJkLmNvcmUuVXNlclIEdXNlcg==');
