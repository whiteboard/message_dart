///
//  Generated code. Do not modify.
//  source: whiteboard/board/floating/palette.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user.pb.dart' as $12;

class Palette extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Palette', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.floating'), createEmptyInstance: create)
    ..aOM<$12.User>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $12.User.create)
    ..hasRequiredFields = false
  ;

  Palette._() : super();
  factory Palette({
    $12.User? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory Palette.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Palette.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Palette clone() => Palette()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Palette copyWith(void Function(Palette) updates) => super.copyWith((message) => updates(message as Palette)) as Palette; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Palette create() => Palette._();
  Palette createEmptyInstance() => create();
  static $pb.PbList<Palette> createRepeated() => $pb.PbList<Palette>();
  @$core.pragma('dart2js:noInline')
  static Palette getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Palette>(create);
  static Palette? _defaultInstance;

  @$pb.TagNumber(5)
  $12.User get user => $_getN(0);
  @$pb.TagNumber(5)
  set user($12.User v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(5)
  void clearUser() => clearField(5);
  @$pb.TagNumber(5)
  $12.User ensureUser() => $_ensure(0);
}

