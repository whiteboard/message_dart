///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import '../../../google/protobuf/timestamp.pb.dart' as $74;
import 'statistics.pb.dart' as $75;

import 'status.pbenum.dart' as $76;

class Snapshot extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Snapshot', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.qa'), createEmptyInstance: create)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOM<$74.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'started', subBuilder: $74.Timestamp.create)
    ..aOM<$74.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ended', subBuilder: $74.Timestamp.create)
    ..pPS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'options')
    ..pPS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'corrects')
    ..e<$76.Status>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $76.Status.STATUS_UNSPECIFIED, valueOf: $76.Status.valueOf, enumValues: $76.Status.values)
    ..aOM<$75.Statistics>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statistics', subBuilder: $75.Statistics.create)
    ..hasRequiredFields = false
  ;

  Snapshot._() : super();
  factory Snapshot({
    $fixnum.Int64? id,
    $74.Timestamp? started,
    $74.Timestamp? ended,
    $core.Iterable<$core.String>? options,
    $core.Iterable<$core.String>? corrects,
    $76.Status? status,
    $75.Statistics? statistics,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (started != null) {
      _result.started = started;
    }
    if (ended != null) {
      _result.ended = ended;
    }
    if (options != null) {
      _result.options.addAll(options);
    }
    if (corrects != null) {
      _result.corrects.addAll(corrects);
    }
    if (status != null) {
      _result.status = status;
    }
    if (statistics != null) {
      _result.statistics = statistics;
    }
    return _result;
  }
  factory Snapshot.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Snapshot.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Snapshot clone() => Snapshot()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Snapshot copyWith(void Function(Snapshot) updates) => super.copyWith((message) => updates(message as Snapshot)) as Snapshot; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Snapshot create() => Snapshot._();
  Snapshot createEmptyInstance() => create();
  static $pb.PbList<Snapshot> createRepeated() => $pb.PbList<Snapshot>();
  @$core.pragma('dart2js:noInline')
  static Snapshot getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Snapshot>(create);
  static Snapshot? _defaultInstance;

  @$pb.TagNumber(2)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(2)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $74.Timestamp get started => $_getN(1);
  @$pb.TagNumber(3)
  set started($74.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStarted() => $_has(1);
  @$pb.TagNumber(3)
  void clearStarted() => clearField(3);
  @$pb.TagNumber(3)
  $74.Timestamp ensureStarted() => $_ensure(1);

  @$pb.TagNumber(4)
  $74.Timestamp get ended => $_getN(2);
  @$pb.TagNumber(4)
  set ended($74.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasEnded() => $_has(2);
  @$pb.TagNumber(4)
  void clearEnded() => clearField(4);
  @$pb.TagNumber(4)
  $74.Timestamp ensureEnded() => $_ensure(2);

  @$pb.TagNumber(5)
  $core.List<$core.String> get options => $_getList(3);

  @$pb.TagNumber(6)
  $core.List<$core.String> get corrects => $_getList(4);

  @$pb.TagNumber(7)
  $76.Status get status => $_getN(5);
  @$pb.TagNumber(7)
  set status($76.Status v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasStatus() => $_has(5);
  @$pb.TagNumber(7)
  void clearStatus() => clearField(7);

  @$pb.TagNumber(8)
  $75.Statistics get statistics => $_getN(6);
  @$pb.TagNumber(8)
  set statistics($75.Statistics v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasStatistics() => $_has(6);
  @$pb.TagNumber(8)
  void clearStatistics() => clearField(8);
  @$pb.TagNumber(8)
  $75.Statistics ensureStatistics() => $_ensure(6);
}

