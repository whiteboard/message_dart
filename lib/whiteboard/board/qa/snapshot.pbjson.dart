///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'id', '3': 2, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'started', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'started'},
    const {'1': 'ended', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ended'},
    const {'1': 'options', '3': 5, '4': 3, '5': 9, '10': 'options'},
    const {'1': 'corrects', '3': 6, '4': 3, '5': 9, '10': 'corrects'},
    const {'1': 'status', '3': 7, '4': 1, '5': 14, '6': '.whiteboard.board.qa.Status', '10': 'status'},
    const {'1': 'statistics', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.qa.Statistics', '10': 'statistics'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBIOCgJpZBgCIAEoA1ICaWQSNAoHc3RhcnRlZBgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSB3N0YXJ0ZWQSMAoFZW5kZWQYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgVlbmRlZBIYCgdvcHRpb25zGAUgAygJUgdvcHRpb25zEhoKCGNvcnJlY3RzGAYgAygJUghjb3JyZWN0cxIzCgZzdGF0dXMYByABKA4yGy53aGl0ZWJvYXJkLmJvYXJkLnFhLlN0YXR1c1IGc3RhdHVzEj8KCnN0YXRpc3RpY3MYCCABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLnFhLlN0YXRpc3RpY3NSCnN0YXRpc3RpY3M=');
