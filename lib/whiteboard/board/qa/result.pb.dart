///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/result.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import '../../../google/protobuf/timestamp.pb.dart' as $74;
import 'statistics.pb.dart' as $75;

class Result extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Result', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.qa'), createEmptyInstance: create)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOM<$74.Timestamp>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'started', subBuilder: $74.Timestamp.create)
    ..aOM<$74.Timestamp>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ended', subBuilder: $74.Timestamp.create)
    ..pPS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'options')
    ..pPS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'corrects')
    ..aOM<$75.Statistics>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statistics', subBuilder: $75.Statistics.create)
    ..hasRequiredFields = false
  ;

  Result._() : super();
  factory Result({
    $fixnum.Int64? id,
    $74.Timestamp? started,
    $74.Timestamp? ended,
    $core.Iterable<$core.String>? options,
    $core.Iterable<$core.String>? corrects,
    $75.Statistics? statistics,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (started != null) {
      _result.started = started;
    }
    if (ended != null) {
      _result.ended = ended;
    }
    if (options != null) {
      _result.options.addAll(options);
    }
    if (corrects != null) {
      _result.corrects.addAll(corrects);
    }
    if (statistics != null) {
      _result.statistics = statistics;
    }
    return _result;
  }
  factory Result.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Result.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Result clone() => Result()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Result copyWith(void Function(Result) updates) => super.copyWith((message) => updates(message as Result)) as Result; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Result create() => Result._();
  Result createEmptyInstance() => create();
  static $pb.PbList<Result> createRepeated() => $pb.PbList<Result>();
  @$core.pragma('dart2js:noInline')
  static Result getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Result>(create);
  static Result? _defaultInstance;

  @$pb.TagNumber(5)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(5)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(5)
  void clearId() => clearField(5);

  @$pb.TagNumber(6)
  $74.Timestamp get started => $_getN(1);
  @$pb.TagNumber(6)
  set started($74.Timestamp v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasStarted() => $_has(1);
  @$pb.TagNumber(6)
  void clearStarted() => clearField(6);
  @$pb.TagNumber(6)
  $74.Timestamp ensureStarted() => $_ensure(1);

  @$pb.TagNumber(7)
  $74.Timestamp get ended => $_getN(2);
  @$pb.TagNumber(7)
  set ended($74.Timestamp v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasEnded() => $_has(2);
  @$pb.TagNumber(7)
  void clearEnded() => clearField(7);
  @$pb.TagNumber(7)
  $74.Timestamp ensureEnded() => $_ensure(2);

  @$pb.TagNumber(10)
  $core.List<$core.String> get options => $_getList(3);

  @$pb.TagNumber(11)
  $core.List<$core.String> get corrects => $_getList(4);

  @$pb.TagNumber(12)
  $75.Statistics get statistics => $_getN(5);
  @$pb.TagNumber(12)
  set statistics($75.Statistics v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasStatistics() => $_has(5);
  @$pb.TagNumber(12)
  void clearStatistics() => clearField(12);
  @$pb.TagNumber(12)
  $75.Statistics ensureStatistics() => $_ensure(5);
}

