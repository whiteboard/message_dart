///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'start.pb.dart' as $77;
import 'end.pb.dart' as $78;
import 'result.pb.dart' as $79;
import 'created.pb.dart' as $80;
import 'snapshot.pb.dart' as $81;

enum Action_Action {
  start, 
  end, 
  result, 
  created, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.start,
    6 : Action_Action.end,
    7 : Action_Action.result,
    8 : Action_Action.created,
    10 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.qa'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 8, 10])
    ..aOM<$2.Window>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$77.Start>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $77.Start.create)
    ..aOM<$78.End>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: $78.End.create)
    ..aOM<$79.Result>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'result', subBuilder: $79.Result.create)
    ..aOM<$80.Created>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'created', subBuilder: $80.Created.create)
    ..aOM<$81.Snapshot>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $81.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $2.Window? window,
    $77.Start? start,
    $78.End? end,
    $79.Result? result,
    $80.Created? created,
    $81.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (start != null) {
      _result.start = start;
    }
    if (end != null) {
      _result.end = end;
    }
    if (result != null) {
      _result.result = result;
    }
    if (created != null) {
      _result.created = created;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $2.Window get window => $_getN(0);
  @$pb.TagNumber(3)
  set window($2.Window v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(3)
  void clearWindow() => clearField(3);
  @$pb.TagNumber(3)
  $2.Window ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $77.Start get start => $_getN(1);
  @$pb.TagNumber(5)
  set start($77.Start v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasStart() => $_has(1);
  @$pb.TagNumber(5)
  void clearStart() => clearField(5);
  @$pb.TagNumber(5)
  $77.Start ensureStart() => $_ensure(1);

  @$pb.TagNumber(6)
  $78.End get end => $_getN(2);
  @$pb.TagNumber(6)
  set end($78.End v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasEnd() => $_has(2);
  @$pb.TagNumber(6)
  void clearEnd() => clearField(6);
  @$pb.TagNumber(6)
  $78.End ensureEnd() => $_ensure(2);

  @$pb.TagNumber(7)
  $79.Result get result => $_getN(3);
  @$pb.TagNumber(7)
  set result($79.Result v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasResult() => $_has(3);
  @$pb.TagNumber(7)
  void clearResult() => clearField(7);
  @$pb.TagNumber(7)
  $79.Result ensureResult() => $_ensure(3);

  @$pb.TagNumber(8)
  $80.Created get created => $_getN(4);
  @$pb.TagNumber(8)
  set created($80.Created v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasCreated() => $_has(4);
  @$pb.TagNumber(8)
  void clearCreated() => clearField(8);
  @$pb.TagNumber(8)
  $80.Created ensureCreated() => $_ensure(4);

  @$pb.TagNumber(10)
  $81.Snapshot get snapshot => $_getN(5);
  @$pb.TagNumber(10)
  set snapshot($81.Snapshot v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSnapshot() => $_has(5);
  @$pb.TagNumber(10)
  void clearSnapshot() => clearField(10);
  @$pb.TagNumber(10)
  $81.Snapshot ensureSnapshot() => $_ensure(5);
}

