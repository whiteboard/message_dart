///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/result.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use resultDescriptor instead')
const Result$json = const {
  '1': 'Result',
  '2': const [
    const {'1': 'id', '3': 5, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'started', '3': 6, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'started'},
    const {'1': 'ended', '3': 7, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ended'},
    const {'1': 'options', '3': 10, '4': 3, '5': 9, '10': 'options'},
    const {'1': 'corrects', '3': 11, '4': 3, '5': 9, '10': 'corrects'},
    const {'1': 'statistics', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.qa.Statistics', '10': 'statistics'},
  ],
};

/// Descriptor for `Result`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resultDescriptor = $convert.base64Decode('CgZSZXN1bHQSDgoCaWQYBSABKANSAmlkEjQKB3N0YXJ0ZWQYBiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgdzdGFydGVkEjAKBWVuZGVkGAcgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIFZW5kZWQSGAoHb3B0aW9ucxgKIAMoCVIHb3B0aW9ucxIaCghjb3JyZWN0cxgLIAMoCVIIY29ycmVjdHMSPwoKc3RhdGlzdGljcxgMIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucWEuU3RhdGlzdGljc1IKc3RhdGlzdGljcw==');
