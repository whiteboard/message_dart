///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/statistics.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use statisticsDescriptor instead')
const Statistics$json = const {
  '1': 'Statistics',
  '2': const [
    const {'1': 'total', '3': 5, '4': 1, '5': 5, '10': 'total'},
    const {'1': 'correct', '3': 6, '4': 1, '5': 5, '10': 'correct'},
    const {'1': 'answers', '3': 7, '4': 3, '5': 11, '6': '.whiteboard.board.qa.Statistics.AnswersEntry', '10': 'answers'},
  ],
  '3': const [Statistics_AnswersEntry$json],
};

@$core.Deprecated('Use statisticsDescriptor instead')
const Statistics_AnswersEntry$json = const {
  '1': 'AnswersEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 5, '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Statistics`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statisticsDescriptor = $convert.base64Decode('CgpTdGF0aXN0aWNzEhQKBXRvdGFsGAUgASgFUgV0b3RhbBIYCgdjb3JyZWN0GAYgASgFUgdjb3JyZWN0EkYKB2Fuc3dlcnMYByADKAsyLC53aGl0ZWJvYXJkLmJvYXJkLnFhLlN0YXRpc3RpY3MuQW5zd2Vyc0VudHJ5UgdhbnN3ZXJzGjoKDEFuc3dlcnNFbnRyeRIQCgNrZXkYASABKAlSA2tleRIUCgV2YWx1ZRgCIAEoBVIFdmFsdWU6AjgB');
