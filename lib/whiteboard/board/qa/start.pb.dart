///
//  Generated code. Do not modify.
//  source: whiteboard/board/qa/start.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Start extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Start', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.qa'), createEmptyInstance: create)
    ..pPS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'options')
    ..pPS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'corrects')
    ..hasRequiredFields = false
  ;

  Start._() : super();
  factory Start({
    $core.Iterable<$core.String>? options,
    $core.Iterable<$core.String>? corrects,
  }) {
    final _result = create();
    if (options != null) {
      _result.options.addAll(options);
    }
    if (corrects != null) {
      _result.corrects.addAll(corrects);
    }
    return _result;
  }
  factory Start.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Start.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Start clone() => Start()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Start copyWith(void Function(Start) updates) => super.copyWith((message) => updates(message as Start)) as Start; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Start create() => Start._();
  Start createEmptyInstance() => create();
  static $pb.PbList<Start> createRepeated() => $pb.PbList<Start>();
  @$core.pragma('dart2js:noInline')
  static Start getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Start>(create);
  static Start? _defaultInstance;

  @$pb.TagNumber(5)
  $core.List<$core.String> get options => $_getList(0);

  @$pb.TagNumber(6)
  $core.List<$core.String> get corrects => $_getList(1);
}

