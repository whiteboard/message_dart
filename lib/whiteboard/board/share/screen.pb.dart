///
//  Generated code. Do not modify.
//  source: whiteboard/board/share/screen.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Screen extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Screen', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.share'), createEmptyInstance: create)
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'switch')
    ..hasRequiredFields = false
  ;

  Screen._() : super();
  factory Screen({
    $core.bool? switch_5,
  }) {
    final _result = create();
    if (switch_5 != null) {
      _result.switch_5 = switch_5;
    }
    return _result;
  }
  factory Screen.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Screen.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Screen clone() => Screen()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Screen copyWith(void Function(Screen) updates) => super.copyWith((message) => updates(message as Screen)) as Screen; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Screen create() => Screen._();
  Screen createEmptyInstance() => create();
  static $pb.PbList<Screen> createRepeated() => $pb.PbList<Screen>();
  @$core.pragma('dart2js:noInline')
  static Screen getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Screen>(create);
  static Screen? _defaultInstance;

  @$pb.TagNumber(5)
  $core.bool get switch_5 => $_getBF(0);
  @$pb.TagNumber(5)
  set switch_5($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasSwitch_5() => $_has(0);
  @$pb.TagNumber(5)
  void clearSwitch_5() => clearField(5);
}

