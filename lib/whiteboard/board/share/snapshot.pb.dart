///
//  Generated code. Do not modify.
//  source: whiteboard/board/share/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'screen.pb.dart' as $86;
import 'webpage.pb.dart' as $87;

class Snapshot extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Snapshot', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.share'), createEmptyInstance: create)
    ..aOM<$86.Screen>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'screen', subBuilder: $86.Screen.create)
    ..pc<$87.Webpage>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'webpages', $pb.PbFieldType.PM, subBuilder: $87.Webpage.create)
    ..hasRequiredFields = false
  ;

  Snapshot._() : super();
  factory Snapshot({
    $86.Screen? screen,
    $core.Iterable<$87.Webpage>? webpages,
  }) {
    final _result = create();
    if (screen != null) {
      _result.screen = screen;
    }
    if (webpages != null) {
      _result.webpages.addAll(webpages);
    }
    return _result;
  }
  factory Snapshot.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Snapshot.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Snapshot clone() => Snapshot()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Snapshot copyWith(void Function(Snapshot) updates) => super.copyWith((message) => updates(message as Snapshot)) as Snapshot; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Snapshot create() => Snapshot._();
  Snapshot createEmptyInstance() => create();
  static $pb.PbList<Snapshot> createRepeated() => $pb.PbList<Snapshot>();
  @$core.pragma('dart2js:noInline')
  static Snapshot getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Snapshot>(create);
  static Snapshot? _defaultInstance;

  @$pb.TagNumber(5)
  $86.Screen get screen => $_getN(0);
  @$pb.TagNumber(5)
  set screen($86.Screen v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasScreen() => $_has(0);
  @$pb.TagNumber(5)
  void clearScreen() => clearField(5);
  @$pb.TagNumber(5)
  $86.Screen ensureScreen() => $_ensure(0);

  @$pb.TagNumber(6)
  $core.List<$87.Webpage> get webpages => $_getList(1);
}

