///
//  Generated code. Do not modify.
//  source: whiteboard/board/share/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'screen', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.share.Screen', '10': 'screen'},
    const {'1': 'webpages', '3': 6, '4': 3, '5': 11, '6': '.whiteboard.board.share.Webpage', '10': 'webpages'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBI2CgZzY3JlZW4YBSABKAsyHi53aGl0ZWJvYXJkLmJvYXJkLnNoYXJlLlNjcmVlblIGc2NyZWVuEjsKCHdlYnBhZ2VzGAYgAygLMh8ud2hpdGVib2FyZC5ib2FyZC5zaGFyZS5XZWJwYWdlUgh3ZWJwYWdlcw==');
