///
//  Generated code. Do not modify.
//  source: whiteboard/board/share/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'webpage.pb.dart' as $87;
import 'screen.pb.dart' as $86;
import 'snapshot.pb.dart' as $88;

enum Action_Action {
  webpage, 
  screen, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.webpage,
    6 : Action_Action.screen,
    10 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.share'), createEmptyInstance: create)
    ..oo(0, [5, 6, 10])
    ..aOM<$87.Webpage>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'webpage', subBuilder: $87.Webpage.create)
    ..aOM<$86.Screen>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'screen', subBuilder: $86.Screen.create)
    ..aOM<$88.Snapshot>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $88.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $87.Webpage? webpage,
    $86.Screen? screen,
    $88.Snapshot? snapshot,
  }) {
    final _result = create();
    if (webpage != null) {
      _result.webpage = webpage;
    }
    if (screen != null) {
      _result.screen = screen;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(5)
  $87.Webpage get webpage => $_getN(0);
  @$pb.TagNumber(5)
  set webpage($87.Webpage v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasWebpage() => $_has(0);
  @$pb.TagNumber(5)
  void clearWebpage() => clearField(5);
  @$pb.TagNumber(5)
  $87.Webpage ensureWebpage() => $_ensure(0);

  @$pb.TagNumber(6)
  $86.Screen get screen => $_getN(1);
  @$pb.TagNumber(6)
  set screen($86.Screen v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasScreen() => $_has(1);
  @$pb.TagNumber(6)
  void clearScreen() => clearField(6);
  @$pb.TagNumber(6)
  $86.Screen ensureScreen() => $_ensure(1);

  @$pb.TagNumber(10)
  $88.Snapshot get snapshot => $_getN(2);
  @$pb.TagNumber(10)
  set snapshot($88.Snapshot v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSnapshot() => $_has(2);
  @$pb.TagNumber(10)
  void clearSnapshot() => clearField(10);
  @$pb.TagNumber(10)
  $88.Snapshot ensureSnapshot() => $_ensure(2);
}

