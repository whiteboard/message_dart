///
//  Generated code. Do not modify.
//  source: whiteboard/board/share/webpage.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/file.pb.dart' as $37;
import '../../core/window.pb.dart' as $2;

class Webpage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Webpage', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.share'), createEmptyInstance: create)
    ..aOM<$37.File>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'file', subBuilder: $37.File.create)
    ..aOM<$2.Window>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..hasRequiredFields = false
  ;

  Webpage._() : super();
  factory Webpage({
    $37.File? file,
    $2.Window? window,
  }) {
    final _result = create();
    if (file != null) {
      _result.file = file;
    }
    if (window != null) {
      _result.window = window;
    }
    return _result;
  }
  factory Webpage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Webpage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Webpage clone() => Webpage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Webpage copyWith(void Function(Webpage) updates) => super.copyWith((message) => updates(message as Webpage)) as Webpage; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Webpage create() => Webpage._();
  Webpage createEmptyInstance() => create();
  static $pb.PbList<Webpage> createRepeated() => $pb.PbList<Webpage>();
  @$core.pragma('dart2js:noInline')
  static Webpage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Webpage>(create);
  static Webpage? _defaultInstance;

  @$pb.TagNumber(5)
  $37.File get file => $_getN(0);
  @$pb.TagNumber(5)
  set file($37.File v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasFile() => $_has(0);
  @$pb.TagNumber(5)
  void clearFile() => clearField(5);
  @$pb.TagNumber(5)
  $37.File ensureFile() => $_ensure(0);

  @$pb.TagNumber(6)
  $2.Window get window => $_getN(1);
  @$pb.TagNumber(6)
  set window($2.Window v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasWindow() => $_has(1);
  @$pb.TagNumber(6)
  void clearWindow() => clearField(6);
  @$pb.TagNumber(6)
  $2.Window ensureWindow() => $_ensure(1);
}

