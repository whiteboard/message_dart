///
//  Generated code. Do not modify.
//  source: whiteboard/board/pdf/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'open.pb.dart' as $64;
import 'flip.pb.dart' as $65;
import 'snapshot.pb.dart' as $66;

enum Action_Action {
  open, 
  flip, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.open,
    6 : Action_Action.flip,
    10 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.pdf'), createEmptyInstance: create)
    ..oo(0, [5, 6, 10])
    ..aOM<$2.Window>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$64.Open>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'open', subBuilder: $64.Open.create)
    ..aOM<$65.Flip>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'flip', subBuilder: $65.Flip.create)
    ..aOM<$66.Snapshot>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $66.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $2.Window? window,
    $64.Open? open,
    $65.Flip? flip,
    $66.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (open != null) {
      _result.open = open;
    }
    if (flip != null) {
      _result.flip = flip;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $2.Window get window => $_getN(0);
  @$pb.TagNumber(3)
  set window($2.Window v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(3)
  void clearWindow() => clearField(3);
  @$pb.TagNumber(3)
  $2.Window ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $64.Open get open => $_getN(1);
  @$pb.TagNumber(5)
  set open($64.Open v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasOpen() => $_has(1);
  @$pb.TagNumber(5)
  void clearOpen() => clearField(5);
  @$pb.TagNumber(5)
  $64.Open ensureOpen() => $_ensure(1);

  @$pb.TagNumber(6)
  $65.Flip get flip => $_getN(2);
  @$pb.TagNumber(6)
  set flip($65.Flip v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasFlip() => $_has(2);
  @$pb.TagNumber(6)
  void clearFlip() => clearField(6);
  @$pb.TagNumber(6)
  $65.Flip ensureFlip() => $_ensure(2);

  @$pb.TagNumber(10)
  $66.Snapshot get snapshot => $_getN(3);
  @$pb.TagNumber(10)
  set snapshot($66.Snapshot v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSnapshot() => $_has(3);
  @$pb.TagNumber(10)
  void clearSnapshot() => clearField(10);
  @$pb.TagNumber(10)
  $66.Snapshot ensureSnapshot() => $_ensure(3);
}

