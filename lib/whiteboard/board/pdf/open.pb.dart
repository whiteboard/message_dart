///
//  Generated code. Do not modify.
//  source: whiteboard/board/pdf/open.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/file.pb.dart' as $37;

class Open extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Open', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.pdf'), createEmptyInstance: create)
    ..aOM<$37.File>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'file', subBuilder: $37.File.create)
    ..hasRequiredFields = false
  ;

  Open._() : super();
  factory Open({
    $37.File? file,
  }) {
    final _result = create();
    if (file != null) {
      _result.file = file;
    }
    return _result;
  }
  factory Open.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Open.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Open clone() => Open()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Open copyWith(void Function(Open) updates) => super.copyWith((message) => updates(message as Open)) as Open; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Open create() => Open._();
  Open createEmptyInstance() => create();
  static $pb.PbList<Open> createRepeated() => $pb.PbList<Open>();
  @$core.pragma('dart2js:noInline')
  static Open getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Open>(create);
  static Open? _defaultInstance;

  @$pb.TagNumber(5)
  $37.File get file => $_getN(0);
  @$pb.TagNumber(5)
  set file($37.File v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasFile() => $_has(0);
  @$pb.TagNumber(5)
  void clearFile() => clearField(5);
  @$pb.TagNumber(5)
  $37.File ensureFile() => $_ensure(0);
}

