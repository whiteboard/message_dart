///
//  Generated code. Do not modify.
//  source: whiteboard/board/online.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use onlineDescriptor instead')
const Online$json = const {
  '1': 'Online',
  '2': const [
    const {'1': 'total', '3': 3, '4': 1, '5': 5, '10': 'total'},
    const {'1': 'collaborator', '3': 4, '4': 1, '5': 11, '6': '.whiteboard.core.Collaborator', '10': 'collaborator'},
  ],
};

/// Descriptor for `Online`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onlineDescriptor = $convert.base64Decode('CgZPbmxpbmUSFAoFdG90YWwYAyABKAVSBXRvdGFsEkEKDGNvbGxhYm9yYXRvchgEIAEoCzIdLndoaXRlYm9hcmQuY29yZS5Db2xsYWJvcmF0b3JSDGNvbGxhYm9yYXRvcg==');
