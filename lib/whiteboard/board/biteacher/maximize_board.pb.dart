///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/maximize_board.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class MaximizeBoard extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MaximizeBoard', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.biteacher'), createEmptyInstance: create)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'switch')
    ..hasRequiredFields = false
  ;

  MaximizeBoard._() : super();
  factory MaximizeBoard({
    $core.bool? switch_3,
  }) {
    final _result = create();
    if (switch_3 != null) {
      _result.switch_3 = switch_3;
    }
    return _result;
  }
  factory MaximizeBoard.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MaximizeBoard.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MaximizeBoard clone() => MaximizeBoard()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MaximizeBoard copyWith(void Function(MaximizeBoard) updates) => super.copyWith((message) => updates(message as MaximizeBoard)) as MaximizeBoard; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MaximizeBoard create() => MaximizeBoard._();
  MaximizeBoard createEmptyInstance() => create();
  static $pb.PbList<MaximizeBoard> createRepeated() => $pb.PbList<MaximizeBoard>();
  @$core.pragma('dart2js:noInline')
  static MaximizeBoard getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MaximizeBoard>(create);
  static MaximizeBoard? _defaultInstance;

  @$pb.TagNumber(3)
  $core.bool get switch_3 => $_getBF(0);
  @$pb.TagNumber(3)
  set switch_3($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasSwitch_3() => $_has(0);
  @$pb.TagNumber(3)
  void clearSwitch_3() => clearField(3);
}

