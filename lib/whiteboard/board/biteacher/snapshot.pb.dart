///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;

import 'layout_type.pbenum.dart' as $3;
import 'maximize_type.pbenum.dart' as $6;
import '../../core/user_type.pbenum.dart' as $4;

class Snapshot extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Snapshot', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.biteacher'), createEmptyInstance: create)
    ..e<$3.LayoutType>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'layout', $pb.PbFieldType.OE, defaultOrMaker: $3.LayoutType.RIGHT, valueOf: $3.LayoutType.valueOf, enumValues: $3.LayoutType.values)
    ..e<$6.MaximizeType>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maximize', $pb.PbFieldType.OE, defaultOrMaker: $6.MaximizeType.NONE, valueOf: $6.MaximizeType.valueOf, enumValues: $6.MaximizeType.values)
    ..e<$4.UserType>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pip', $pb.PbFieldType.OE, defaultOrMaker: $4.UserType.USER_TYPE_UNSPECIFIED, valueOf: $4.UserType.valueOf, enumValues: $4.UserType.values)
    ..aOM<$5.Point>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'assistant', subBuilder: $5.Point.create)
    ..hasRequiredFields = false
  ;

  Snapshot._() : super();
  factory Snapshot({
    $3.LayoutType? layout,
    $6.MaximizeType? maximize,
    $4.UserType? pip,
    $5.Point? assistant,
  }) {
    final _result = create();
    if (layout != null) {
      _result.layout = layout;
    }
    if (maximize != null) {
      _result.maximize = maximize;
    }
    if (pip != null) {
      _result.pip = pip;
    }
    if (assistant != null) {
      _result.assistant = assistant;
    }
    return _result;
  }
  factory Snapshot.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Snapshot.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Snapshot clone() => Snapshot()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Snapshot copyWith(void Function(Snapshot) updates) => super.copyWith((message) => updates(message as Snapshot)) as Snapshot; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Snapshot create() => Snapshot._();
  Snapshot createEmptyInstance() => create();
  static $pb.PbList<Snapshot> createRepeated() => $pb.PbList<Snapshot>();
  @$core.pragma('dart2js:noInline')
  static Snapshot getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Snapshot>(create);
  static Snapshot? _defaultInstance;

  @$pb.TagNumber(5)
  $3.LayoutType get layout => $_getN(0);
  @$pb.TagNumber(5)
  set layout($3.LayoutType v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLayout() => $_has(0);
  @$pb.TagNumber(5)
  void clearLayout() => clearField(5);

  @$pb.TagNumber(6)
  $6.MaximizeType get maximize => $_getN(1);
  @$pb.TagNumber(6)
  set maximize($6.MaximizeType v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMaximize() => $_has(1);
  @$pb.TagNumber(6)
  void clearMaximize() => clearField(6);

  @$pb.TagNumber(7)
  $4.UserType get pip => $_getN(2);
  @$pb.TagNumber(7)
  set pip($4.UserType v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasPip() => $_has(2);
  @$pb.TagNumber(7)
  void clearPip() => clearField(7);

  @$pb.TagNumber(8)
  $5.Point get assistant => $_getN(3);
  @$pb.TagNumber(8)
  set assistant($5.Point v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasAssistant() => $_has(3);
  @$pb.TagNumber(8)
  void clearAssistant() => clearField(8);
  @$pb.TagNumber(8)
  $5.Point ensureAssistant() => $_ensure(3);
}

