///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'layout', '3': 5, '4': 1, '5': 14, '6': '.whiteboard.board.biteacher.LayoutType', '10': 'layout'},
    const {'1': 'maximize', '3': 6, '4': 1, '5': 14, '6': '.whiteboard.board.biteacher.MaximizeType', '10': 'maximize'},
    const {'1': 'pip', '3': 7, '4': 1, '5': 14, '6': '.whiteboard.core.UserType', '10': 'pip'},
    const {'1': 'assistant', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'assistant'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBI+CgZsYXlvdXQYBSABKA4yJi53aGl0ZWJvYXJkLmJvYXJkLmJpdGVhY2hlci5MYXlvdXRUeXBlUgZsYXlvdXQSRAoIbWF4aW1pemUYBiABKA4yKC53aGl0ZWJvYXJkLmJvYXJkLmJpdGVhY2hlci5NYXhpbWl6ZVR5cGVSCG1heGltaXplEisKA3BpcBgHIAEoDjIZLndoaXRlYm9hcmQuY29yZS5Vc2VyVHlwZVIDcGlwEjQKCWFzc2lzdGFudBgIIAEoCzIWLndoaXRlYm9hcmQuY29yZS5Qb2ludFIJYXNzaXN0YW50');
