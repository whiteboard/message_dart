///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/maximize_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class MaximizeType extends $pb.ProtobufEnum {
  static const MaximizeType NONE = MaximizeType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NONE');
  static const MaximizeType BOARD = MaximizeType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'BOARD');
  static const MaximizeType TEACHER = MaximizeType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TEACHER');

  static const $core.List<MaximizeType> values = <MaximizeType> [
    NONE,
    BOARD,
    TEACHER,
  ];

  static final $core.Map<$core.int, MaximizeType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static MaximizeType? valueOf($core.int value) => _byValue[value];

  const MaximizeType._($core.int v, $core.String n) : super(v, n);
}

