///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'layout.pb.dart' as $7;
import 'maximize_board.pb.dart' as $8;
import 'maximize_teacher.pb.dart' as $9;
import 'pip.pb.dart' as $10;
import 'snapshot.pb.dart' as $11;

enum Action_Action {
  layout, 
  maximizeBoard, 
  maximizeTeacher, 
  pip, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.layout,
    6 : Action_Action.maximizeBoard,
    7 : Action_Action.maximizeTeacher,
    8 : Action_Action.pip,
    12 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.biteacher'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 8, 12])
    ..aOM<$7.Layout>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'layout', subBuilder: $7.Layout.create)
    ..aOM<$8.MaximizeBoard>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maximizeBoard', subBuilder: $8.MaximizeBoard.create)
    ..aOM<$9.MaximizeTeacher>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maximizeTeacher', subBuilder: $9.MaximizeTeacher.create)
    ..aOM<$10.Pip>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pip', subBuilder: $10.Pip.create)
    ..aOM<$11.Snapshot>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $11.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $7.Layout? layout,
    $8.MaximizeBoard? maximizeBoard,
    $9.MaximizeTeacher? maximizeTeacher,
    $10.Pip? pip,
    $11.Snapshot? snapshot,
  }) {
    final _result = create();
    if (layout != null) {
      _result.layout = layout;
    }
    if (maximizeBoard != null) {
      _result.maximizeBoard = maximizeBoard;
    }
    if (maximizeTeacher != null) {
      _result.maximizeTeacher = maximizeTeacher;
    }
    if (pip != null) {
      _result.pip = pip;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(5)
  $7.Layout get layout => $_getN(0);
  @$pb.TagNumber(5)
  set layout($7.Layout v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLayout() => $_has(0);
  @$pb.TagNumber(5)
  void clearLayout() => clearField(5);
  @$pb.TagNumber(5)
  $7.Layout ensureLayout() => $_ensure(0);

  @$pb.TagNumber(6)
  $8.MaximizeBoard get maximizeBoard => $_getN(1);
  @$pb.TagNumber(6)
  set maximizeBoard($8.MaximizeBoard v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMaximizeBoard() => $_has(1);
  @$pb.TagNumber(6)
  void clearMaximizeBoard() => clearField(6);
  @$pb.TagNumber(6)
  $8.MaximizeBoard ensureMaximizeBoard() => $_ensure(1);

  @$pb.TagNumber(7)
  $9.MaximizeTeacher get maximizeTeacher => $_getN(2);
  @$pb.TagNumber(7)
  set maximizeTeacher($9.MaximizeTeacher v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasMaximizeTeacher() => $_has(2);
  @$pb.TagNumber(7)
  void clearMaximizeTeacher() => clearField(7);
  @$pb.TagNumber(7)
  $9.MaximizeTeacher ensureMaximizeTeacher() => $_ensure(2);

  @$pb.TagNumber(8)
  $10.Pip get pip => $_getN(3);
  @$pb.TagNumber(8)
  set pip($10.Pip v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasPip() => $_has(3);
  @$pb.TagNumber(8)
  void clearPip() => clearField(8);
  @$pb.TagNumber(8)
  $10.Pip ensurePip() => $_ensure(3);

  @$pb.TagNumber(12)
  $11.Snapshot get snapshot => $_getN(4);
  @$pb.TagNumber(12)
  set snapshot($11.Snapshot v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasSnapshot() => $_has(4);
  @$pb.TagNumber(12)
  void clearSnapshot() => clearField(12);
  @$pb.TagNumber(12)
  $11.Snapshot ensureSnapshot() => $_ensure(4);
}

