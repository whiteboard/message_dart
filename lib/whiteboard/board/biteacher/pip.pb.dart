///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/pip.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user_type.pbenum.dart' as $4;

class Pip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pip', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.biteacher'), createEmptyInstance: create)
    ..e<$4.UserType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $4.UserType.USER_TYPE_UNSPECIFIED, valueOf: $4.UserType.valueOf, enumValues: $4.UserType.values)
    ..hasRequiredFields = false
  ;

  Pip._() : super();
  factory Pip({
    $4.UserType? type,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    return _result;
  }
  factory Pip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Pip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Pip clone() => Pip()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Pip copyWith(void Function(Pip) updates) => super.copyWith((message) => updates(message as Pip)) as Pip; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Pip create() => Pip._();
  Pip createEmptyInstance() => create();
  static $pb.PbList<Pip> createRepeated() => $pb.PbList<Pip>();
  @$core.pragma('dart2js:noInline')
  static Pip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Pip>(create);
  static Pip? _defaultInstance;

  @$pb.TagNumber(1)
  $4.UserType get type => $_getN(0);
  @$pb.TagNumber(1)
  set type($4.UserType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);
}

