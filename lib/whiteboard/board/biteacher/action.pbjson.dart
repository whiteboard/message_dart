///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'layout', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.Layout', '9': 0, '10': 'layout'},
    const {'1': 'maximize_board', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.MaximizeBoard', '9': 0, '10': 'maximizeBoard'},
    const {'1': 'maximize_teacher', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.MaximizeTeacher', '9': 0, '10': 'maximizeTeacher'},
    const {'1': 'pip', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.Pip', '9': 0, '10': 'pip'},
    const {'1': 'snapshot', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.biteacher.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SPAoGbGF5b3V0GAUgASgLMiIud2hpdGVib2FyZC5ib2FyZC5iaXRlYWNoZXIuTGF5b3V0SABSBmxheW91dBJSCg5tYXhpbWl6ZV9ib2FyZBgGIAEoCzIpLndoaXRlYm9hcmQuYm9hcmQuYml0ZWFjaGVyLk1heGltaXplQm9hcmRIAFINbWF4aW1pemVCb2FyZBJYChBtYXhpbWl6ZV90ZWFjaGVyGAcgASgLMisud2hpdGVib2FyZC5ib2FyZC5iaXRlYWNoZXIuTWF4aW1pemVUZWFjaGVySABSD21heGltaXplVGVhY2hlchIzCgNwaXAYCCABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLmJpdGVhY2hlci5QaXBIAFIDcGlwEkIKCHNuYXBzaG90GAwgASgLMiQud2hpdGVib2FyZC5ib2FyZC5iaXRlYWNoZXIuU25hcHNob3RIAFIIc25hcHNob3RCCAoGYWN0aW9u');
