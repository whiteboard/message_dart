///
//  Generated code. Do not modify.
//  source: whiteboard/board/biteacher/layout_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use layoutTypeDescriptor instead')
const LayoutType$json = const {
  '1': 'LayoutType',
  '2': const [
    const {'1': 'RIGHT', '2': 0},
    const {'1': 'LEFT', '2': 1},
  ],
};

/// Descriptor for `LayoutType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List layoutTypeDescriptor = $convert.base64Decode('CgpMYXlvdXRUeXBlEgkKBVJJR0hUEAASCAoETEVGVBAB');
