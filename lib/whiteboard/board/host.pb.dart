///
//  Generated code. Do not modify.
//  source: whiteboard/board/host.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../core/user.pb.dart' as $1;

class Host extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Host', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..aOM<$1.User>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'from', subBuilder: $1.User.create)
    ..aOM<$1.User>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'to', subBuilder: $1.User.create)
    ..hasRequiredFields = false
  ;

  Host._() : super();
  factory Host({
    $1.User? from,
    $1.User? to,
  }) {
    final _result = create();
    if (from != null) {
      _result.from = from;
    }
    if (to != null) {
      _result.to = to;
    }
    return _result;
  }
  factory Host.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Host.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Host clone() => Host()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Host copyWith(void Function(Host) updates) => super.copyWith((message) => updates(message as Host)) as Host; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Host create() => Host._();
  Host createEmptyInstance() => create();
  static $pb.PbList<Host> createRepeated() => $pb.PbList<Host>();
  @$core.pragma('dart2js:noInline')
  static Host getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Host>(create);
  static Host? _defaultInstance;

  @$pb.TagNumber(3)
  $1.User get from => $_getN(0);
  @$pb.TagNumber(3)
  set from($1.User v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFrom() => $_has(0);
  @$pb.TagNumber(3)
  void clearFrom() => clearField(3);
  @$pb.TagNumber(3)
  $1.User ensureFrom() => $_ensure(0);

  @$pb.TagNumber(4)
  $1.User get to => $_getN(1);
  @$pb.TagNumber(4)
  set to($1.User v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasTo() => $_has(1);
  @$pb.TagNumber(4)
  void clearTo() => clearField(4);
  @$pb.TagNumber(4)
  $1.User ensureTo() => $_ensure(1);
}

