///
//  Generated code. Do not modify.
//  source: whiteboard/board/timer/status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Status extends $pb.ProtobufEnum {
  static const Status STATUS_UNSPECIFIED = Status._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STATUS_UNSPECIFIED');
  static const Status OPENED = Status._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OPENED');
  static const Status STARTED = Status._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STARTED');
  static const Status PAUSED = Status._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PAUSED');

  static const $core.List<Status> values = <Status> [
    STATUS_UNSPECIFIED,
    OPENED,
    STARTED,
    PAUSED,
  ];

  static final $core.Map<$core.int, Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Status? valueOf($core.int value) => _byValue[value];

  const Status._($core.int v, $core.String n) : super(v, n);
}

