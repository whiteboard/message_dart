///
//  Generated code. Do not modify.
//  source: whiteboard/board/timer/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.Window', '10': 'window'},
    const {'1': 'open', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Open', '9': 0, '10': 'open'},
    const {'1': 'update', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Update', '9': 0, '10': 'update'},
    const {'1': 'start', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Start', '9': 0, '10': 'start'},
    const {'1': 'pause', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Pause', '9': 0, '10': 'pause'},
    const {'1': 'cancel', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Cancel', '9': 0, '10': 'cancel'},
    const {'1': 'snapshot', '3': 13, '4': 1, '5': 11, '6': '.whiteboard.board.timer.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SLwoGd2luZG93GAMgASgLMhcud2hpdGVib2FyZC5jb3JlLldpbmRvd1IGd2luZG93EjIKBG9wZW4YBSABKAsyHC53aGl0ZWJvYXJkLmJvYXJkLnRpbWVyLk9wZW5IAFIEb3BlbhI4CgZ1cGRhdGUYBiABKAsyHi53aGl0ZWJvYXJkLmJvYXJkLnRpbWVyLlVwZGF0ZUgAUgZ1cGRhdGUSNQoFc3RhcnQYByABKAsyHS53aGl0ZWJvYXJkLmJvYXJkLnRpbWVyLlN0YXJ0SABSBXN0YXJ0EjUKBXBhdXNlGAggASgLMh0ud2hpdGVib2FyZC5ib2FyZC50aW1lci5QYXVzZUgAUgVwYXVzZRI4CgZjYW5jZWwYCSABKAsyHi53aGl0ZWJvYXJkLmJvYXJkLnRpbWVyLkNhbmNlbEgAUgZjYW5jZWwSPgoIc25hcHNob3QYDSABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnRpbWVyLlNuYXBzaG90SABSCHNuYXBzaG90QggKBmFjdGlvbg==');
