///
//  Generated code. Do not modify.
//  source: whiteboard/board/timer/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'open.pb.dart' as $94;
import 'update.pb.dart' as $95;
import 'start.pb.dart' as $96;
import 'pause.pb.dart' as $97;
import 'cancel.pb.dart' as $98;
import 'snapshot.pb.dart' as $99;

enum Action_Action {
  open, 
  update, 
  start, 
  pause, 
  cancel, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.open,
    6 : Action_Action.update,
    7 : Action_Action.start,
    8 : Action_Action.pause,
    9 : Action_Action.cancel,
    13 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.timer'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 8, 9, 13])
    ..aOM<$2.Window>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$94.Open>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'open', subBuilder: $94.Open.create)
    ..aOM<$95.Update>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'update', subBuilder: $95.Update.create)
    ..aOM<$96.Start>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $96.Start.create)
    ..aOM<$97.Pause>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pause', subBuilder: $97.Pause.create)
    ..aOM<$98.Cancel>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cancel', subBuilder: $98.Cancel.create)
    ..aOM<$99.Snapshot>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $99.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $2.Window? window,
    $94.Open? open,
    $95.Update? update,
    $96.Start? start,
    $97.Pause? pause,
    $98.Cancel? cancel,
    $99.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (open != null) {
      _result.open = open;
    }
    if (update != null) {
      _result.update = update;
    }
    if (start != null) {
      _result.start = start;
    }
    if (pause != null) {
      _result.pause = pause;
    }
    if (cancel != null) {
      _result.cancel = cancel;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $2.Window get window => $_getN(0);
  @$pb.TagNumber(3)
  set window($2.Window v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(3)
  void clearWindow() => clearField(3);
  @$pb.TagNumber(3)
  $2.Window ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $94.Open get open => $_getN(1);
  @$pb.TagNumber(5)
  set open($94.Open v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasOpen() => $_has(1);
  @$pb.TagNumber(5)
  void clearOpen() => clearField(5);
  @$pb.TagNumber(5)
  $94.Open ensureOpen() => $_ensure(1);

  @$pb.TagNumber(6)
  $95.Update get update => $_getN(2);
  @$pb.TagNumber(6)
  set update($95.Update v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasUpdate() => $_has(2);
  @$pb.TagNumber(6)
  void clearUpdate() => clearField(6);
  @$pb.TagNumber(6)
  $95.Update ensureUpdate() => $_ensure(2);

  @$pb.TagNumber(7)
  $96.Start get start => $_getN(3);
  @$pb.TagNumber(7)
  set start($96.Start v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasStart() => $_has(3);
  @$pb.TagNumber(7)
  void clearStart() => clearField(7);
  @$pb.TagNumber(7)
  $96.Start ensureStart() => $_ensure(3);

  @$pb.TagNumber(8)
  $97.Pause get pause => $_getN(4);
  @$pb.TagNumber(8)
  set pause($97.Pause v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasPause() => $_has(4);
  @$pb.TagNumber(8)
  void clearPause() => clearField(8);
  @$pb.TagNumber(8)
  $97.Pause ensurePause() => $_ensure(4);

  @$pb.TagNumber(9)
  $98.Cancel get cancel => $_getN(5);
  @$pb.TagNumber(9)
  set cancel($98.Cancel v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasCancel() => $_has(5);
  @$pb.TagNumber(9)
  void clearCancel() => clearField(9);
  @$pb.TagNumber(9)
  $98.Cancel ensureCancel() => $_ensure(5);

  @$pb.TagNumber(13)
  $99.Snapshot get snapshot => $_getN(6);
  @$pb.TagNumber(13)
  set snapshot($99.Snapshot v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasSnapshot() => $_has(6);
  @$pb.TagNumber(13)
  void clearSnapshot() => clearField(13);
  @$pb.TagNumber(13)
  $99.Snapshot ensureSnapshot() => $_ensure(6);
}

