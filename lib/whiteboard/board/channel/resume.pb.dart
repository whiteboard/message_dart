///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/resume.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Resume extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Resume', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.channel'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Resume._() : super();
  factory Resume() => create();
  factory Resume.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Resume.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Resume clone() => Resume()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Resume copyWith(void Function(Resume) updates) => super.copyWith((message) => updates(message as Resume)) as Resume; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Resume create() => Resume._();
  Resume createEmptyInstance() => create();
  static $pb.PbList<Resume> createRepeated() => $pb.PbList<Resume>();
  @$core.pragma('dart2js:noInline')
  static Resume getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Resume>(create);
  static Resume? _defaultInstance;
}

