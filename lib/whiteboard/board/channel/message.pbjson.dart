///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/message.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'id', '3': 3, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'to', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.core.User', '10': 'to'},
    const {'1': 'content', '3': 6, '4': 1, '5': 9, '10': 'content'},
    const {'1': 'type', '3': 7, '4': 1, '5': 14, '6': '.whiteboard.core.MessageType', '10': 'type'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEg4KAmlkGAMgASgJUgJpZBIlCgJ0bxgFIAEoCzIVLndoaXRlYm9hcmQuY29yZS5Vc2VyUgJ0bxIYCgdjb250ZW50GAYgASgJUgdjb250ZW50EjAKBHR5cGUYByABKA4yHC53aGl0ZWJvYXJkLmNvcmUuTWVzc2FnZVR5cGVSBHR5cGU=');
