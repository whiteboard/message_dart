///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/pause.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Pause extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pause', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.channel'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Pause._() : super();
  factory Pause() => create();
  factory Pause.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Pause.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Pause clone() => Pause()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Pause copyWith(void Function(Pause) updates) => super.copyWith((message) => updates(message as Pause)) as Pause; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Pause create() => Pause._();
  Pause createEmptyInstance() => create();
  static $pb.PbList<Pause> createRepeated() => $pb.PbList<Pause>();
  @$core.pragma('dart2js:noInline')
  static Pause getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Pause>(create);
  static Pause? _defaultInstance;
}

