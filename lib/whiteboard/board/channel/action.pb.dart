///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'message.pb.dart' as $14;
import 'recall.pb.dart' as $15;
import 'prohibit.pb.dart' as $16;
import 'allow.pb.dart' as $17;
import 'pause.pb.dart' as $18;
import 'resume.pb.dart' as $19;

enum Action_Action {
  message, 
  recall, 
  prohibit, 
  allow, 
  pause, 
  resume, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.message,
    6 : Action_Action.recall,
    7 : Action_Action.prohibit,
    8 : Action_Action.allow,
    9 : Action_Action.pause,
    10 : Action_Action.resume,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.channel'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 8, 9, 10])
    ..aOM<$14.Message>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message', subBuilder: $14.Message.create)
    ..aOM<$15.Recall>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'recall', subBuilder: $15.Recall.create)
    ..aOM<$16.Prohibit>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'prohibit', subBuilder: $16.Prohibit.create)
    ..aOM<$17.Allow>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'allow', subBuilder: $17.Allow.create)
    ..aOM<$18.Pause>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pause', subBuilder: $18.Pause.create)
    ..aOM<$19.Resume>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'resume', subBuilder: $19.Resume.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $14.Message? message,
    $15.Recall? recall,
    $16.Prohibit? prohibit,
    $17.Allow? allow,
    $18.Pause? pause,
    $19.Resume? resume,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (recall != null) {
      _result.recall = recall;
    }
    if (prohibit != null) {
      _result.prohibit = prohibit;
    }
    if (allow != null) {
      _result.allow = allow;
    }
    if (pause != null) {
      _result.pause = pause;
    }
    if (resume != null) {
      _result.resume = resume;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(5)
  $14.Message get message => $_getN(0);
  @$pb.TagNumber(5)
  set message($14.Message v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(5)
  void clearMessage() => clearField(5);
  @$pb.TagNumber(5)
  $14.Message ensureMessage() => $_ensure(0);

  @$pb.TagNumber(6)
  $15.Recall get recall => $_getN(1);
  @$pb.TagNumber(6)
  set recall($15.Recall v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasRecall() => $_has(1);
  @$pb.TagNumber(6)
  void clearRecall() => clearField(6);
  @$pb.TagNumber(6)
  $15.Recall ensureRecall() => $_ensure(1);

  @$pb.TagNumber(7)
  $16.Prohibit get prohibit => $_getN(2);
  @$pb.TagNumber(7)
  set prohibit($16.Prohibit v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasProhibit() => $_has(2);
  @$pb.TagNumber(7)
  void clearProhibit() => clearField(7);
  @$pb.TagNumber(7)
  $16.Prohibit ensureProhibit() => $_ensure(2);

  @$pb.TagNumber(8)
  $17.Allow get allow => $_getN(3);
  @$pb.TagNumber(8)
  set allow($17.Allow v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasAllow() => $_has(3);
  @$pb.TagNumber(8)
  void clearAllow() => clearField(8);
  @$pb.TagNumber(8)
  $17.Allow ensureAllow() => $_ensure(3);

  @$pb.TagNumber(9)
  $18.Pause get pause => $_getN(4);
  @$pb.TagNumber(9)
  set pause($18.Pause v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasPause() => $_has(4);
  @$pb.TagNumber(9)
  void clearPause() => clearField(9);
  @$pb.TagNumber(9)
  $18.Pause ensurePause() => $_ensure(4);

  @$pb.TagNumber(10)
  $19.Resume get resume => $_getN(5);
  @$pb.TagNumber(10)
  set resume($19.Resume v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasResume() => $_has(5);
  @$pb.TagNumber(10)
  void clearResume() => clearField(10);
  @$pb.TagNumber(10)
  $19.Resume ensureResume() => $_ensure(5);
}

