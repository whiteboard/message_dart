///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'message', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Message', '9': 0, '10': 'message'},
    const {'1': 'recall', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Recall', '9': 0, '10': 'recall'},
    const {'1': 'prohibit', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Prohibit', '9': 0, '10': 'prohibit'},
    const {'1': 'allow', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Allow', '9': 0, '10': 'allow'},
    const {'1': 'pause', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Pause', '9': 0, '10': 'pause'},
    const {'1': 'resume', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.channel.Resume', '9': 0, '10': 'resume'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SPQoHbWVzc2FnZRgFIAEoCzIhLndoaXRlYm9hcmQuYm9hcmQuY2hhbm5lbC5NZXNzYWdlSABSB21lc3NhZ2USOgoGcmVjYWxsGAYgASgLMiAud2hpdGVib2FyZC5ib2FyZC5jaGFubmVsLlJlY2FsbEgAUgZyZWNhbGwSQAoIcHJvaGliaXQYByABKAsyIi53aGl0ZWJvYXJkLmJvYXJkLmNoYW5uZWwuUHJvaGliaXRIAFIIcHJvaGliaXQSNwoFYWxsb3cYCCABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLmNoYW5uZWwuQWxsb3dIAFIFYWxsb3cSNwoFcGF1c2UYCSABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLmNoYW5uZWwuUGF1c2VIAFIFcGF1c2USOgoGcmVzdW1lGAogASgLMiAud2hpdGVib2FyZC5ib2FyZC5jaGFubmVsLlJlc3VtZUgAUgZyZXN1bWVCCAoGYWN0aW9u');
