///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/recall.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Recall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Recall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.channel'), createEmptyInstance: create)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..hasRequiredFields = false
  ;

  Recall._() : super();
  factory Recall({
    $core.String? id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory Recall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Recall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Recall clone() => Recall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Recall copyWith(void Function(Recall) updates) => super.copyWith((message) => updates(message as Recall)) as Recall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Recall create() => Recall._();
  Recall createEmptyInstance() => create();
  static $pb.PbList<Recall> createRepeated() => $pb.PbList<Recall>();
  @$core.pragma('dart2js:noInline')
  static Recall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Recall>(create);
  static Recall? _defaultInstance;

  @$pb.TagNumber(5)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(5)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(5)
  void clearId() => clearField(5);
}

