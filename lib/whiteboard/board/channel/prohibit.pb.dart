///
//  Generated code. Do not modify.
//  source: whiteboard/board/channel/prohibit.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user.pb.dart' as $12;

class Prohibit extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Prohibit', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.channel'), createEmptyInstance: create)
    ..aOM<$12.User>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $12.User.create)
    ..hasRequiredFields = false
  ;

  Prohibit._() : super();
  factory Prohibit({
    $12.User? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory Prohibit.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Prohibit.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Prohibit clone() => Prohibit()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Prohibit copyWith(void Function(Prohibit) updates) => super.copyWith((message) => updates(message as Prohibit)) as Prohibit; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Prohibit create() => Prohibit._();
  Prohibit createEmptyInstance() => create();
  static $pb.PbList<Prohibit> createRepeated() => $pb.PbList<Prohibit>();
  @$core.pragma('dart2js:noInline')
  static Prohibit getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Prohibit>(create);
  static Prohibit? _defaultInstance;

  @$pb.TagNumber(5)
  $12.User get user => $_getN(0);
  @$pb.TagNumber(5)
  set user($12.User v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(5)
  void clearUser() => clearField(5);
  @$pb.TagNumber(5)
  $12.User ensureUser() => $_ensure(0);
}

