///
//  Generated code. Do not modify.
//  source: whiteboard/board/responder/end.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user.pb.dart' as $12;

class End extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'End', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.responder'), createEmptyInstance: create)
    ..aOM<$12.User>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'success', subBuilder: $12.User.create)
    ..hasRequiredFields = false
  ;

  End._() : super();
  factory End({
    $12.User? success,
  }) {
    final _result = create();
    if (success != null) {
      _result.success = success;
    }
    return _result;
  }
  factory End.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory End.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  End clone() => End()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  End copyWith(void Function(End) updates) => super.copyWith((message) => updates(message as End)) as End; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static End create() => End._();
  End createEmptyInstance() => create();
  static $pb.PbList<End> createRepeated() => $pb.PbList<End>();
  @$core.pragma('dart2js:noInline')
  static End getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<End>(create);
  static End? _defaultInstance;

  @$pb.TagNumber(10)
  $12.User get success => $_getN(0);
  @$pb.TagNumber(10)
  set success($12.User v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSuccess() => $_has(0);
  @$pb.TagNumber(10)
  void clearSuccess() => clearField(10);
  @$pb.TagNumber(10)
  $12.User ensureSuccess() => $_ensure(0);
}

