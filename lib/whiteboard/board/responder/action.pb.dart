///
//  Generated code. Do not modify.
//  source: whiteboard/board/responder/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window.pb.dart' as $2;
import 'start.pb.dart' as $82;
import 'request.pb.dart' as $83;
import 'end.pb.dart' as $84;
import 'snapshot.pb.dart' as $85;

enum Action_Action {
  start, 
  request, 
  end, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.start,
    6 : Action_Action.request,
    7 : Action_Action.end,
    10 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.responder'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 10])
    ..aOM<$2.Window>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $2.Window.create)
    ..aOM<$82.Start>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $82.Start.create)
    ..aOM<$83.Request>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'request', subBuilder: $83.Request.create)
    ..aOM<$84.End>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: $84.End.create)
    ..aOM<$85.Snapshot>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $85.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $2.Window? window,
    $82.Start? start,
    $83.Request? request,
    $84.End? end,
    $85.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (start != null) {
      _result.start = start;
    }
    if (request != null) {
      _result.request = request;
    }
    if (end != null) {
      _result.end = end;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $2.Window get window => $_getN(0);
  @$pb.TagNumber(3)
  set window($2.Window v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(3)
  void clearWindow() => clearField(3);
  @$pb.TagNumber(3)
  $2.Window ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $82.Start get start => $_getN(1);
  @$pb.TagNumber(5)
  set start($82.Start v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasStart() => $_has(1);
  @$pb.TagNumber(5)
  void clearStart() => clearField(5);
  @$pb.TagNumber(5)
  $82.Start ensureStart() => $_ensure(1);

  @$pb.TagNumber(6)
  $83.Request get request => $_getN(2);
  @$pb.TagNumber(6)
  set request($83.Request v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasRequest() => $_has(2);
  @$pb.TagNumber(6)
  void clearRequest() => clearField(6);
  @$pb.TagNumber(6)
  $83.Request ensureRequest() => $_ensure(2);

  @$pb.TagNumber(7)
  $84.End get end => $_getN(3);
  @$pb.TagNumber(7)
  set end($84.End v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasEnd() => $_has(3);
  @$pb.TagNumber(7)
  void clearEnd() => clearField(7);
  @$pb.TagNumber(7)
  $84.End ensureEnd() => $_ensure(3);

  @$pb.TagNumber(10)
  $85.Snapshot get snapshot => $_getN(4);
  @$pb.TagNumber(10)
  set snapshot($85.Snapshot v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSnapshot() => $_has(4);
  @$pb.TagNumber(10)
  void clearSnapshot() => clearField(10);
  @$pb.TagNumber(10)
  $85.Snapshot ensureSnapshot() => $_ensure(4);
}

