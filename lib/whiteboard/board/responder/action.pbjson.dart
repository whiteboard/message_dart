///
//  Generated code. Do not modify.
//  source: whiteboard/board/responder/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.Window', '10': 'window'},
    const {'1': 'start', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.responder.Start', '9': 0, '10': 'start'},
    const {'1': 'request', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.responder.Request', '9': 0, '10': 'request'},
    const {'1': 'end', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.responder.End', '9': 0, '10': 'end'},
    const {'1': 'snapshot', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.responder.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SLwoGd2luZG93GAMgASgLMhcud2hpdGVib2FyZC5jb3JlLldpbmRvd1IGd2luZG93EjkKBXN0YXJ0GAUgASgLMiEud2hpdGVib2FyZC5ib2FyZC5yZXNwb25kZXIuU3RhcnRIAFIFc3RhcnQSPwoHcmVxdWVzdBgGIAEoCzIjLndoaXRlYm9hcmQuYm9hcmQucmVzcG9uZGVyLlJlcXVlc3RIAFIHcmVxdWVzdBIzCgNlbmQYByABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLnJlc3BvbmRlci5FbmRIAFIDZW5kEkIKCHNuYXBzaG90GAogASgLMiQud2hpdGVib2FyZC5ib2FyZC5yZXNwb25kZXIuU25hcHNob3RIAFIIc25hcHNob3RCCAoGYWN0aW9u');
