///
//  Generated code. Do not modify.
//  source: whiteboard/board/raise_hand.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class RaiseHand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RaiseHand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RaiseHand._() : super();
  factory RaiseHand() => create();
  factory RaiseHand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RaiseHand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RaiseHand clone() => RaiseHand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RaiseHand copyWith(void Function(RaiseHand) updates) => super.copyWith((message) => updates(message as RaiseHand)) as RaiseHand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RaiseHand create() => RaiseHand._();
  RaiseHand createEmptyInstance() => create();
  static $pb.PbList<RaiseHand> createRepeated() => $pb.PbList<RaiseHand>();
  @$core.pragma('dart2js:noInline')
  static RaiseHand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RaiseHand>(create);
  static RaiseHand? _defaultInstance;
}

