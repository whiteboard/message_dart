///
//  Generated code. Do not modify.
//  source: whiteboard/board/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'online.pb.dart' as $6;
import 'offline.pb.dart' as $7;
import 'collapse.pb.dart' as $8;
import 'expand.pb.dart' as $9;
import 'status_changed.pb.dart' as $10;
import 'webcam.pb.dart' as $11;
import 'microphone.pb.dart' as $12;
import 'host.pb.dart' as $13;
import 'stream.pb.dart' as $14;
import 'raise_hand.pb.dart' as $15;

enum Action_Action {
  online, 
  offline, 
  collapse, 
  expand, 
  statusChanged, 
  webcam, 
  microphone, 
  host, 
  stream, 
  raiseHand, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    3 : Action_Action.online,
    4 : Action_Action.offline,
    7 : Action_Action.collapse,
    8 : Action_Action.expand,
    9 : Action_Action.statusChanged,
    10 : Action_Action.webcam,
    11 : Action_Action.microphone,
    12 : Action_Action.host,
    13 : Action_Action.stream,
    20 : Action_Action.raiseHand,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..oo(0, [3, 4, 7, 8, 9, 10, 11, 12, 13, 20])
    ..aOM<$6.Online>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'online', subBuilder: $6.Online.create)
    ..aOM<$7.Offline>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offline', subBuilder: $7.Offline.create)
    ..aOM<$8.Collapse>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'collapse', subBuilder: $8.Collapse.create)
    ..aOM<$9.Expand>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'expand', subBuilder: $9.Expand.create)
    ..aOM<$10.StatusChanged>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statusChanged', subBuilder: $10.StatusChanged.create)
    ..aOM<$11.Webcam>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'webcam', subBuilder: $11.Webcam.create)
    ..aOM<$12.Microphone>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'microphone', subBuilder: $12.Microphone.create)
    ..aOM<$13.Host>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'host', subBuilder: $13.Host.create)
    ..aOM<$14.Stream>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stream', subBuilder: $14.Stream.create)
    ..aOM<$15.RaiseHand>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'raiseHand', subBuilder: $15.RaiseHand.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $6.Online? online,
    $7.Offline? offline,
    $8.Collapse? collapse,
    $9.Expand? expand,
    $10.StatusChanged? statusChanged,
    $11.Webcam? webcam,
    $12.Microphone? microphone,
    $13.Host? host,
    $14.Stream? stream,
    $15.RaiseHand? raiseHand,
  }) {
    final _result = create();
    if (online != null) {
      _result.online = online;
    }
    if (offline != null) {
      _result.offline = offline;
    }
    if (collapse != null) {
      _result.collapse = collapse;
    }
    if (expand != null) {
      _result.expand = expand;
    }
    if (statusChanged != null) {
      _result.statusChanged = statusChanged;
    }
    if (webcam != null) {
      _result.webcam = webcam;
    }
    if (microphone != null) {
      _result.microphone = microphone;
    }
    if (host != null) {
      _result.host = host;
    }
    if (stream != null) {
      _result.stream = stream;
    }
    if (raiseHand != null) {
      _result.raiseHand = raiseHand;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(3)
  $6.Online get online => $_getN(0);
  @$pb.TagNumber(3)
  set online($6.Online v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasOnline() => $_has(0);
  @$pb.TagNumber(3)
  void clearOnline() => clearField(3);
  @$pb.TagNumber(3)
  $6.Online ensureOnline() => $_ensure(0);

  @$pb.TagNumber(4)
  $7.Offline get offline => $_getN(1);
  @$pb.TagNumber(4)
  set offline($7.Offline v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasOffline() => $_has(1);
  @$pb.TagNumber(4)
  void clearOffline() => clearField(4);
  @$pb.TagNumber(4)
  $7.Offline ensureOffline() => $_ensure(1);

  @$pb.TagNumber(7)
  $8.Collapse get collapse => $_getN(2);
  @$pb.TagNumber(7)
  set collapse($8.Collapse v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasCollapse() => $_has(2);
  @$pb.TagNumber(7)
  void clearCollapse() => clearField(7);
  @$pb.TagNumber(7)
  $8.Collapse ensureCollapse() => $_ensure(2);

  @$pb.TagNumber(8)
  $9.Expand get expand => $_getN(3);
  @$pb.TagNumber(8)
  set expand($9.Expand v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasExpand() => $_has(3);
  @$pb.TagNumber(8)
  void clearExpand() => clearField(8);
  @$pb.TagNumber(8)
  $9.Expand ensureExpand() => $_ensure(3);

  @$pb.TagNumber(9)
  $10.StatusChanged get statusChanged => $_getN(4);
  @$pb.TagNumber(9)
  set statusChanged($10.StatusChanged v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasStatusChanged() => $_has(4);
  @$pb.TagNumber(9)
  void clearStatusChanged() => clearField(9);
  @$pb.TagNumber(9)
  $10.StatusChanged ensureStatusChanged() => $_ensure(4);

  @$pb.TagNumber(10)
  $11.Webcam get webcam => $_getN(5);
  @$pb.TagNumber(10)
  set webcam($11.Webcam v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasWebcam() => $_has(5);
  @$pb.TagNumber(10)
  void clearWebcam() => clearField(10);
  @$pb.TagNumber(10)
  $11.Webcam ensureWebcam() => $_ensure(5);

  @$pb.TagNumber(11)
  $12.Microphone get microphone => $_getN(6);
  @$pb.TagNumber(11)
  set microphone($12.Microphone v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasMicrophone() => $_has(6);
  @$pb.TagNumber(11)
  void clearMicrophone() => clearField(11);
  @$pb.TagNumber(11)
  $12.Microphone ensureMicrophone() => $_ensure(6);

  @$pb.TagNumber(12)
  $13.Host get host => $_getN(7);
  @$pb.TagNumber(12)
  set host($13.Host v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasHost() => $_has(7);
  @$pb.TagNumber(12)
  void clearHost() => clearField(12);
  @$pb.TagNumber(12)
  $13.Host ensureHost() => $_ensure(7);

  @$pb.TagNumber(13)
  $14.Stream get stream => $_getN(8);
  @$pb.TagNumber(13)
  set stream($14.Stream v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasStream() => $_has(8);
  @$pb.TagNumber(13)
  void clearStream() => clearField(13);
  @$pb.TagNumber(13)
  $14.Stream ensureStream() => $_ensure(8);

  @$pb.TagNumber(20)
  $15.RaiseHand get raiseHand => $_getN(9);
  @$pb.TagNumber(20)
  set raiseHand($15.RaiseHand v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasRaiseHand() => $_has(9);
  @$pb.TagNumber(20)
  void clearRaiseHand() => clearField(20);
  @$pb.TagNumber(20)
  $15.RaiseHand ensureRaiseHand() => $_ensure(9);
}

