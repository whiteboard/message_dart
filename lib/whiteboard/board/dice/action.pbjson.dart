///
//  Generated code. Do not modify.
//  source: whiteboard/board/dice/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.core.Window', '10': 'window'},
    const {'1': 'open', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.dice.Open', '9': 0, '10': 'open'},
    const {'1': 'start', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.dice.Start', '9': 0, '10': 'start'},
    const {'1': 'stop', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.dice.Stop', '9': 0, '10': 'stop'},
    const {'1': 'snapshot', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.dice.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SLwoGd2luZG93GAMgASgLMhcud2hpdGVib2FyZC5jb3JlLldpbmRvd1IGd2luZG93EjEKBG9wZW4YBSABKAsyGy53aGl0ZWJvYXJkLmJvYXJkLmRpY2UuT3BlbkgAUgRvcGVuEjQKBXN0YXJ0GAYgASgLMhwud2hpdGVib2FyZC5ib2FyZC5kaWNlLlN0YXJ0SABSBXN0YXJ0EjEKBHN0b3AYByABKAsyGy53aGl0ZWJvYXJkLmJvYXJkLmRpY2UuU3RvcEgAUgRzdG9wEj0KCHNuYXBzaG90GAogASgLMh8ud2hpdGVib2FyZC5ib2FyZC5kaWNlLlNuYXBzaG90SABSCHNuYXBzaG90QggKBmFjdGlvbg==');
