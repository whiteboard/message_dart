///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/carousel.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use carouselDescriptor instead')
const Carousel$json = const {
  '1': 'Carousel',
  '2': const [
    const {'1': 'users', '3': 5, '4': 3, '5': 11, '6': '.whiteboard.core.User', '10': 'users'},
    const {'1': 'switch', '3': 6, '4': 1, '5': 8, '10': 'switch'},
  ],
};

/// Descriptor for `Carousel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List carouselDescriptor = $convert.base64Decode('CghDYXJvdXNlbBIrCgV1c2VycxgFIAMoCzIVLndoaXRlYm9hcmQuY29yZS5Vc2VyUgV1c2VycxIWCgZzd2l0Y2gYBiABKAhSBnN3aXRjaA==');
