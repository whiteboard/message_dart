///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'carousel.pb.dart' as $89;
import 'flip.pb.dart' as $90;
import 'auto.pb.dart' as $91;
import 'snapshot.pb.dart' as $92;

enum Action_Action {
  carousel, 
  flip, 
  auto, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.carousel,
    6 : Action_Action.flip,
    7 : Action_Action.auto,
    12 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.stage'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 12])
    ..aOM<$89.Carousel>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'carousel', subBuilder: $89.Carousel.create)
    ..aOM<$90.Flip>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'flip', subBuilder: $90.Flip.create)
    ..aOM<$91.Auto>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'auto', subBuilder: $91.Auto.create)
    ..aOM<$92.Snapshot>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $92.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $89.Carousel? carousel,
    $90.Flip? flip,
    $91.Auto? auto,
    $92.Snapshot? snapshot,
  }) {
    final _result = create();
    if (carousel != null) {
      _result.carousel = carousel;
    }
    if (flip != null) {
      _result.flip = flip;
    }
    if (auto != null) {
      _result.auto = auto;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(5)
  $89.Carousel get carousel => $_getN(0);
  @$pb.TagNumber(5)
  set carousel($89.Carousel v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasCarousel() => $_has(0);
  @$pb.TagNumber(5)
  void clearCarousel() => clearField(5);
  @$pb.TagNumber(5)
  $89.Carousel ensureCarousel() => $_ensure(0);

  @$pb.TagNumber(6)
  $90.Flip get flip => $_getN(1);
  @$pb.TagNumber(6)
  set flip($90.Flip v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasFlip() => $_has(1);
  @$pb.TagNumber(6)
  void clearFlip() => clearField(6);
  @$pb.TagNumber(6)
  $90.Flip ensureFlip() => $_ensure(1);

  @$pb.TagNumber(7)
  $91.Auto get auto => $_getN(2);
  @$pb.TagNumber(7)
  set auto($91.Auto v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasAuto() => $_has(2);
  @$pb.TagNumber(7)
  void clearAuto() => clearField(7);
  @$pb.TagNumber(7)
  $91.Auto ensureAuto() => $_ensure(2);

  @$pb.TagNumber(12)
  $92.Snapshot get snapshot => $_getN(3);
  @$pb.TagNumber(12)
  set snapshot($92.Snapshot v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasSnapshot() => $_has(3);
  @$pb.TagNumber(12)
  void clearSnapshot() => clearField(12);
  @$pb.TagNumber(12)
  $92.Snapshot ensureSnapshot() => $_ensure(3);
}

