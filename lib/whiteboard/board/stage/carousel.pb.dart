///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/carousel.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/user.pb.dart' as $12;

class Carousel extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Carousel', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.stage'), createEmptyInstance: create)
    ..pc<$12.User>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'users', $pb.PbFieldType.PM, subBuilder: $12.User.create)
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'switch')
    ..hasRequiredFields = false
  ;

  Carousel._() : super();
  factory Carousel({
    $core.Iterable<$12.User>? users,
    $core.bool? switch_6,
  }) {
    final _result = create();
    if (users != null) {
      _result.users.addAll(users);
    }
    if (switch_6 != null) {
      _result.switch_6 = switch_6;
    }
    return _result;
  }
  factory Carousel.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Carousel.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Carousel clone() => Carousel()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Carousel copyWith(void Function(Carousel) updates) => super.copyWith((message) => updates(message as Carousel)) as Carousel; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Carousel create() => Carousel._();
  Carousel createEmptyInstance() => create();
  static $pb.PbList<Carousel> createRepeated() => $pb.PbList<Carousel>();
  @$core.pragma('dart2js:noInline')
  static Carousel getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Carousel>(create);
  static Carousel? _defaultInstance;

  @$pb.TagNumber(5)
  $core.List<$12.User> get users => $_getList(0);

  @$pb.TagNumber(6)
  $core.bool get switch_6 => $_getBF(1);
  @$pb.TagNumber(6)
  set switch_6($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(6)
  $core.bool hasSwitch_6() => $_has(1);
  @$pb.TagNumber(6)
  void clearSwitch_6() => clearField(6);
}

