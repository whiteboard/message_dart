///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'carousel', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.stage.Carousel', '9': 0, '10': 'carousel'},
    const {'1': 'flip', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.stage.Flip', '9': 0, '10': 'flip'},
    const {'1': 'auto', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.stage.Auto', '9': 0, '10': 'auto'},
    const {'1': 'snapshot', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.board.stage.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SPgoIY2Fyb3VzZWwYBSABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnN0YWdlLkNhcm91c2VsSABSCGNhcm91c2VsEjIKBGZsaXAYBiABKAsyHC53aGl0ZWJvYXJkLmJvYXJkLnN0YWdlLkZsaXBIAFIEZmxpcBIyCgRhdXRvGAcgASgLMhwud2hpdGVib2FyZC5ib2FyZC5zdGFnZS5BdXRvSABSBGF1dG8SPgoIc25hcHNob3QYDCABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnN0YWdlLlNuYXBzaG90SABSCHNuYXBzaG90QggKBmFjdGlvbg==');
