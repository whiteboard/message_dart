///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/flip.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Flip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Flip', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.stage'), createEmptyInstance: create)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.O3)
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'carousel')
    ..hasRequiredFields = false
  ;

  Flip._() : super();
  factory Flip({
    $core.int? page,
    $core.bool? carousel,
  }) {
    final _result = create();
    if (page != null) {
      _result.page = page;
    }
    if (carousel != null) {
      _result.carousel = carousel;
    }
    return _result;
  }
  factory Flip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Flip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Flip clone() => Flip()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Flip copyWith(void Function(Flip) updates) => super.copyWith((message) => updates(message as Flip)) as Flip; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Flip create() => Flip._();
  Flip createEmptyInstance() => create();
  static $pb.PbList<Flip> createRepeated() => $pb.PbList<Flip>();
  @$core.pragma('dart2js:noInline')
  static Flip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Flip>(create);
  static Flip? _defaultInstance;

  @$pb.TagNumber(5)
  $core.int get page => $_getIZ(0);
  @$pb.TagNumber(5)
  set page($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(5)
  $core.bool hasPage() => $_has(0);
  @$pb.TagNumber(5)
  void clearPage() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get carousel => $_getBF(1);
  @$pb.TagNumber(6)
  set carousel($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(6)
  $core.bool hasCarousel() => $_has(1);
  @$pb.TagNumber(6)
  void clearCarousel() => clearField(6);
}

