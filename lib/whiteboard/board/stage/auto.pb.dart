///
//  Generated code. Do not modify.
//  source: whiteboard/board/stage/auto.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Auto extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Auto', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.stage'), createEmptyInstance: create)
    ..aOB(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'switch')
    ..hasRequiredFields = false
  ;

  Auto._() : super();
  factory Auto({
    $core.bool? switch_10,
  }) {
    final _result = create();
    if (switch_10 != null) {
      _result.switch_10 = switch_10;
    }
    return _result;
  }
  factory Auto.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Auto.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Auto clone() => Auto()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Auto copyWith(void Function(Auto) updates) => super.copyWith((message) => updates(message as Auto)) as Auto; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Auto create() => Auto._();
  Auto createEmptyInstance() => create();
  static $pb.PbList<Auto> createRepeated() => $pb.PbList<Auto>();
  @$core.pragma('dart2js:noInline')
  static Auto getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Auto>(create);
  static Auto? _defaultInstance;

  @$pb.TagNumber(10)
  $core.bool get switch_10 => $_getBF(0);
  @$pb.TagNumber(10)
  set switch_10($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(10)
  $core.bool hasSwitch_10() => $_has(0);
  @$pb.TagNumber(10)
  void clearSwitch_10() => clearField(10);
}

