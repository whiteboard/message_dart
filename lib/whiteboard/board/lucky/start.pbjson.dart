///
//  Generated code. Do not modify.
//  source: whiteboard/board/lucky/start.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use startDescriptor instead')
const Start$json = const {
  '1': 'Start',
};

/// Descriptor for `Start`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List startDescriptor = $convert.base64Decode('CgVTdGFydA==');
