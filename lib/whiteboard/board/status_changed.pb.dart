///
//  Generated code. Do not modify.
//  source: whiteboard/board/status_changed.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../core/status.pbenum.dart' as $2;

class StatusChanged extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StatusChanged', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board'), createEmptyInstance: create)
    ..e<$2.Status>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $2.Status.STATUS_UNSPECIFIED, valueOf: $2.Status.valueOf, enumValues: $2.Status.values)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reason')
    ..hasRequiredFields = false
  ;

  StatusChanged._() : super();
  factory StatusChanged({
    $2.Status? status,
    $core.String? reason,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (reason != null) {
      _result.reason = reason;
    }
    return _result;
  }
  factory StatusChanged.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StatusChanged.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StatusChanged clone() => StatusChanged()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StatusChanged copyWith(void Function(StatusChanged) updates) => super.copyWith((message) => updates(message as StatusChanged)) as StatusChanged; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusChanged create() => StatusChanged._();
  StatusChanged createEmptyInstance() => create();
  static $pb.PbList<StatusChanged> createRepeated() => $pb.PbList<StatusChanged>();
  @$core.pragma('dart2js:noInline')
  static StatusChanged getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StatusChanged>(create);
  static StatusChanged? _defaultInstance;

  @$pb.TagNumber(3)
  $2.Status get status => $_getN(0);
  @$pb.TagNumber(3)
  set status($2.Status v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get reason => $_getSZ(1);
  @$pb.TagNumber(4)
  set reason($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasReason() => $_has(1);
  @$pb.TagNumber(4)
  void clearReason() => clearField(4);
}

