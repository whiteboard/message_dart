///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use updateDescriptor instead')
const Update$json = const {
  '1': 'Update',
  '2': const [
    const {'1': 'strokes', '3': 10, '4': 3, '5': 11, '6': '.whiteboard.board.palette.Stroke', '10': 'strokes'},
  ],
};

/// Descriptor for `Update`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateDescriptor = $convert.base64Decode('CgZVcGRhdGUSOgoHc3Ryb2tlcxgKIAMoCzIgLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5TdHJva2VSB3N0cm9rZXM=');
