///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/stroke.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;
import 'range.pb.dart' as $46;
import 'style.pb.dart' as $44;

import 'stroke_type.pbenum.dart' as $45;
import 'stroke_status.pbenum.dart' as $47;

class Stroke extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Stroke', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..pc<$5.Point>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'points', $pb.PbFieldType.PM, subBuilder: $5.Point.create)
    ..aOM<$46.Range>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'range', subBuilder: $46.Range.create)
    ..aOS(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'key')
    ..pPS(40, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'texts')
    ..e<$45.StrokeType>(50, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $45.StrokeType.TYPE_, valueOf: $45.StrokeType.valueOf, enumValues: $45.StrokeType.values)
    ..e<$47.StrokeStatus>(60, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $47.StrokeStatus.STATUS_, valueOf: $47.StrokeStatus.valueOf, enumValues: $47.StrokeStatus.values)
    ..aOM<$44.Style>(70, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'style', subBuilder: $44.Style.create)
    ..aOS(80, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deletedId')
    ..hasRequiredFields = false
  ;

  Stroke._() : super();
  factory Stroke({
    $core.Iterable<$5.Point>? points,
    $46.Range? range,
    $core.String? key,
    $core.Iterable<$core.String>? texts,
    $45.StrokeType? type,
    $47.StrokeStatus? status,
    $44.Style? style,
    $core.String? deletedId,
  }) {
    final _result = create();
    if (points != null) {
      _result.points.addAll(points);
    }
    if (range != null) {
      _result.range = range;
    }
    if (key != null) {
      _result.key = key;
    }
    if (texts != null) {
      _result.texts.addAll(texts);
    }
    if (type != null) {
      _result.type = type;
    }
    if (status != null) {
      _result.status = status;
    }
    if (style != null) {
      _result.style = style;
    }
    if (deletedId != null) {
      _result.deletedId = deletedId;
    }
    return _result;
  }
  factory Stroke.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Stroke.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Stroke clone() => Stroke()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Stroke copyWith(void Function(Stroke) updates) => super.copyWith((message) => updates(message as Stroke)) as Stroke; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Stroke create() => Stroke._();
  Stroke createEmptyInstance() => create();
  static $pb.PbList<Stroke> createRepeated() => $pb.PbList<Stroke>();
  @$core.pragma('dart2js:noInline')
  static Stroke getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Stroke>(create);
  static Stroke? _defaultInstance;

  @$pb.TagNumber(10)
  $core.List<$5.Point> get points => $_getList(0);

  @$pb.TagNumber(20)
  $46.Range get range => $_getN(1);
  @$pb.TagNumber(20)
  set range($46.Range v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasRange() => $_has(1);
  @$pb.TagNumber(20)
  void clearRange() => clearField(20);
  @$pb.TagNumber(20)
  $46.Range ensureRange() => $_ensure(1);

  @$pb.TagNumber(30)
  $core.String get key => $_getSZ(2);
  @$pb.TagNumber(30)
  set key($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(30)
  $core.bool hasKey() => $_has(2);
  @$pb.TagNumber(30)
  void clearKey() => clearField(30);

  @$pb.TagNumber(40)
  $core.List<$core.String> get texts => $_getList(3);

  @$pb.TagNumber(50)
  $45.StrokeType get type => $_getN(4);
  @$pb.TagNumber(50)
  set type($45.StrokeType v) { setField(50, v); }
  @$pb.TagNumber(50)
  $core.bool hasType() => $_has(4);
  @$pb.TagNumber(50)
  void clearType() => clearField(50);

  @$pb.TagNumber(60)
  $47.StrokeStatus get status => $_getN(5);
  @$pb.TagNumber(60)
  set status($47.StrokeStatus v) { setField(60, v); }
  @$pb.TagNumber(60)
  $core.bool hasStatus() => $_has(5);
  @$pb.TagNumber(60)
  void clearStatus() => clearField(60);

  @$pb.TagNumber(70)
  $44.Style get style => $_getN(6);
  @$pb.TagNumber(70)
  set style($44.Style v) { setField(70, v); }
  @$pb.TagNumber(70)
  $core.bool hasStyle() => $_has(6);
  @$pb.TagNumber(70)
  void clearStyle() => clearField(70);
  @$pb.TagNumber(70)
  $44.Style ensureStyle() => $_ensure(6);

  @$pb.TagNumber(80)
  $core.String get deletedId => $_getSZ(7);
  @$pb.TagNumber(80)
  set deletedId($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(80)
  $core.bool hasDeletedId() => $_has(7);
  @$pb.TagNumber(80)
  void clearDeletedId() => clearField(80);
}

