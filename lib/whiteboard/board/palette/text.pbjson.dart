///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/text.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use textDescriptor instead')
const Text$json = const {
  '1': 'Text',
  '2': const [
    const {'1': 'start', '3': 1, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'start'},
    const {'1': 'texts', '3': 2, '4': 3, '5': 9, '10': 'texts'},
    const {'1': 'style', '3': 3, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Style', '10': 'style'},
  ],
};

/// Descriptor for `Text`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List textDescriptor = $convert.base64Decode('CgRUZXh0EiwKBXN0YXJ0GAEgASgLMhYud2hpdGVib2FyZC5jb3JlLlBvaW50UgVzdGFydBIUCgV0ZXh0cxgCIAMoCVIFdGV4dHMSNQoFc3R5bGUYAyABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuU3R5bGVSBXN0eWxl');
