///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/stroke_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use strokeStatusDescriptor instead')
const StrokeStatus$json = const {
  '1': 'StrokeStatus',
  '2': const [
    const {'1': '_STATUS', '2': 0},
    const {'1': 'SHOW', '2': 1},
    const {'1': 'HIDE', '2': 3},
    const {'1': 'DELETED', '2': 4},
  ],
};

/// Descriptor for `StrokeStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List strokeStatusDescriptor = $convert.base64Decode('CgxTdHJva2VTdGF0dXMSCwoHX1NUQVRVUxAAEggKBFNIT1cQARIICgRISURFEAMSCwoHREVMRVRFRBAE');
