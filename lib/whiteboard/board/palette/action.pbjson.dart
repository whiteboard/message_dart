///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'window', '3': 1, '4': 1, '5': 11, '6': '.whiteboard.core.WindowInfo', '10': 'window'},
    const {'1': 'line', '3': 5, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Line', '9': 0, '10': 'line'},
    const {'1': 'mark', '3': 6, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Mark', '9': 0, '10': 'mark'},
    const {'1': 'shape', '3': 7, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Shape', '9': 0, '10': 'shape'},
    const {'1': 'stroke', '3': 8, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Stroke', '9': 0, '10': 'stroke'},
    const {'1': 'text', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Text', '9': 0, '10': 'text'},
    const {'1': 'laser', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Laser', '9': 0, '10': 'laser'},
    const {'1': 'update', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Update', '9': 0, '10': 'update'},
    const {'1': 'reset', '3': 20, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Reset', '9': 0, '10': 'reset'},
    const {'1': 'redo', '3': 21, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Redo', '9': 0, '10': 'redo'},
    const {'1': 'undo', '3': 22, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Undo', '9': 0, '10': 'undo'},
    const {'1': 'clear', '3': 23, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Clear', '9': 0, '10': 'clear'},
    const {'1': 'delete', '3': 24, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Delete', '9': 0, '10': 'delete'},
    const {'1': 'page', '3': 25, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Page', '9': 0, '10': 'page'},
    const {'1': 'snapshot', '3': 30, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Snapshot', '9': 0, '10': 'snapshot'},
  ],
  '8': const [
    const {'1': 'action'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SMwoGd2luZG93GAEgASgLMhsud2hpdGVib2FyZC5jb3JlLldpbmRvd0luZm9SBndpbmRvdxI0CgRsaW5lGAUgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLkxpbmVIAFIEbGluZRI0CgRtYXJrGAYgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLk1hcmtIAFIEbWFyaxI3CgVzaGFwZRgHIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5TaGFwZUgAUgVzaGFwZRI6CgZzdHJva2UYCCABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuU3Ryb2tlSABSBnN0cm9rZRI0CgR0ZXh0GAkgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlRleHRIAFIEdGV4dBI3CgVsYXNlchgKIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5MYXNlckgAUgVsYXNlchI6CgZ1cGRhdGUYCyABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuVXBkYXRlSABSBnVwZGF0ZRI3CgVyZXNldBgUIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5SZXNldEgAUgVyZXNldBI0CgRyZWRvGBUgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlJlZG9IAFIEcmVkbxI0CgR1bmRvGBYgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlVuZG9IAFIEdW5kbxI3CgVjbGVhchgXIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5DbGVhckgAUgVjbGVhchI6CgZkZWxldGUYGCABKAsyIC53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuRGVsZXRlSABSBmRlbGV0ZRI0CgRwYWdlGBkgASgLMh4ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlBhZ2VIAFIEcGFnZRJACghzbmFwc2hvdBgeIAEoCzIiLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5TbmFwc2hvdEgAUghzbmFwc2hvdEIICgZhY3Rpb24=');
