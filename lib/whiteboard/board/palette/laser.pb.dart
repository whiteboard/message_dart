///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/laser.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;

class Laser extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Laser', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..aOM<$5.Point>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'position', subBuilder: $5.Point.create)
    ..hasRequiredFields = false
  ;

  Laser._() : super();
  factory Laser({
    $5.Point? position,
  }) {
    final _result = create();
    if (position != null) {
      _result.position = position;
    }
    return _result;
  }
  factory Laser.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Laser.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Laser clone() => Laser()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Laser copyWith(void Function(Laser) updates) => super.copyWith((message) => updates(message as Laser)) as Laser; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Laser create() => Laser._();
  Laser createEmptyInstance() => create();
  static $pb.PbList<Laser> createRepeated() => $pb.PbList<Laser>();
  @$core.pragma('dart2js:noInline')
  static Laser getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Laser>(create);
  static Laser? _defaultInstance;

  @$pb.TagNumber(10)
  $5.Point get position => $_getN(0);
  @$pb.TagNumber(10)
  set position($5.Point v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasPosition() => $_has(0);
  @$pb.TagNumber(10)
  void clearPosition() => clearField(10);
  @$pb.TagNumber(10)
  $5.Point ensurePosition() => $_ensure(0);
}

