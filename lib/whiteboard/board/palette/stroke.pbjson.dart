///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/stroke.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use strokeDescriptor instead')
const Stroke$json = const {
  '1': 'Stroke',
  '2': const [
    const {'1': 'points', '3': 10, '4': 3, '5': 11, '6': '.whiteboard.core.Point', '10': 'points'},
    const {'1': 'range', '3': 20, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Range', '10': 'range'},
    const {'1': 'key', '3': 30, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'texts', '3': 40, '4': 3, '5': 9, '10': 'texts'},
    const {'1': 'type', '3': 50, '4': 1, '5': 14, '6': '.whiteboard.board.palette.StrokeType', '10': 'type'},
    const {'1': 'status', '3': 60, '4': 1, '5': 14, '6': '.whiteboard.board.palette.StrokeStatus', '10': 'status'},
    const {'1': 'style', '3': 70, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Style', '10': 'style'},
    const {'1': 'deleted_id', '3': 80, '4': 1, '5': 9, '10': 'deletedId'},
  ],
};

/// Descriptor for `Stroke`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List strokeDescriptor = $convert.base64Decode('CgZTdHJva2USLgoGcG9pbnRzGAogAygLMhYud2hpdGVib2FyZC5jb3JlLlBvaW50UgZwb2ludHMSNQoFcmFuZ2UYFCABKAsyHy53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuUmFuZ2VSBXJhbmdlEhAKA2tleRgeIAEoCVIDa2V5EhQKBXRleHRzGCggAygJUgV0ZXh0cxI4CgR0eXBlGDIgASgOMiQud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlN0cm9rZVR5cGVSBHR5cGUSPgoGc3RhdHVzGDwgASgOMiYud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlN0cm9rZVN0YXR1c1IGc3RhdHVzEjUKBXN0eWxlGEYgASgLMh8ud2hpdGVib2FyZC5ib2FyZC5wYWxldHRlLlN0eWxlUgVzdHlsZRIdCgpkZWxldGVkX2lkGFAgASgJUglkZWxldGVkSWQ=');
