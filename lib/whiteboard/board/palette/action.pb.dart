///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/action.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/window_info.pb.dart' as $50;
import 'line.pb.dart' as $51;
import 'mark.pb.dart' as $52;
import 'shape.pb.dart' as $53;
import 'stroke.pb.dart' as $48;
import 'text.pb.dart' as $54;
import 'laser.pb.dart' as $55;
import 'update.pb.dart' as $56;
import 'reset.pb.dart' as $57;
import 'redo.pb.dart' as $58;
import 'undo.pb.dart' as $59;
import 'clear.pb.dart' as $60;
import 'delete.pb.dart' as $61;
import 'page.pb.dart' as $62;
import 'snapshot.pb.dart' as $63;

enum Action_Action {
  line, 
  mark, 
  shape, 
  stroke, 
  text, 
  laser, 
  update, 
  reset, 
  redo, 
  undo, 
  clear_23, 
  delete, 
  page, 
  snapshot, 
  notSet
}

class Action extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Action_Action> _Action_ActionByTag = {
    5 : Action_Action.line,
    6 : Action_Action.mark,
    7 : Action_Action.shape,
    8 : Action_Action.stroke,
    9 : Action_Action.text,
    10 : Action_Action.laser,
    11 : Action_Action.update,
    20 : Action_Action.reset,
    21 : Action_Action.redo,
    22 : Action_Action.undo,
    23 : Action_Action.clear_23,
    24 : Action_Action.delete,
    25 : Action_Action.page,
    30 : Action_Action.snapshot,
    0 : Action_Action.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..oo(0, [5, 6, 7, 8, 9, 10, 11, 20, 21, 22, 23, 24, 25, 30])
    ..aOM<$50.WindowInfo>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'window', subBuilder: $50.WindowInfo.create)
    ..aOM<$51.Line>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'line', subBuilder: $51.Line.create)
    ..aOM<$52.Mark>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mark', subBuilder: $52.Mark.create)
    ..aOM<$53.Shape>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'shape', subBuilder: $53.Shape.create)
    ..aOM<$48.Stroke>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stroke', subBuilder: $48.Stroke.create)
    ..aOM<$54.Text>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text', subBuilder: $54.Text.create)
    ..aOM<$55.Laser>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'laser', subBuilder: $55.Laser.create)
    ..aOM<$56.Update>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'update', subBuilder: $56.Update.create)
    ..aOM<$57.Reset>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reset', subBuilder: $57.Reset.create)
    ..aOM<$58.Redo>(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redo', subBuilder: $58.Redo.create)
    ..aOM<$59.Undo>(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'undo', subBuilder: $59.Undo.create)
    ..aOM<$60.Clear>(23, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'clear', subBuilder: $60.Clear.create)
    ..aOM<$61.Delete>(24, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'delete', subBuilder: $61.Delete.create)
    ..aOM<$62.Page>(25, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', subBuilder: $62.Page.create)
    ..aOM<$63.Snapshot>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'snapshot', subBuilder: $63.Snapshot.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    $50.WindowInfo? window,
    $51.Line? line,
    $52.Mark? mark,
    $53.Shape? shape,
    $48.Stroke? stroke,
    $54.Text? text,
    $55.Laser? laser,
    $56.Update? update,
    $57.Reset? reset,
    $58.Redo? redo,
    $59.Undo? undo,
    $60.Clear? clear_23,
    $61.Delete? delete,
    $62.Page? page,
    $63.Snapshot? snapshot,
  }) {
    final _result = create();
    if (window != null) {
      _result.window = window;
    }
    if (line != null) {
      _result.line = line;
    }
    if (mark != null) {
      _result.mark = mark;
    }
    if (shape != null) {
      _result.shape = shape;
    }
    if (stroke != null) {
      _result.stroke = stroke;
    }
    if (text != null) {
      _result.text = text;
    }
    if (laser != null) {
      _result.laser = laser;
    }
    if (update != null) {
      _result.update = update;
    }
    if (reset != null) {
      _result.reset = reset;
    }
    if (redo != null) {
      _result.redo = redo;
    }
    if (undo != null) {
      _result.undo = undo;
    }
    if (clear_23 != null) {
      _result.clear_23 = clear_23;
    }
    if (delete != null) {
      _result.delete = delete;
    }
    if (page != null) {
      _result.page = page;
    }
    if (snapshot != null) {
      _result.snapshot = snapshot;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  Action_Action whichAction() => _Action_ActionByTag[$_whichOneof(0)]!;
  void clearAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $50.WindowInfo get window => $_getN(0);
  @$pb.TagNumber(1)
  set window($50.WindowInfo v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasWindow() => $_has(0);
  @$pb.TagNumber(1)
  void clearWindow() => clearField(1);
  @$pb.TagNumber(1)
  $50.WindowInfo ensureWindow() => $_ensure(0);

  @$pb.TagNumber(5)
  $51.Line get line => $_getN(1);
  @$pb.TagNumber(5)
  set line($51.Line v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLine() => $_has(1);
  @$pb.TagNumber(5)
  void clearLine() => clearField(5);
  @$pb.TagNumber(5)
  $51.Line ensureLine() => $_ensure(1);

  @$pb.TagNumber(6)
  $52.Mark get mark => $_getN(2);
  @$pb.TagNumber(6)
  set mark($52.Mark v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMark() => $_has(2);
  @$pb.TagNumber(6)
  void clearMark() => clearField(6);
  @$pb.TagNumber(6)
  $52.Mark ensureMark() => $_ensure(2);

  @$pb.TagNumber(7)
  $53.Shape get shape => $_getN(3);
  @$pb.TagNumber(7)
  set shape($53.Shape v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasShape() => $_has(3);
  @$pb.TagNumber(7)
  void clearShape() => clearField(7);
  @$pb.TagNumber(7)
  $53.Shape ensureShape() => $_ensure(3);

  @$pb.TagNumber(8)
  $48.Stroke get stroke => $_getN(4);
  @$pb.TagNumber(8)
  set stroke($48.Stroke v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasStroke() => $_has(4);
  @$pb.TagNumber(8)
  void clearStroke() => clearField(8);
  @$pb.TagNumber(8)
  $48.Stroke ensureStroke() => $_ensure(4);

  @$pb.TagNumber(9)
  $54.Text get text => $_getN(5);
  @$pb.TagNumber(9)
  set text($54.Text v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasText() => $_has(5);
  @$pb.TagNumber(9)
  void clearText() => clearField(9);
  @$pb.TagNumber(9)
  $54.Text ensureText() => $_ensure(5);

  @$pb.TagNumber(10)
  $55.Laser get laser => $_getN(6);
  @$pb.TagNumber(10)
  set laser($55.Laser v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasLaser() => $_has(6);
  @$pb.TagNumber(10)
  void clearLaser() => clearField(10);
  @$pb.TagNumber(10)
  $55.Laser ensureLaser() => $_ensure(6);

  @$pb.TagNumber(11)
  $56.Update get update => $_getN(7);
  @$pb.TagNumber(11)
  set update($56.Update v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasUpdate() => $_has(7);
  @$pb.TagNumber(11)
  void clearUpdate() => clearField(11);
  @$pb.TagNumber(11)
  $56.Update ensureUpdate() => $_ensure(7);

  @$pb.TagNumber(20)
  $57.Reset get reset => $_getN(8);
  @$pb.TagNumber(20)
  set reset($57.Reset v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasReset() => $_has(8);
  @$pb.TagNumber(20)
  void clearReset() => clearField(20);
  @$pb.TagNumber(20)
  $57.Reset ensureReset() => $_ensure(8);

  @$pb.TagNumber(21)
  $58.Redo get redo => $_getN(9);
  @$pb.TagNumber(21)
  set redo($58.Redo v) { setField(21, v); }
  @$pb.TagNumber(21)
  $core.bool hasRedo() => $_has(9);
  @$pb.TagNumber(21)
  void clearRedo() => clearField(21);
  @$pb.TagNumber(21)
  $58.Redo ensureRedo() => $_ensure(9);

  @$pb.TagNumber(22)
  $59.Undo get undo => $_getN(10);
  @$pb.TagNumber(22)
  set undo($59.Undo v) { setField(22, v); }
  @$pb.TagNumber(22)
  $core.bool hasUndo() => $_has(10);
  @$pb.TagNumber(22)
  void clearUndo() => clearField(22);
  @$pb.TagNumber(22)
  $59.Undo ensureUndo() => $_ensure(10);

  @$pb.TagNumber(23)
  $60.Clear get clear_23 => $_getN(11);
  @$pb.TagNumber(23)
  set clear_23($60.Clear v) { setField(23, v); }
  @$pb.TagNumber(23)
  $core.bool hasClear_23() => $_has(11);
  @$pb.TagNumber(23)
  void clearClear_23() => clearField(23);
  @$pb.TagNumber(23)
  $60.Clear ensureClear_23() => $_ensure(11);

  @$pb.TagNumber(24)
  $61.Delete get delete => $_getN(12);
  @$pb.TagNumber(24)
  set delete($61.Delete v) { setField(24, v); }
  @$pb.TagNumber(24)
  $core.bool hasDelete() => $_has(12);
  @$pb.TagNumber(24)
  void clearDelete() => clearField(24);
  @$pb.TagNumber(24)
  $61.Delete ensureDelete() => $_ensure(12);

  @$pb.TagNumber(25)
  $62.Page get page => $_getN(13);
  @$pb.TagNumber(25)
  set page($62.Page v) { setField(25, v); }
  @$pb.TagNumber(25)
  $core.bool hasPage() => $_has(13);
  @$pb.TagNumber(25)
  void clearPage() => clearField(25);
  @$pb.TagNumber(25)
  $62.Page ensurePage() => $_ensure(13);

  @$pb.TagNumber(30)
  $63.Snapshot get snapshot => $_getN(14);
  @$pb.TagNumber(30)
  set snapshot($63.Snapshot v) { setField(30, v); }
  @$pb.TagNumber(30)
  $core.bool hasSnapshot() => $_has(14);
  @$pb.TagNumber(30)
  void clearSnapshot() => clearField(30);
  @$pb.TagNumber(30)
  $63.Snapshot ensureSnapshot() => $_ensure(14);
}

