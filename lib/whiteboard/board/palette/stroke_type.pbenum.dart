///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/stroke_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class StrokeType extends $pb.ProtobufEnum {
  static const StrokeType TYPE_ = StrokeType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : '_TYPE');
  static const StrokeType LINE = StrokeType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LINE');
  static const StrokeType MARK = StrokeType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MARK');
  static const StrokeType TEXT = StrokeType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TEXT');
  static const StrokeType TRIANGLE = StrokeType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TRIANGLE');
  static const StrokeType RECTANGLE = StrokeType._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'RECTANGLE');
  static const StrokeType ELLIPSE = StrokeType._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ELLIPSE');
  static const StrokeType CIRCLE = StrokeType._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CIRCLE');

  static const $core.List<StrokeType> values = <StrokeType> [
    TYPE_,
    LINE,
    MARK,
    TEXT,
    TRIANGLE,
    RECTANGLE,
    ELLIPSE,
    CIRCLE,
  ];

  static final $core.Map<$core.int, StrokeType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static StrokeType? valueOf($core.int value) => _byValue[value];

  const StrokeType._($core.int v, $core.String n) : super(v, n);
}

