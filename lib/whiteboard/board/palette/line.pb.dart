///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/line.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;
import 'style.pb.dart' as $44;

class Line extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Line', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..aOM<$5.Point>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $5.Point.create)
    ..aOM<$5.Point>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: $5.Point.create)
    ..aOM<$44.Style>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'style', subBuilder: $44.Style.create)
    ..hasRequiredFields = false
  ;

  Line._() : super();
  factory Line({
    $5.Point? start,
    $5.Point? end,
    $44.Style? style,
  }) {
    final _result = create();
    if (start != null) {
      _result.start = start;
    }
    if (end != null) {
      _result.end = end;
    }
    if (style != null) {
      _result.style = style;
    }
    return _result;
  }
  factory Line.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Line.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Line clone() => Line()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Line copyWith(void Function(Line) updates) => super.copyWith((message) => updates(message as Line)) as Line; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Line create() => Line._();
  Line createEmptyInstance() => create();
  static $pb.PbList<Line> createRepeated() => $pb.PbList<Line>();
  @$core.pragma('dart2js:noInline')
  static Line getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Line>(create);
  static Line? _defaultInstance;

  @$pb.TagNumber(10)
  $5.Point get start => $_getN(0);
  @$pb.TagNumber(10)
  set start($5.Point v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasStart() => $_has(0);
  @$pb.TagNumber(10)
  void clearStart() => clearField(10);
  @$pb.TagNumber(10)
  $5.Point ensureStart() => $_ensure(0);

  @$pb.TagNumber(20)
  $5.Point get end => $_getN(1);
  @$pb.TagNumber(20)
  set end($5.Point v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasEnd() => $_has(1);
  @$pb.TagNumber(20)
  void clearEnd() => clearField(20);
  @$pb.TagNumber(20)
  $5.Point ensureEnd() => $_ensure(1);

  @$pb.TagNumber(30)
  $44.Style get style => $_getN(2);
  @$pb.TagNumber(30)
  set style($44.Style v) { setField(30, v); }
  @$pb.TagNumber(30)
  $core.bool hasStyle() => $_has(2);
  @$pb.TagNumber(30)
  void clearStyle() => clearField(30);
  @$pb.TagNumber(30)
  $44.Style ensureStyle() => $_ensure(2);
}

