///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/style.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use styleDescriptor instead')
const Style$json = const {
  '1': 'Style',
  '2': const [
    const {'1': 'width', '3': 1, '4': 1, '5': 5, '10': 'width'},
    const {'1': 'alpha', '3': 2, '4': 1, '5': 5, '10': 'alpha'},
    const {'1': 'stroke_style', '3': 3, '4': 1, '5': 9, '10': 'strokeStyle'},
    const {'1': 'fill_style', '3': 4, '4': 1, '5': 9, '10': 'fillStyle'},
    const {'1': 'font_size', '3': 5, '4': 1, '5': 9, '10': 'fontSize'},
  ],
};

/// Descriptor for `Style`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List styleDescriptor = $convert.base64Decode('CgVTdHlsZRIUCgV3aWR0aBgBIAEoBVIFd2lkdGgSFAoFYWxwaGEYAiABKAVSBWFscGhhEiEKDHN0cm9rZV9zdHlsZRgDIAEoCVILc3Ryb2tlU3R5bGUSHQoKZmlsbF9zdHlsZRgEIAEoCVIJZmlsbFN0eWxlEhsKCWZvbnRfc2l6ZRgFIAEoCVIIZm9udFNpemU=');
