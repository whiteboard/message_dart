///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/laser.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use laserDescriptor instead')
const Laser$json = const {
  '1': 'Laser',
  '2': const [
    const {'1': 'position', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'position'},
  ],
};

/// Descriptor for `Laser`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List laserDescriptor = $convert.base64Decode('CgVMYXNlchIyCghwb3NpdGlvbhgKIAEoCzIWLndoaXRlYm9hcmQuY29yZS5Qb2ludFIIcG9zaXRpb24=');
