///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/style.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Style extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Style', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'width', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'alpha', $pb.PbFieldType.O3)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'strokeStyle')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fillStyle')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fontSize')
    ..hasRequiredFields = false
  ;

  Style._() : super();
  factory Style({
    $core.int? width,
    $core.int? alpha,
    $core.String? strokeStyle,
    $core.String? fillStyle,
    $core.String? fontSize,
  }) {
    final _result = create();
    if (width != null) {
      _result.width = width;
    }
    if (alpha != null) {
      _result.alpha = alpha;
    }
    if (strokeStyle != null) {
      _result.strokeStyle = strokeStyle;
    }
    if (fillStyle != null) {
      _result.fillStyle = fillStyle;
    }
    if (fontSize != null) {
      _result.fontSize = fontSize;
    }
    return _result;
  }
  factory Style.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Style.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Style clone() => Style()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Style copyWith(void Function(Style) updates) => super.copyWith((message) => updates(message as Style)) as Style; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Style create() => Style._();
  Style createEmptyInstance() => create();
  static $pb.PbList<Style> createRepeated() => $pb.PbList<Style>();
  @$core.pragma('dart2js:noInline')
  static Style getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Style>(create);
  static Style? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get width => $_getIZ(0);
  @$pb.TagNumber(1)
  set width($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasWidth() => $_has(0);
  @$pb.TagNumber(1)
  void clearWidth() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get alpha => $_getIZ(1);
  @$pb.TagNumber(2)
  set alpha($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAlpha() => $_has(1);
  @$pb.TagNumber(2)
  void clearAlpha() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get strokeStyle => $_getSZ(2);
  @$pb.TagNumber(3)
  set strokeStyle($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStrokeStyle() => $_has(2);
  @$pb.TagNumber(3)
  void clearStrokeStyle() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get fillStyle => $_getSZ(3);
  @$pb.TagNumber(4)
  set fillStyle($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFillStyle() => $_has(3);
  @$pb.TagNumber(4)
  void clearFillStyle() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get fontSize => $_getSZ(4);
  @$pb.TagNumber(5)
  set fontSize($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasFontSize() => $_has(4);
  @$pb.TagNumber(5)
  void clearFontSize() => clearField(5);
}

