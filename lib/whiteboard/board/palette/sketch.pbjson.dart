///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/sketch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use sketchDescriptor instead')
const Sketch$json = const {
  '1': 'Sketch',
  '2': const [
    const {'1': 'cursor', '3': 1, '4': 1, '5': 5, '10': 'cursor'},
    const {'1': 'stroke_keys', '3': 2, '4': 3, '5': 9, '10': 'strokeKeys'},
    const {'1': 'stroke_map', '3': 3, '4': 3, '5': 11, '6': '.whiteboard.board.palette.Sketch.StrokeMapEntry', '10': 'strokeMap'},
  ],
  '3': const [Sketch_StrokeMapEntry$json],
};

@$core.Deprecated('Use sketchDescriptor instead')
const Sketch_StrokeMapEntry$json = const {
  '1': 'StrokeMapEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Stroke', '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Sketch`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sketchDescriptor = $convert.base64Decode('CgZTa2V0Y2gSFgoGY3Vyc29yGAEgASgFUgZjdXJzb3ISHwoLc3Ryb2tlX2tleXMYAiADKAlSCnN0cm9rZUtleXMSTgoKc3Ryb2tlX21hcBgDIAMoCzIvLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5Ta2V0Y2guU3Ryb2tlTWFwRW50cnlSCXN0cm9rZU1hcBpeCg5TdHJva2VNYXBFbnRyeRIQCgNrZXkYASABKAlSA2tleRI2CgV2YWx1ZRgCIAEoCzIgLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5TdHJva2VSBXZhbHVlOgI4AQ==');
