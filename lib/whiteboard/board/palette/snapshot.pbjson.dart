///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/snapshot.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use snapshotDescriptor instead')
const Snapshot$json = const {
  '1': 'Snapshot',
  '2': const [
    const {'1': 'sketches', '3': 2, '4': 3, '5': 11, '6': '.whiteboard.board.palette.Sketch', '10': 'sketches'},
  ],
};

/// Descriptor for `Snapshot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List snapshotDescriptor = $convert.base64Decode('CghTbmFwc2hvdBI8Cghza2V0Y2hlcxgCIAMoCzIgLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5Ta2V0Y2hSCHNrZXRjaGVz');
