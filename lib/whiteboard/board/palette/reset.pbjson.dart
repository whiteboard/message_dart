///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/reset.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use resetDescriptor instead')
const Reset$json = const {
  '1': 'Reset',
  '2': const [
    const {'1': 'page', '3': 10, '4': 1, '5': 5, '10': 'page'},
  ],
};

/// Descriptor for `Reset`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resetDescriptor = $convert.base64Decode('CgVSZXNldBISCgRwYWdlGAogASgFUgRwYWdl');
