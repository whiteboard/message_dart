///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/stroke_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use strokeTypeDescriptor instead')
const StrokeType$json = const {
  '1': 'StrokeType',
  '2': const [
    const {'1': '_TYPE', '2': 0},
    const {'1': 'LINE', '2': 1},
    const {'1': 'MARK', '2': 2},
    const {'1': 'TEXT', '2': 3},
    const {'1': 'TRIANGLE', '2': 4},
    const {'1': 'RECTANGLE', '2': 5},
    const {'1': 'ELLIPSE', '2': 6},
    const {'1': 'CIRCLE', '2': 7},
  ],
};

/// Descriptor for `StrokeType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List strokeTypeDescriptor = $convert.base64Decode('CgpTdHJva2VUeXBlEgkKBV9UWVBFEAASCAoETElORRABEggKBE1BUksQAhIICgRURVhUEAMSDAoIVFJJQU5HTEUQBBINCglSRUNUQU5HTEUQBRILCgdFTExJUFNFEAYSCgoGQ0lSQ0xFEAc=');
