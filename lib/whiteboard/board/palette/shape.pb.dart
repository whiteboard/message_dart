///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/shape.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;
import 'style.pb.dart' as $44;

import 'stroke_type.pbenum.dart' as $45;

class Shape extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Shape', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..aOM<$5.Point>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $5.Point.create)
    ..aOM<$5.Point>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'delta', subBuilder: $5.Point.create)
    ..e<$45.StrokeType>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $45.StrokeType.TYPE_, valueOf: $45.StrokeType.valueOf, enumValues: $45.StrokeType.values)
    ..aOM<$44.Style>(40, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'style', subBuilder: $44.Style.create)
    ..hasRequiredFields = false
  ;

  Shape._() : super();
  factory Shape({
    $5.Point? start,
    $5.Point? delta,
    $45.StrokeType? type,
    $44.Style? style,
  }) {
    final _result = create();
    if (start != null) {
      _result.start = start;
    }
    if (delta != null) {
      _result.delta = delta;
    }
    if (type != null) {
      _result.type = type;
    }
    if (style != null) {
      _result.style = style;
    }
    return _result;
  }
  factory Shape.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Shape.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Shape clone() => Shape()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Shape copyWith(void Function(Shape) updates) => super.copyWith((message) => updates(message as Shape)) as Shape; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Shape create() => Shape._();
  Shape createEmptyInstance() => create();
  static $pb.PbList<Shape> createRepeated() => $pb.PbList<Shape>();
  @$core.pragma('dart2js:noInline')
  static Shape getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Shape>(create);
  static Shape? _defaultInstance;

  @$pb.TagNumber(10)
  $5.Point get start => $_getN(0);
  @$pb.TagNumber(10)
  set start($5.Point v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasStart() => $_has(0);
  @$pb.TagNumber(10)
  void clearStart() => clearField(10);
  @$pb.TagNumber(10)
  $5.Point ensureStart() => $_ensure(0);

  @$pb.TagNumber(20)
  $5.Point get delta => $_getN(1);
  @$pb.TagNumber(20)
  set delta($5.Point v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasDelta() => $_has(1);
  @$pb.TagNumber(20)
  void clearDelta() => clearField(20);
  @$pb.TagNumber(20)
  $5.Point ensureDelta() => $_ensure(1);

  @$pb.TagNumber(30)
  $45.StrokeType get type => $_getN(2);
  @$pb.TagNumber(30)
  set type($45.StrokeType v) { setField(30, v); }
  @$pb.TagNumber(30)
  $core.bool hasType() => $_has(2);
  @$pb.TagNumber(30)
  void clearType() => clearField(30);

  @$pb.TagNumber(40)
  $44.Style get style => $_getN(3);
  @$pb.TagNumber(40)
  set style($44.Style v) { setField(40, v); }
  @$pb.TagNumber(40)
  $core.bool hasStyle() => $_has(3);
  @$pb.TagNumber(40)
  void clearStyle() => clearField(40);
  @$pb.TagNumber(40)
  $44.Style ensureStyle() => $_ensure(3);
}

