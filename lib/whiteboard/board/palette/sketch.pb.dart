///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/sketch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'stroke.pb.dart' as $48;

class Sketch extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Sketch', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cursor', $pb.PbFieldType.O3)
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'strokeKeys')
    ..m<$core.String, $48.Stroke>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'strokeMap', entryClassName: 'Sketch.StrokeMapEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OM, valueCreator: $48.Stroke.create, packageName: const $pb.PackageName('whiteboard.board.palette'))
    ..hasRequiredFields = false
  ;

  Sketch._() : super();
  factory Sketch({
    $core.int? cursor,
    $core.Iterable<$core.String>? strokeKeys,
    $core.Map<$core.String, $48.Stroke>? strokeMap,
  }) {
    final _result = create();
    if (cursor != null) {
      _result.cursor = cursor;
    }
    if (strokeKeys != null) {
      _result.strokeKeys.addAll(strokeKeys);
    }
    if (strokeMap != null) {
      _result.strokeMap.addAll(strokeMap);
    }
    return _result;
  }
  factory Sketch.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Sketch.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Sketch clone() => Sketch()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Sketch copyWith(void Function(Sketch) updates) => super.copyWith((message) => updates(message as Sketch)) as Sketch; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Sketch create() => Sketch._();
  Sketch createEmptyInstance() => create();
  static $pb.PbList<Sketch> createRepeated() => $pb.PbList<Sketch>();
  @$core.pragma('dart2js:noInline')
  static Sketch getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Sketch>(create);
  static Sketch? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get cursor => $_getIZ(0);
  @$pb.TagNumber(1)
  set cursor($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCursor() => $_has(0);
  @$pb.TagNumber(1)
  void clearCursor() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get strokeKeys => $_getList(1);

  @$pb.TagNumber(3)
  $core.Map<$core.String, $48.Stroke> get strokeMap => $_getMap(2);
}

