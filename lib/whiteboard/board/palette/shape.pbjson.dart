///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/shape.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use shapeDescriptor instead')
const Shape$json = const {
  '1': 'Shape',
  '2': const [
    const {'1': 'start', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'start'},
    const {'1': 'delta', '3': 20, '4': 1, '5': 11, '6': '.whiteboard.core.Point', '10': 'delta'},
    const {'1': 'type', '3': 30, '4': 1, '5': 14, '6': '.whiteboard.board.palette.StrokeType', '10': 'type'},
    const {'1': 'style', '3': 40, '4': 1, '5': 11, '6': '.whiteboard.board.palette.Style', '10': 'style'},
  ],
};

/// Descriptor for `Shape`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List shapeDescriptor = $convert.base64Decode('CgVTaGFwZRIsCgVzdGFydBgKIAEoCzIWLndoaXRlYm9hcmQuY29yZS5Qb2ludFIFc3RhcnQSLAoFZGVsdGEYFCABKAsyFi53aGl0ZWJvYXJkLmNvcmUuUG9pbnRSBWRlbHRhEjgKBHR5cGUYHiABKA4yJC53aGl0ZWJvYXJkLmJvYXJkLnBhbGV0dGUuU3Ryb2tlVHlwZVIEdHlwZRI1CgVzdHlsZRgoIAEoCzIfLndoaXRlYm9hcmQuYm9hcmQucGFsZXR0ZS5TdHlsZVIFc3R5bGU=');
