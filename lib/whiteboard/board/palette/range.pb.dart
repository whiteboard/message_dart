///
//  Generated code. Do not modify.
//  source: whiteboard/board/palette/range.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../core/point.pb.dart' as $5;

class Range extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Range', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.board.palette'), createEmptyInstance: create)
    ..aOM<$5.Point>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', subBuilder: $5.Point.create)
    ..aOM<$5.Point>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', subBuilder: $5.Point.create)
    ..hasRequiredFields = false
  ;

  Range._() : super();
  factory Range({
    $5.Point? min,
    $5.Point? max,
  }) {
    final _result = create();
    if (min != null) {
      _result.min = min;
    }
    if (max != null) {
      _result.max = max;
    }
    return _result;
  }
  factory Range.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Range.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Range clone() => Range()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Range copyWith(void Function(Range) updates) => super.copyWith((message) => updates(message as Range)) as Range; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Range create() => Range._();
  Range createEmptyInstance() => create();
  static $pb.PbList<Range> createRepeated() => $pb.PbList<Range>();
  @$core.pragma('dart2js:noInline')
  static Range getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Range>(create);
  static Range? _defaultInstance;

  @$pb.TagNumber(10)
  $5.Point get min => $_getN(0);
  @$pb.TagNumber(10)
  set min($5.Point v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasMin() => $_has(0);
  @$pb.TagNumber(10)
  void clearMin() => clearField(10);
  @$pb.TagNumber(10)
  $5.Point ensureMin() => $_ensure(0);

  @$pb.TagNumber(20)
  $5.Point get max => $_getN(1);
  @$pb.TagNumber(20)
  set max($5.Point v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasMax() => $_has(1);
  @$pb.TagNumber(20)
  void clearMax() => clearField(20);
  @$pb.TagNumber(20)
  $5.Point ensureMax() => $_ensure(1);
}

